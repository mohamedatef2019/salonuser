package grand.app.salonssuser.network

import grand.app.salonssuser.main.changepassword.request.ChangePassRequest
import grand.app.salonssuser.auth.forgotpass.request.ForgotRequest
import grand.app.salonssuser.auth.login.request.LoginRequest
import grand.app.salonssuser.auth.login.request.SocialLoginRequest
import grand.app.salonssuser.auth.register.request.RegisterRequest
import grand.app.salonssuser.auth.resetpass.request.ResetRequest
import grand.app.salonssuser.auth.verifycode.request.VerifyRequest
import grand.app.salonssuser.dialogs.addrate.reuqest.AddRateRequest
import grand.app.salonssuser.main.addsalon.request.AddSalonRequest
import grand.app.salonssuser.main.allsalonss.home.request.HomeShopsRequest
import grand.app.salonssuser.main.contactus.request.ContactRequest
import grand.app.salonssuser.main.editprofile.request.EditProfileRequest
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.home.request.UpdateTokenRequest
import grand.app.salonssuser.main.messages.request.SendMessageRequest
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.search.request.SearchRequest
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.toMultiPart
import grand.app.salonssuser.utils.toRequestBodyParam

/**
 * Created by MouazSalah on 28/12/2020.
 **/

class ApiRepo(private val apiInterface: ApiInterface) {

    suspend fun login(request : LoginRequest) = apiInterface.login(request)
    suspend fun socialLogin(request : SocialLoginRequest) = apiInterface.socialLogin(request)

    suspend fun register(request : RegisterRequest) = apiInterface.register(
        request.name!!.toRequestBodyParam(),
        request.email!!.toRequestBodyParam(),
        request.phone!!.toRequestBodyParam(),
        request.password!!.toRequestBodyParam(),
        request.firebase_token!!.toRequestBodyParam(),
        request.type.toRequestBodyParam(),
        request.userImg?.toMultiPart("img")!!
    )

    suspend fun forgotPassword(request : ForgotRequest) = apiInterface.forgotPass(request)
    suspend fun verifyCode(request : VerifyRequest) = apiInterface.verify(request)
    suspend fun resetPassword(request : ResetRequest) = apiInterface.resetPass(request)
    suspend fun changePass(request : ChangePassRequest) = apiInterface.changePassword(PrefMethods.getUserData()!!.jwt.toString(), request)

    suspend fun splash() = apiInterface.splash()

    suspend fun getTerms() = apiInterface.getTerms()
    suspend fun getAboutApp() = apiInterface.getAboutApp()

    suspend fun getHome() = apiInterface.getHome()

    suspend fun getSalonShops(catId : Int, serviceType : Int) = apiInterface.getSalonsShops(catId, serviceType)

    suspend fun getHomeShops(request : HomeShopsRequest) = apiInterface.getHomeShops(request)

    suspend fun getFavorites() = apiInterface.getFavorites(PrefMethods.getUserData()!!.jwt.toString())
    suspend fun addToFav(itemId : Int) = apiInterface.addToFav(PrefMethods.getUserData()!!.jwt.toString(), itemId)

    suspend fun getRates(salonId : Int) = apiInterface.getRates(salonId)
    suspend fun addRate(addRateRequest : AddRateRequest) = apiInterface.addRate(PrefMethods.getUserData()!!.jwt.toString(), addRateRequest)

    suspend fun getVisitorSalonDetails(salonId : Int) = apiInterface.getVisitorSalonDetails(salonId)
    suspend fun getLoginSalonDetails(salonId : Int) = apiInterface.getLoginSalonDetails(PrefMethods.getUserData()!!.jwt.toString(), salonId)

    suspend fun sendHelpMessage(request : ContactRequest) = apiInterface.sendHelpMsg(request)

    suspend fun updateProfileWithImg(request : EditProfileRequest) = apiInterface.updateProfileWithImg(
            PrefMethods.getUserData()!!.jwt.toString(),
            request.name!!.toRequestBodyParam(),
            request.email!!.toRequestBodyParam(),
            request.phone!!.toRequestBodyParam(),
            request.userImg?.toMultiPart("img")!!
    )

    suspend fun updateProfileWithoutImg(request : EditProfileRequest) = apiInterface.updateWithoutImg(PrefMethods.getUserData()!!.jwt.toString(), request)

    suspend fun getWallet() = apiInterface.getWallet(PrefMethods.getUserData()!!.jwt.toString())

    suspend fun getMyBookings(bookingState : Int, pageNumber : Int) = apiInterface.getMyBookings(PrefMethods.getUserData()!!.jwt.toString(), bookingState, pageNumber)

    suspend fun getBookingDetails(bookingId : Int) = apiInterface.getBookingDetails(PrefMethods.getUserData()!!.jwt.toString(), bookingId)

    suspend fun updateToken(request : UpdateTokenRequest) = apiInterface.updateToken(PrefMethods.getUserData()!!.jwt.toString(), request)

    suspend fun addSalon(request : AddSalonRequest) = apiInterface.addSalon(
            request.name!!.toRequestBodyParam(),
            request.email!!.toRequestBodyParam(),
            request.phone!!.toRequestBodyParam(),
            request.type!!.toRequestBodyParam(),
            request.salon_name!!.toRequestBodyParam(),
            request.iban_number!!.toRequestBodyParam(),
            request.payment_number!!.toRequestBodyParam(),
            request.salonCertificate?.toMultiPart("certificate")!!,
            request.salonLicense?.toMultiPart("license")!!
    )

    suspend fun getChats() = apiInterface.getChats(PrefMethods.getUserData()!!.jwt.toString())

    suspend fun getCities() = apiInterface.getCities()

    suspend fun getFilterData(catId : Int) = apiInterface.getFilterData(catId)
    suspend fun filterSalons(request : FilterRequest) = apiInterface.filterSalons(request)

    suspend fun getMessages(salonId : Int) = apiInterface.getMessages(PrefMethods.getUserData()!!.jwt.toString(), salonId)

    suspend fun sendChatMsg(request : SendMessageRequest) = apiInterface.sendChatMsg(PrefMethods.getUserData()!!.jwt.toString(), request)

    suspend fun sendChatMsgWithImg(request : SendMessageRequest) = apiInterface.sendChatMsg(
            PrefMethods.getUserData()!!.jwt.toString(),
            request.receiver_id!!.toRequestBodyParam(),
            request.salon_id!!.toRequestBodyParam(),
            request.text!!.toRequestBodyParam(),
            request.img?.toMultiPart("imgs[]")!!)

    suspend fun searchSalons(request : SearchRequest) = apiInterface.searchSalons(request)

    suspend fun createOrder(request : CreateOrderRequest) = apiInterface.createOrder(PrefMethods.getUserData()!!.jwt.toString(), request)

    suspend fun cancelOrder(id : Int) = apiInterface.cancelBooking(PrefMethods.getUserData()!!.jwt.toString(), id)
}