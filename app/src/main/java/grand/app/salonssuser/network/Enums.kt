package grand.app.salonssuser.network

/**
 * Created by MouazSalah 28/12/2020.
 **/
enum class Status {
    SUCCESS,
    SUCCESS_MESSAGE,
    ERROR_MESSAGE,
    SHIMMER_LOADING,
    PROGRESS_LOADING,
    CREATE_ORDER
}

