package grand.app.salonssuser.network

object ServerStatus {
    const val SUCCESS = 200
    const val FAIL = 404
    const val NOT_AUTH = 401
}