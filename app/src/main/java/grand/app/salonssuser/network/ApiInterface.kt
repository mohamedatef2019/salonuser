package grand.app.salonssuser.network

import grand.app.salonssuser.main.changepassword.request.ChangePassRequest
import grand.app.salonssuser.auth.forgotpass.request.ForgotRequest
import grand.app.salonssuser.auth.forgotpass.response.ForgotResponse
import grand.app.salonssuser.auth.login.request.LoginRequest
import grand.app.salonssuser.auth.login.request.SocialLoginRequest
import grand.app.salonssuser.auth.login.response.LoginResponse
import grand.app.salonssuser.auth.login.response.SocialLoginResponse
import grand.app.salonssuser.auth.register.response.RegisterResponse
import grand.app.salonssuser.auth.resetpass.request.ResetRequest
import grand.app.salonssuser.auth.resetpass.response.ResetPassResponse
import grand.app.salonssuser.auth.splash.response.SplashResponse
import grand.app.salonssuser.auth.verifycode.request.VerifyRequest
import grand.app.salonssuser.auth.verifycode.response.VerifyResponse
import grand.app.salonssuser.main.aboutapp.response.AboutAppResponse
import grand.app.salonssuser.dialogs.addrate.reuqest.AddRateRequest
import grand.app.salonssuser.dialogs.addrate.response.AddRateResponse
import grand.app.salonssuser.main.addsalon.response.AddSalonResponse
import grand.app.salonssuser.main.allsalonss.home.request.HomeShopsRequest
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.main.bookingdetails.response.BookingDetailsResponse
import grand.app.salonssuser.main.bookings.response.MyBookingsResponse
import grand.app.salonssuser.main.bookings.response.CancelOrderResponse
import grand.app.salonssuser.main.chat.response.ChatsResponse
import grand.app.salonssuser.main.contactus.request.ContactRequest
import grand.app.salonssuser.main.contactus.response.ContactUsResponse
import grand.app.salonssuser.main.editprofile.request.EditProfileRequest
import grand.app.salonssuser.main.editprofile.response.UpdateProfileResponse
import grand.app.salonssuser.main.favorites.response.AddToFavResponse
import grand.app.salonssuser.main.favorites.response.FavoriteResponse
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.response.FilterDataResponse
import grand.app.salonssuser.main.filter.response.FilterSalonsResponse
import grand.app.salonssuser.main.home.request.UpdateTokenRequest
import grand.app.salonssuser.main.home.response.HomeResponse
import grand.app.salonssuser.main.home.response.UpdateTokenResponse
import grand.app.salonssuser.main.messages.request.SendMessageRequest
import grand.app.salonssuser.main.messages.response.MessagesResponse
import grand.app.salonssuser.main.messages.response.SendMessageResponse
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.orderreview.response.CreateOrderResponse
import grand.app.salonssuser.main.rates.response.RatesResponse
import grand.app.salonssuser.main.salondetails.response.SalonDetailsResponse
import grand.app.salonssuser.main.search.request.SearchRequest
import grand.app.salonssuser.main.search.response.SearchResponse
import grand.app.salonssuser.main.selectaddress.response.CitiesResponse
import grand.app.salonssuser.main.terms.response.TermsResponse
import grand.app.salonssuser.main.wallet.response.WalletResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface
{
    /* ---------- Auth APIs -------- */
    // Splash
    @GET("api/settings/sliders?type=user")
    suspend fun splash(): SplashResponse

    // login
    @POST("api/user/login")
     suspend fun login(@Body loginRequest: LoginRequest): LoginResponse

    @Multipart
    @POST("api/user/register")
    suspend fun register(@Part("name") name: RequestBody,
                         @Part("email") email: RequestBody,
                         @Part("phone") phone: RequestBody,
                         @Part("password") password: RequestBody,
                         @Part("firebase_token") firebaseToken: RequestBody,
                         @Part("type") type: RequestBody,
                         @Part image: MultipartBody.Part): RegisterResponse

    @Multipart
    @POST("api/user/update-profile")
    suspend fun updateProfileWithImg(@Header("jwt") jwt : String,
                         @Part("name") name: RequestBody,
                         @Part("email") email: RequestBody,
                         @Part("phone") phone: RequestBody,
                         @Part image: MultipartBody.Part): UpdateProfileResponse

    // login
    @POST("api/user/update-profile")
    suspend fun updateWithoutImg(@Header("jwt") jwt : String, @Body request: EditProfileRequest): UpdateProfileResponse

    // Social Login
    @POST("api/user/social-login")
    suspend fun socialLogin(@Body socialRequest: SocialLoginRequest?): SocialLoginResponse

    // forgotPass
    @POST("api/user/send-code")
    suspend fun forgotPass(@Body forgotPassRequest: ForgotRequest?): ForgotResponse

    // resetPass
    @POST("api/user/change-password")
    suspend fun changePassword(@Header("jwt") jwt : String,@Body changeRequest: ChangePassRequest?): ResetPassResponse

    // verify
    @POST("api/user/verify-code")
    suspend fun verify(@Body verifyCodeRequest: VerifyRequest?): VerifyResponse

    // resetPass
    @POST("api/user/change-password")
    suspend fun resetPass(@Body resetPassRequest: ResetRequest?): ResetPassResponse

    @GET("api/orders/salons/{catId}/{serviceType}")
    suspend fun getSalonsShops(@Path("catId") catId : Int,
                               @Path("serviceType") serviceType : Int): AllSalonsResponse

    @POST("api/orders/salons-by-lat-lng")
    suspend fun getHomeShops( @Body request : HomeShopsRequest): AllSalonsResponse

    @POST("api/user/update-token")
    suspend fun updateToken(@Header("jwt") jwt : String, @Body request: UpdateTokenRequest): UpdateTokenResponse

    // ABOUT
    @GET("api/settings/about")
    suspend fun getAboutApp(): AboutAppResponse

    // Terms
    @GET("api/settings/terms/0")
    suspend fun getTerms(): TermsResponse

    @GET("api/settings/home")
    suspend fun getHome(): HomeResponse

    @GET("api/orders/favorites")
    suspend fun getFavorites(@Header("jwt") jwt : String): FavoriteResponse

    @GET("api/orders/favorite/{id}")
    suspend fun addToFav(@Header("jwt") jwt : String, @Path("id") id : Int): AddToFavResponse

    @GET("api/orders/rates/{id}")
    suspend fun getRates(@Path("id") id : Int): RatesResponse

    @POST("api/orders/rate")
    suspend fun addRate(@Header("jwt") jwt : String, @Body addRateRequest: AddRateRequest?): AddRateResponse

    @GET("api/orders/salon-details/{id}")
    suspend fun getLoginSalonDetails(@Header("jwt") jwt : String, @Path("id") id : Int): SalonDetailsResponse

    @GET("api/orders/salon-details/{id}")
    suspend fun getVisitorSalonDetails(@Path("id") id : Int): SalonDetailsResponse

    @POST("api/settings/contact-us")
    suspend fun sendHelpMsg(@Body request: ContactRequest): ContactUsResponse

    @GET("api/orders/my-wallet")
    suspend fun getWallet(@Header("jwt") jwt : String): WalletResponse

    @GET("api/orders/my-orders/{bookingsStatus}")
    suspend fun getMyBookings(@Header("jwt") jwt : String, @Path("bookingsStatus") bookingsStatus: Int,
                              @Query("pageNumber") pageNumber : Int): MyBookingsResponse

    @GET("api/orders/order/{id}")
    suspend fun getBookingDetails(@Header("jwt") jwt : String, @Path("id") id : Int): BookingDetailsResponse

    @GET("api/orders/cancel-order/{bookingId}")
    suspend fun cancelBooking(@Header("jwt") jwt : String, @Path("bookingId") bookingId : Int): CancelOrderResponse

    @GET("api/settings/messages-history")
    suspend fun getChats(@Header("jwt") jwt : String): ChatsResponse

    @GET("api/settings/messages/{chatId}")
    suspend fun getMessages(@Header("jwt") jwt : String, @Path("chatId") chatId : Int): MessagesResponse

    @GET("api/settings/cities")
    suspend fun getCities(): CitiesResponse

    @GET("api/settings/filter-data/{id}")
    suspend fun getFilterData(@Path("id") id : Int): FilterDataResponse

    @POST("api/settings/filter")
    suspend fun filterSalons(@Body request : FilterRequest): FilterSalonsResponse

    @POST("api/settings/send-message")
    suspend fun sendChatMsg(@Header("jwt") jwt : String, @Body request: SendMessageRequest): SendMessageResponse

    @Multipart
    @POST("api/settings/send-message")
    suspend fun sendChatMsg(@Header("jwt") jwt : String,
                            @Part("receiver_id") name: RequestBody,
                            @Part("salon_id") email: RequestBody,
                            @Part("text") phone: RequestBody,
                            @Part img: MultipartBody.Part): SendMessageResponse

    @Multipart
    @POST("api/orders/provider-request")
    suspend fun addSalon(@Part("name") name: RequestBody,
                         @Part("email") email: RequestBody,
                         @Part("phone") phone: RequestBody,
                         @Part("type") type: RequestBody,
                         @Part("salon_name") salonName: RequestBody,
                         @Part("iban_number") iban_number: RequestBody,
                         @Part("payment_number") payment_number: RequestBody,
                         @Part certificate: MultipartBody.Part,
                         @Part license: MultipartBody.Part): AddSalonResponse



    @POST("api/settings/search")
    suspend fun searchSalons(@Body request: SearchRequest): SearchResponse

    @POST("api/orders/create-order")
    suspend fun createOrder(@Header("jwt") jwt : String, @Body request: CreateOrderRequest): CreateOrderResponse
}