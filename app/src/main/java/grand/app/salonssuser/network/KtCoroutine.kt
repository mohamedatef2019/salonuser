package grand.app.salonssuser.network

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Created by MouazSalah 15/05/2021.
 **/

class KtCoroutine {
    companion object {
        fun delayJob(delayInSeconds: Long, callBack:()->Unit) {
            GlobalScope.launch {
                delay(delayInSeconds * 1000)
                callBack()
            }
        }

        fun repeatJob(interval:Int, action: (Int) -> Unit):Job{
            return GlobalScope.launch {
                repeat(interval, action)
            }
        }
    }
}