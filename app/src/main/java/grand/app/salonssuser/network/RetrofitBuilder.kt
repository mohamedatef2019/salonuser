package grand.app.salonssuser.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/*
 * Created by MouazSalah on 28/12/2020.
 */

private const val BASE_URL = "https://salonss.my-staff.net/"

/*
*add this if you want a static header in all requests
**/

private fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .apply {
            this.addInterceptor(HttpLoggingInterceptor().setLevel(Level.BODY))
                .addInterceptor(Interceptor { chain ->
                    val request: Request = chain.request().newBuilder().addHeader("lang", "ar").build()
                    chain.proceed(request)
                })
        }
        .readTimeout(20, TimeUnit.MINUTES)
        .connectTimeout(20, TimeUnit.MINUTES)
        .build()
}

    private val retrofitBuilder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(createOkHttpClient())
        .build()

    object RetrofitBuilder {
        val API_SERVICE: ApiInterface by lazy {
            retrofitBuilder.create(ApiInterface::class.java)
        }
}