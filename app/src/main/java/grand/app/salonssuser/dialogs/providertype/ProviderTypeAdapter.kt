package grand.app.salonssuser.dialogs.providertype

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemProviderTypeBinding
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class ProviderTypeAdapter : RecyclerView.Adapter<ProviderTypeAdapter.ProviderTypeHolder>()
{
    var itemsList: ArrayList<ProviderTypeItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<ProviderTypeItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderTypeHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemProviderTypeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_provider_type, parent, false)
        return ProviderTypeHolder(binding)
    }

    override fun onBindViewHolder(holder: ProviderTypeHolder, position: Int) {
        val itemViewModel = ItemProviderTypeViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): ProviderTypeItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProviderTypeItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProviderTypeHolder(val binding: ItemProviderTypeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
