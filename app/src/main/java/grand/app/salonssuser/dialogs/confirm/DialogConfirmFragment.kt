package grand.app.salonssuser.dialogs.confirm

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonssuser.databinding.DialogConfirmBinding
import grand.app.salonssuser.utils.constants.Params
import timber.log.Timber

class DialogConfirmFragment  : DialogFragment()
{
    lateinit  var  binding: DialogConfirmBinding
    var message : String = ""
    var orderId : Int = -1
    var requestCode : Int = -1

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.DIALOG_CONFIRM_MESSAGE) -> {
                        message = requireArguments().getString(Params.DIALOG_CONFIRM_MESSAGE, null)
                        orderId = requireArguments().getInt(Params.ORDER_ID, 0)
                        requestCode = requireArguments().getInt(Params.REQUEST_CODE, 0)
                        Timber.e("Mou3aaaz dialog onCreate")
                        Timber.e("Mou3aaaz dialog feedback request code " + requireArguments().getInt(Params.REQUEST_CODE, 0))
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        binding = DialogConfirmBinding.inflate(inflater, container, false)

        binding.tvSubMessage.text = message

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(requestCode, intent)
            requireActivity().finish()
        }

        binding.btnConfirm.setOnClickListener {
            Timber.e("Mou3aaaz dialog feedback request code " + requestCode)
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            intent.putExtra(Params.ORDER_ID, orderId)
            requireActivity().setResult(requestCode, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}