package grand.app.salonssuser.dialogs.dialogcity

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.constants.Codes

class DialogCityViewModel : BaseViewModel()
{
    var cityItem = CityItem()
    var adapter = CitiesAdapter()

    fun onConfirmClicked() {
        when (cityItem.id) {
            null -> {
                setValue(Codes.EMPTY_CITY)
            }
            else -> {
                setValue(Codes.CITY_SELECTED)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}