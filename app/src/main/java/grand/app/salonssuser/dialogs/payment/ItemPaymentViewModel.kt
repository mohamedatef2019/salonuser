package grand.app.salonssuser.dialogs.payment

import grand.app.salonssuser.base.BaseViewModel

class ItemPaymentViewModel(var item: PaymentItem) : BaseViewModel()
