package grand.app.salonssuser.dialogs.addrate.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.DialogAddRateBinding
import grand.app.salonssuser.dialogs.addrate.response.AddRateResponse
import grand.app.salonssuser.dialogs.addrate.viewmodel.DialogAddRateViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class DialogAddRateFragment  : DialogFragment()
{
    lateinit  var  binding: DialogAddRateBinding
    lateinit var viewModel : DialogAddRateViewModel
    var salonId : Int = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.ORDER_ID) -> {
                        salonId = requireArguments().getInt(Params.ORDER_ID, 0)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogAddRateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogAddRateViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.request.salon_id = salonId

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_MESSAGE -> Toasty.error(requireActivity(), getString(R.string.msg_empty_message)).show()
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    Toasty.error(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS_MESSAGE -> {
                    Toasty.success(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddRateResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.RATE_MESSAGE, it.data.msg)
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            requireActivity().setResult(Codes.DIALOG_RATE_ORDER, intent)
                            requireActivity().finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}