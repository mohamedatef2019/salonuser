package grand.app.salonssuser.dialogs.addrate.reuqest

data class AddRateRequest (
    var salon_id: Int? = null,
    var rate: Int? = null,
    var comment: String? = null)