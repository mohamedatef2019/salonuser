package grand.app.salonssuser.dialogs.dialogcity

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.selectaddress.response.CityItem

class ItemCityViewModel(var item: CityItem) : BaseViewModel()
