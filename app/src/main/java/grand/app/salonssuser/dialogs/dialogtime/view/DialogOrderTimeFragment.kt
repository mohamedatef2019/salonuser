package grand.app.salonssuser.dialogs.dialogtime.view

import CalendarUtil
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.DialogOrderTimeBinding
import grand.app.salonssuser.dialogs.dialogtime.time.ItemSalonTime
import grand.app.salonssuser.dialogs.dialogtime.viewmodel.DialogOrderTimeViewModel
import grand.app.salonssuser.main.salondetails.response.ReservedTimeItem
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import grand.app.salonssuser.main.salondetails.response.SalonDetailsData
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DialogOrderTimeFragment : DialogFragment() {
    lateinit var binding: DialogOrderTimeBinding
    lateinit var viewModel: DialogOrderTimeViewModel
    var salonData = SalonDetailsData()
    var reservedTimes = ArrayList<ReservedTimeItem>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.SALON_DETAILS_DATA) -> {
                        salonData = requireArguments().getSerializable(Params.SALON_DETAILS_DATA) as SalonDetailsData
                        reservedTimes = salonData.reservedTimesList as ArrayList<ReservedTimeItem>

                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogOrderTimeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogOrderTimeViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.salonDetails = salonData

        setDaysList(salonData.daysList as ArrayList<SalonDayItem>)


        observe(viewModel.daysAdapter.itemLiveData) {
            viewModel.obsDay.set(it!!.date)
            setTimesList(it)
        }

        observe(viewModel.timesAdapter.itemLiveData) {
            viewModel.obstime.set(it!!.time)
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.EMPTY_DATE -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_select_date)).show()
                }
                Codes.EMPTY_TIME -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_select_time)).show()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_ORDER_TIME, intent)
                    requireActivity().finish()
                }
                Codes.TIME_SELECTED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.ORDER_DATE, viewModel.obsDay.get())
                    intent.putExtra(Params.ORDER_TIME, viewModel.obstime.get())
                    intent.putExtra(Params.ORDER_NOTE, viewModel.obsNote.get())
                    requireActivity().setResult(Codes.DIALOG_ORDER_TIME, intent)
                    requireActivity().finish()
                }
            }
        }
    }

    private fun setDaysList(datesList: ArrayList<SalonDayItem>) {

        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val currentDate = sdf.format(Date())
        val datesList2 = CalendarUtil.getSevenComingDays(7, currentDate)

        datesList.forEach { dateItem ->

            datesList2.forEach { dateItem2 ->

                val inFormat = SimpleDateFormat("dd-MM-yyyy")
                val date = inFormat.parse(dateItem2.date)

                val outFormat = SimpleDateFormat("EEEE", Locale("ar"))
                val dayName = outFormat.format(date)

                when {
                    dateItem.day.equals(dayName) -> {
                        dateItem2.startTime = dateItem.startTime
                        dateItem2.endTime = dateItem.endTime
                        dateItem2.jsonMemberSwitch = dateItem.jsonMemberSwitch
                    }
                }
            }
        }

        viewModel.obsDay.set(datesList2[0].day)
        viewModel.daysAdapter.updateList(datesList2)
        setTimesList(datesList[0])
    }

    private fun setTimesList(item: SalonDayItem) {

        val cTimesList = CalendarUtil.getTimes(30, item.startTime, item.endTime)

        //booked times
        val takenList = salonData.reservedTimesList

        val newList = arrayListOf<ItemSalonTime>()

        val salonTimesList = arrayListOf<String>()

        cTimesList.forEach { itemtime ->
            var date = Date()
            val format = SimpleDateFormat("HH:mm")
            try {
                date = format.parse(itemtime)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
            salonTimesList.add(timeFormat.format(date).toString())
        }

        salonTimesList.forEach { itemtime ->
            newList.add(ItemSalonTime(itemtime, found(takenList,itemtime, item.date.toString())))
        }


        for (item in newList) {

            val stringArray: List<String> = item.time!!.split(":")

            var number  = Integer.parseInt(stringArray.get(0))

            if(number > 12){
                number -= 12
                item.time = item.time!!.replaceFirst(stringArray[0],number.toString()).plus(" PM")

            }else {
                item.time =  item.time.plus(" AM")
            }

        }

        viewModel.timesAdapter.updateList(newList)
        Timber.e("Mou3aaaaz times list : " + newList.size.toString())
    }

    fun found (takenList: List<ReservedTimeItem?>?, itemTime: String , currentDate : String) : Boolean{
        takenList.let {
            it?.forEach {

                val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
                var date = Date()
                val format = SimpleDateFormat("HH:mm")
                try {
                    date = format.parse(it?.time)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                var temp = timeFormat.format(date ).toString()
                Log.e("DATa",temp +"  "+itemTime)

                if (temp == itemTime && currentDate == it?.date) {
                    Log.e("DATa2",temp +"  "+itemTime)
                    return false
                }
            }
        }
        return true
    }
}