package grand.app.salonssuser.dialogs.dialogtime.day

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemDayViewModel(var item: SalonDayItem) : BaseViewModel()
{
    var obsDayName = ObservableField<String>()
    var obsMonthName = ObservableField<String>()
    var obsDayNumber = ObservableField<String>()

    var date = Date()

    init {
            item.let {

                when {
                    item.date != null -> {
                        val format = SimpleDateFormat("dd-MM-yyyy")
                        try {
                            date = format.parse(it.date)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }

                        val dayFormat = SimpleDateFormat("EEEE", Locale("ar"))
                        obsDayName.set(dayFormat.format(date).toString());

                        val monthFormat = SimpleDateFormat("MMM", Locale.ENGLISH)
                        obsMonthName.set(monthFormat.format(date).toString());

                        val dayNumber = SimpleDateFormat("dd", Locale.ENGLISH)
                        obsDayNumber.set(dayNumber.format(date).toString())
                    }
                }
        }
    }
}
