package grand.app.salonssuser.dialogs.dialogtime.time

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemTimeViewModel(var item: ItemSalonTime) : BaseViewModel()
{
    var obstime = ObservableField<String>()
    var date = Date()

    init {
        when {
            item.time != null -> {
//                val format = SimpleDateFormat("hh:mm aa", Locale("en"))
//                try {
//                    date = format.parse(item.time)
//                } catch (e: ParseException) {
//                    e.printStackTrace()
//                }
//
//                val timeFormat = SimpleDateFormat("hh:mm aa", Locale("en"))
                obstime.set(item.time)
            }
        }
    }
}
