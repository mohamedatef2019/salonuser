package grand.app.salonssuser.dialogs.dialogcity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemCityBinding
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class CitiesAdapter : RecyclerView.Adapter<CitiesAdapter.CitiesHolder>()
{
    var itemsList: ArrayList<CityItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CityItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCityBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_city, parent, false)
        return CitiesHolder(binding)
    }

    override fun onBindViewHolder(holder: CitiesHolder, position: Int) {
        val itemViewModel = ItemCityViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): CityItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CityItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CitiesHolder(val binding: ItemCityBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
