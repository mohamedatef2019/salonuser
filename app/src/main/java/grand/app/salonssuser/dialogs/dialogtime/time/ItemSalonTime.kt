package grand.app.salonssuser.dialogs.dialogtime.time

data class ItemSalonTime(
        var time : String ?= null,
        var isSelected : Boolean ?= null
)
