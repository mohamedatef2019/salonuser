package grand.app.salonssuser.dialogs.dialogtime.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.dialogs.dialogtime.day.OrderDaysAdapter
import grand.app.salonssuser.dialogs.dialogtime.time.OrderTimesAdapter
import grand.app.salonssuser.main.salondetails.response.SalonDetailsData
import grand.app.salonssuser.utils.constants.Codes

class DialogOrderTimeViewModel : BaseViewModel()
{
    var salonDetails = SalonDetailsData()
    var obsNote = ObservableField<String>()
    var daysAdapter = OrderDaysAdapter()
    var timesAdapter = OrderTimesAdapter()
    var obsDay = ObservableField<String>()
    var obstime = ObservableField<String>()

    fun onConfirmClicked() {
        when {
            obsDay.get() == null -> {
                setValue(Codes.EMPTY_DATE)
            }
            obstime.get() == null -> {
                setValue(Codes.EMPTY_TIME)
            }
            else -> {
                setValue(Codes.TIME_SELECTED)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}