package grand.app.salonssuser.dialogs.dialogtime.time

import android.R.string
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.ItemOrderTimeBinding
import grand.app.salonssuser.utils.SingleLiveEvent


class OrderTimesAdapter : RecyclerView.Adapter<OrderTimesAdapter.OrderTimesHolder>()
{
    var itemsList: ArrayList<ItemSalonTime> = ArrayList()
    var itemLiveData = SingleLiveEvent<ItemSalonTime>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderTimesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderTimeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_time, parent, false)
        return OrderTimesHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderTimesHolder, position: Int)
    {
        val itemViewModel = ItemTimeViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setOpen()
        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                itemViewModel.item.isSelected == true -> {
                    when {
                        position != selectedPosition -> {
                            notifyItemChanged(position)
                            notifyItemChanged(selectedPosition)
                            selectedPosition = position
                            itemLiveData.value = itemViewModel.item
                        }
                    }
                }
            }
        }

//
//        if(itemsList[position].isSelected == false){
//            holder.binding.timeLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
//            holder.binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
//            holder.binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.secondary_color))
//        }
////
//        holder.binding.rawLayout.setOnClickListener {
//            if (itemViewModel.item.isSelected == true) {
//                when {
//                    position != selectedPosition -> {
//                        notifyItemChanged(position)
//                        notifyItemChanged(selectedPosition)
//                        selectedPosition = position
//                        itemLiveData.value = itemViewModel.item
//                    }
//                }
//            }
//        }
    }

    fun getItem(pos:Int): ItemSalonTime {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ItemSalonTime>) {
        itemsList = models





        notifyDataSetChanged()
    }

    inner class OrderTimesHolder(val binding: ItemOrderTimeBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.timeLayout.strokeWidth = 6
                }
                else -> {
                    binding.timeLayout.strokeWidth = 0
                }
            }
        }

        fun setOpen() {
            when (getItem(adapterPosition).isSelected) {
                false -> {
                    binding.timeLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.secondary_color))
                }
            }
        }
    }
}
