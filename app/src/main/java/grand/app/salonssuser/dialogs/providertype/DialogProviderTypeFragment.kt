package grand.app.salonssuser.dialogs.providertype

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.DialogCityBinding
import grand.app.salonssuser.databinding.DialogProviderTypeBinding
import grand.app.salonssuser.main.selectaddress.response.CitiesResponse
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import kotlin.collections.ArrayList

class DialogProviderTypeFragment  : DialogFragment()
{
    lateinit  var  binding: DialogProviderTypeBinding
    lateinit var viewModel : DialogProviderTypeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogProviderTypeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogProviderTypeViewModel::class.java)
        binding.viewModel = viewModel


        observe(viewModel.adapter.itemLiveData){
            viewModel.item = it as ProviderTypeItem
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_TYPE -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_empty_type)).show()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_PROVIDER_REQUEST, intent)
                    requireActivity().finish()
                }
                Codes.TYPE_SELECTED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.PROVIDER_TYPE, viewModel.item)
                    requireActivity().setResult(Codes.DIALOG_PROVIDER_REQUEST, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}