package grand.app.salonssuser.dialogs.message

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import grand.app.salonssuser.databinding.DialogMessageBinding
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DialogMessageFragment  : DialogFragment()
{
    lateinit  var  binding: DialogMessageBinding
    var message : String = ""
    var subMsg : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.DIALOG_CONFIRM_MESSAGE) -> {
                        message = requireArguments().getString(Params.DIALOG_CONFIRM_MESSAGE, null)
                        subMsg = requireArguments().getString(Params.DIALOG_CONFIRM_SUB_MESSAGE, null)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogMessageBinding.inflate(inflater, container, false)

        binding.tvMessage.text = message
        binding.tvSubMessage.text = subMsg

        lifecycleScope.launch {
            delay(3000)
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_MESSAGE_REQUEST, intent)
            requireActivity().finish()
        }
        return binding.root
    }
}