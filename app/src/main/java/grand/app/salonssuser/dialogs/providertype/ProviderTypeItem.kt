package grand.app.salonssuser.dialogs.providertype

import java.io.Serializable

data class ProviderTypeItem(
    var id: Int? = null,
    var name: String? = null
) : Serializable
