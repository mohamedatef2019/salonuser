package grand.app.salonssuser.dialogs.payment

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.utils.constants.Codes

class DialogPaymentViewModel : BaseViewModel()
{
    var adapter = PaymentAdapter()
    var paymentWay : Int = -1

    init {
        val paymentList = arrayListOf(
                PaymentItem(0 , "كاش"),
                PaymentItem(1 , "اونلاين")
        )

        adapter.updateList(paymentList)
    }

    fun onConfirmClicked() {
        when (paymentWay) {
            -1 -> {
                setValue(Codes.EMPTY_PAYMENT_METHOD)
            }
            else -> {
                setValue(Codes.SELECT_PAYMENT)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}