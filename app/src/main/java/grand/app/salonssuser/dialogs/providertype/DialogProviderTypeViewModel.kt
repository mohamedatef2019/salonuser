package grand.app.salonssuser.dialogs.providertype

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.dialogs.payment.PaymentItem
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.constants.Codes

class DialogProviderTypeViewModel : BaseViewModel()
{
    var item = ProviderTypeItem()
    var adapter = ProviderTypeAdapter()

    init {
        val typesList = arrayListOf(
                ProviderTypeItem(1 , "فرد"),
                ProviderTypeItem(2 , "صالون"),
                ProviderTypeItem(3 , "فرد + صالون")
        )

        adapter.updateList(typesList)
    }


    fun onConfirmClicked() {
        when (item.id) {
            null -> {
                setValue(Codes.EMPTY_TYPE)
            }
            else -> {
                setValue(Codes.TYPE_SELECTED)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}