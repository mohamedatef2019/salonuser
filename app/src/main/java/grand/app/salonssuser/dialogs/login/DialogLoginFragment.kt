package grand.app.salonssuser.dialogs.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonssuser.databinding.DialogLoginBinding
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params

class DialogLoginFragment  : DialogFragment()
{
    lateinit  var  binding: DialogLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogLoginBinding.inflate(inflater, container, false)

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(Codes.DIALOG_LOGIN_REQUEST, intent)
            requireActivity().finish()
        }

        binding.btnConfirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_LOGIN_REQUEST, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}