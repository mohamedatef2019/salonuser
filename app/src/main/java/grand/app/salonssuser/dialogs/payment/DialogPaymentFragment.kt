package grand.app.salonssuser.dialogs.payment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import grand.app.salonssuser.databinding.DialogPaymentWayBinding
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe

class DialogPaymentFragment  : DialogFragment()
{
    lateinit  var  binding: DialogPaymentWayBinding
    lateinit var viewModel : DialogPaymentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogPaymentWayBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogPaymentViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData){
            viewModel.paymentWay = it!!.id!!
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.SELECT_PAYMENT -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.PAYMENT_WAY, viewModel.paymentWay)
                    requireActivity().setResult(Codes.SELECT_ORDER_PAYMENT_DIALOG, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.SELECT_ORDER_PAYMENT_DIALOG, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}