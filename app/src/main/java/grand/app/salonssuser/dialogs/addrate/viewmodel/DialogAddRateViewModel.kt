package grand.app.salonssuser.dialogs.addrate.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.dialogs.addrate.response.AddRateResponse
import grand.app.salonssuser.dialogs.addrate.reuqest.AddRateRequest
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DialogAddRateViewModel : BaseViewModel()
{
    var request = AddRateRequest()
    var obsRate = ObservableField<Float>(5f)

    fun onAddClicked() {
        when (request.comment) {
            null -> {
                setValue(Codes.EMPTY_MESSAGE)
            }
            else -> {
                addRate()
            }
        }
    }

    fun addRate() {
        obsIsProgress.set(true)
        request.rate = obsRate.get()!!.toInt()
        requestCall<AddRateResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().addRate(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}