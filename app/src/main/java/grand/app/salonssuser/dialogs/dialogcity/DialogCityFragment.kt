package grand.app.salonssuser.dialogs.dialogcity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.DialogCityBinding
import grand.app.salonssuser.main.selectaddress.response.CitiesResponse
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import kotlin.collections.ArrayList

class DialogCityFragment  : DialogFragment()
{
    lateinit  var  binding: DialogCityBinding
    lateinit var viewModel : DialogCityViewModel
    var citiesResponse = CitiesResponse()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.CITIES_RESPONSE) -> {
                        citiesResponse = requireArguments().getSerializable(Params.CITIES_RESPONSE) as CitiesResponse
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogCityBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogCityViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.updateList(citiesResponse.citiesList as ArrayList<CityItem>)

        observe(viewModel.adapter.itemLiveData){
            viewModel.cityItem = it as CityItem
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_CITY -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_empty_city)).show()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_CITY_REQUEST, intent)
                    requireActivity().finish()
                }
                Codes.CITY_SELECTED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.CITY_ITEM, viewModel.cityItem)
                    requireActivity().setResult(Codes.DIALOG_CITY_REQUEST, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}