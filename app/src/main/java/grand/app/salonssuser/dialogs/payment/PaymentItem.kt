package grand.app.salonssuser.dialogs.payment

data class PaymentItem(
    var id: Int? = null,
    var name: String? = null
)
