package grand.app.salonssuser.dialogs.dialogtime.day

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.ItemOrderDayBinding
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderDaysAdapter : RecyclerView.Adapter<OrderDaysAdapter.CitiesHolder>()
{
    var itemsList: ArrayList<SalonDayItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SalonDayItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderDayBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_day, parent, false)
        return CitiesHolder(binding)
    }

    override fun onBindViewHolder(holder: CitiesHolder, position: Int)
    {
        val itemViewModel = ItemDayViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setOpen()
        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                itemViewModel.item.jsonMemberSwitch != 0 -> {
                    when {
                        position != selectedPosition -> {
                            notifyItemChanged(position)
                            notifyItemChanged(selectedPosition)
                            selectedPosition = position
                            itemLiveData.value = itemViewModel.item
                        }
                    }
                }
            }
        }
    }

    fun getItem(pos:Int): SalonDayItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonDayItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CitiesHolder(val binding: ItemOrderDayBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 6
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                }
            }
        }
        fun setOpen() {
            when (getItem(adapterPosition).jsonMemberSwitch) {
                0 -> {
                    binding.dayLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.tvDayName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.secondary_color))
                    binding.tvDayNumber.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.secondary_color))
                    binding.tvMonthName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.secondary_color))
                }
            }
        }
    }
}
