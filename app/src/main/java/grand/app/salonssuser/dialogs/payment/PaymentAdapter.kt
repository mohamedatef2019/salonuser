package grand.app.salonssuser.dialogs.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemPaymentWayBinding
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class PaymentAdapter : RecyclerView.Adapter<PaymentAdapter.PaymentHolder>()
{
    var itemsList: ArrayList<PaymentItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<PaymentItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemPaymentWayBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_payment_way, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemPaymentViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): PaymentItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<PaymentItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: ItemPaymentWayBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
