package grand.app.salonssuser.dialogs.orderbooked

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonssuser.databinding.DialogOrderBookedBinding
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params

class DialogOrderBookedFragment  : DialogFragment()
{
    lateinit  var  binding: DialogOrderBookedBinding
    lateinit var orderMsg : String

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.ORDER_MSG) -> {
                        orderMsg = requireArguments().getString(Params.ORDER_MSG).toString()
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogOrderBookedBinding.inflate(inflater, container, false)

        binding.tvMessage.text = orderMsg

        binding.layoutBooked.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_ORDER_SUCCESS, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}