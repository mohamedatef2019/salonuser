package grand.app.salonssuser.dialogs.addrate.response

import com.google.gson.annotations.SerializedName

data class AddRateResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val addRateData: AddRateData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class AddRateData(

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
