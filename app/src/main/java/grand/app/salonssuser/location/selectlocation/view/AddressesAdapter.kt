package grand.app.salonssuser.location.selectlocation.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemAddressBinding
import grand.app.salonssuser.location.selectlocation.viewmodel.ItemAddressViewModel
import grand.app.salonssuser.location.util.AddressItem
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class AddressesAdapter : RecyclerView.Adapter<AddressesAdapter.AddressHolder>()
{
    var itemsList: ArrayList<AddressItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<AddressItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemAddressBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_address, parent, false)
        return AddressHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressHolder, position: Int) {
        val itemViewModel = ItemAddressViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<AddressItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class AddressHolder(val binding: ItemAddressBinding) : RecyclerView.ViewHolder(binding.root)
}
