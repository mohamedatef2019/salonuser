package grand.app.salonssuser.location.util

import android.os.Parcel
import android.os.Parcelable
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.utils.constants.Const

data class AddressItem(
    var lat: Double = Const.latitude,
    var lng: Double = Const.longitude,
    var address: String? = BaseApp.getInstance.getString(R.string.no_saved_addresses)
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "UserLocation(lat=$lat, lng=$lng, address=$address)"
    }

    companion object CREATOR : Parcelable.Creator<AddressItem> {
        override fun createFromParcel(parcel: Parcel): AddressItem {
            return AddressItem(parcel)
        }

        override fun newArray(size: Int): Array<AddressItem?> {
            return arrayOfNulls(size)
        }
    }
}