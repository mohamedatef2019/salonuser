package grand.app.salonssuser.location.selectlocation.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ActivitySelectAddressBinding
import grand.app.salonssuser.location.dialogpermission.DialogPermissionFragment
import grand.app.salonssuser.location.map.MapsActivity
import grand.app.salonssuser.location.selectlocation.viewmodel.SelectLocationViewModel
import grand.app.salonssuser.location.util.AddressItem
import grand.app.salonssuser.location.util.GpsUtils
import grand.app.salonssuser.location.util.MapUtil
import grand.app.salonssuser.location.util.PermissionUtil
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.util.*

class SelectLocationActivity : AppCompatActivity()
{
    lateinit var binding : ActivitySelectAddressBinding
    lateinit var viewModel : SelectLocationViewModel
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private lateinit var geocoder: Geocoder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_address)
        viewModel = ViewModelProvider(this).get(SelectLocationViewModel::class.java)
        binding.viewModel = viewModel

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        observe(viewModel.adapter.itemLiveData) {
            saveLocationAndCloseFragment(it as AddressItem)
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.EMPTY_ADDRESS -> {
                    Toasty.warning(this, getString(R.string.msg_empty_address)).show()
                }
                Codes.GET_LOCATION_FROM_MAP -> {
                    val intent = Intent(this, MapsActivity::class.java)
                    startActivityForResult(intent, Codes.GET_LOCATION_FROM_MAP)
                }
                Codes.GETTING_CURRENT_LOCATION -> {
                    requestLocationPermission()
                }
                Codes.ADDRESS_SAVED -> {
                    saveLocationAndCloseFragment(AddressItem(viewModel.latitude , viewModel.longitude , viewModel.obsAddress.get()))
                }
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.LOCATION_REQUEST, intent)
                    finish()
                }
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(this, Locale.forLanguageTag(PrefMethods.getLanguage()))
    }

    /* Checking the LOCATION permission state before asking the user runtime permission */
    private fun requestLocationPermission() {
        /* If user selected NEVER ASK AGAIN OR device policy prohibits the app from having that permission */
        when {
            PrefMethods.getIsPermissionDeniedForEver() -> {
                Utils.startDialogActivity(this,
                        DialogPermissionFragment::class.java.name,
                        Codes.OPEN_SETTING_DIALOG_REQUEST_CODE,
                        null)
            }
            /* If user clicked deny once Or this the first time to open the application */
            else ->
            {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        Codes.ACCESS_LOCATION_REQUEST_CODE)
            }
        }
    }

    /* Handling actions when user click on Permissions dialog */
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    )
    {
        when (requestCode)
        {
            Codes.ACCESS_LOCATION_REQUEST_CODE -> {
                // Permission is granted. Continue the action or workflow
                when {
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        // Don't call tis method from here, I handle it (ON RESUME) method to not called twice
                        openLocationFromApp()
                    }
                    else -> {
                        when {
                            ActivityCompat.shouldShowRequestPermissionRationale(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) -> {
                                // User clicked deny only once
                            }
                            else -> {
                                PrefMethods.saveIsPermissionDeniedForEver(true)
                                //Never ask again selected, or device policy prohibits the app from having that permission.
                                //So, disable that feature, or fall back to another situation...
                            }
                        }
                    }
                }
                return
            }
        }
    }



    /* When user clicked Allow to open GPS without going to setting page */
    private fun openLocationFromApp() {

        GpsUtils(this).turnGPSOn(object : GpsUtils.onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                requestLocationUpdates()
            }
        })
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient?.removeLocationUpdates(it)
        }
    }

    fun getGeoCoder(): Geocoder {
        return geocoder
    }

    /* Open app setting details page If user selected deny and don't ask again For the ACCESS_FINE_LOCATION permission */
    private fun openAppDetails() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivityForResult(intent, Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE)
    }

    private fun isGpsEnabled() : Boolean
    {
        val locationManager: LocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return when {
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) -> {
                true
            }
            else -> {
                false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /* When user select his location manually from map activity*/
        when {
            requestCode == Codes.GET_LOCATION_FROM_MAP && data != null -> {
                when {
                    data.hasExtra(Params.ADDRESS_ITEM) -> {
                        val locationItem = data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM)
                        when {
                            locationItem != null -> {
                                saveLocationAndCloseFragment(locationItem)
                            }
                        }
                    }
                } }
        }

        /* When user clicked confirm to open setting page and allow permission from there */
        when {
            requestCode == Codes.OPEN_SETTING_DIALOG_REQUEST_CODE && data != null -> {
                when {
                    data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                        when {
                            data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                openAppDetails()
                            }
                        }
                    }
                }
            }
        }

        /* When user back from setting page */
        when (requestCode) {
            Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE -> {
                when {
                    PermissionUtil.isGranted(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) -> {
                        PrefMethods.saveIsPermissionDeniedForEver(false)
                        when {
                            isGpsEnabled() -> {
                                requestLocationUpdates()
                            }
                            else -> {
                                openLocationFromApp()
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun requestLocationUpdates() {

        // Timber.e("getting location updates")
        Timber.tag("mou3az_location").e("progress true")
        viewModel.obsIsProgress.set(true)
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (10 * 1000).toLong()
        locationRequest.fastestInterval = (2 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                viewModel.obsIsProgress.set(false)
                Timber.tag("mou3az_location").e("progress false")
                if (locationResult == null) {
                    // Timber.e("couldn't get location update")
                    requestLocationPermission()
                } else {
                    // Timber.e("$locationResult")
                    if (locationResult.locations.size > 0) {

                        val location = locationResult.locations[0]
                        val userLocation = AddressItem()
                        userLocation.run {
                            lat = location!!.latitude
                            lng = location.longitude
                            address = MapUtil.getLocationAddress(getGeoCoder(),
                                    location.latitude, location.longitude)
                        }
                        saveLocationAndCloseFragment(userLocation)
                    }
                    stopLocationUpdates()
                }
                viewModel.isShown.set(false)
            }
        }
        try {
            fusedLocationClient?.requestLocationUpdates(locationRequest,
                    locationCallback,
                    Looper.getMainLooper())
        } catch (e: Exception) {
            // Timber.e(e)
        }
    }

    private fun saveLocationAndCloseFragment(addressItem: AddressItem)
    {
        when {
            !isAddressExist(addressItem) -> {
                viewModel.addressesList.add(addressItem)
                PrefMethods.saveAddressesList(viewModel.addressesList)
            }
        }

        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
        intent.putExtra(Params.ADDRESS_ITEM, addressItem)
        setResult(Codes.LOCATION_REQUEST, intent)
        finish()
    }

    private fun isAddressExist(addressItem: AddressItem) : Boolean {
        var isExist = false

        viewModel.addressesList.forEach {
            when {
                it.lat == addressItem.lat || it.lng == addressItem.lng || it.address == addressItem.address -> {
                    isExist = true
                }
            }
        }
        return isExist
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.LOCATION_REQUEST, intent)
        finish()
    }
}