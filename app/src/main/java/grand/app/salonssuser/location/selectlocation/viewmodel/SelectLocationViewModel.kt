package grand.app.salonssuser.location.selectlocation.viewmodel

import android.location.Address
import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.location.selectlocation.view.AddressesAdapter
import grand.app.salonssuser.location.util.AddressItem
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const

class SelectLocationViewModel : BaseViewModel()
{
    var adapter = AddressesAdapter()
    val obsAddress = ObservableField<String>()
    var latitude =  Const.latitude
    var longitude = Const.longitude
    var addressesList = ArrayList<AddressItem>()

    init {
        getSavedAddress()
    }

    private fun getSavedAddress() {
        when (PrefMethods.getSavedAddress()!!.size) {
            0 -> {
                obsLayout.set(LoadingStatus.EMPTY)
            }
            else -> {
                addressesList = PrefMethods.getSavedAddress() as ArrayList<AddressItem>
                 adapter.updateList(PrefMethods.getSavedAddress() as ArrayList<AddressItem>)
                obsLayout.set(LoadingStatus.FULL)
            }
        }
    }

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    fun onCurrentLocationClicked() {
        setValue(Codes.GETTING_CURRENT_LOCATION)
    }

    fun onSearchClicked() {
        setValue(Codes.GET_LOCATION_FROM_MAP)
    }
}