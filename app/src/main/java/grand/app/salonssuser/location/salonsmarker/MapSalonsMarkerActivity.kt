package grand.app.salonssuser.location.salonsmarker

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.databinding.ActivityMapSalonMarkerBinding
import grand.app.salonssuser.location.*
import grand.app.salonssuser.location.dialogpermission.DialogPermissionFragment
import grand.app.salonssuser.location.util.*
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.util.*

class MapSalonsMarkerActivity : AppCompatActivity(), OnMapReadyCallback, Observer<Any?>
{
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapSalonMarkerBinding
    private lateinit var salonsMarkerViewModel: MapSalonsMarkerViewModel
    private lateinit var geocoder: Geocoder
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    var zoomLevel : Float = 17f
    var salonId : Int = -1

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, grand.app.salonssuser.R.layout.activity_map_salon_marker)
        salonsMarkerViewModel = ViewModelProvider(this).get(MapSalonsMarkerViewModel::class.java)
        binding.viewModel = salonsMarkerViewModel

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(this, Locale.forLanguageTag(PrefMethods.getLanguage()))

        requestLocationPermission()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(grand.app.salonssuser.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        salonsMarkerViewModel.mutableLiveData.observe(this, this)

        binding.salonLayout.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            intent.putExtra(Params.SALON_ID, salonId)
            setResult(Codes.SALON_MAP_REQUEST, intent)
            finish()
        }
    }

    /* Checking the LOCATION permission state before asking the user runtime permission */
    private fun requestLocationPermission() {
        /* If user selected NEVER ASK AGAIN OR device policy prohibits the app from having that permission */
        when {
            PrefMethods.getIsPermissionDeniedForEver() -> {

                Utils.startDialogActivity(
                    this@MapSalonsMarkerActivity,
                    DialogPermissionFragment::class.java.name,
                    Codes.OPEN_SETTING_DIALOG_REQUEST_CODE,
                    null
                )
            }
            /* If user clicked deny once Or this the first time to open the application */
            else -> {
                ActivityCompat.requestPermissions(
                    this@MapSalonsMarkerActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    Codes.ACCESS_LOCATION_REQUEST_CODE
                )
            }
        }
    }

    /* Handling actions when user click on Permissions dialog */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode)
        {
            Codes.ACCESS_LOCATION_REQUEST_CODE -> {
                // Permission is granted. Continue the action or workflow
                when {
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        // Don't call tis method from here, I handle it (ON RESUME) method to not called twice
                        openLocationFromApp()
                    }
                    else -> {
                        when {
                            ActivityCompat.shouldShowRequestPermissionRationale(
                                this@MapSalonsMarkerActivity,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) -> {
                                // User clicked deny only once
                            }
                            else -> {
                                //Never ask again selected, or device policy prohibits the app from having that permission.
                                //So, disable that feature, or fall back to another situation...
                                PrefMethods.saveIsPermissionDeniedForEver(true)
                            }
                        }
                    }
                }
                return
            }
        }
    }

    /* When user clicked Allow to open GPS without going to setting page */
    private fun openLocationFromApp()
    {
        GpsUtils(this@MapSalonsMarkerActivity).turnGPSOn(object : GpsUtils.onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                requestLocationUpdates()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap)
    {
        mMap = googleMap

        val myLocation = LatLng(salonsMarkerViewModel.latitude, salonsMarkerViewModel.longitude)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, zoomLevel))

        observe(salonsMarkerViewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    Toasty.error(this, it.message.toString()).show()
                }
                Status.SUCCESS_MESSAGE -> {
                    Toasty.success(this, it.message.toString()).show()
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AllSalonsResponse -> {

                            val marker = (getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(grand.app.salonssuser.R.layout.custom_marker, null)




                            it.data.allSalonsData!!.salonsList!!.forEach {




                                mMap.addMarker(
                                    MarkerOptions().position(LatLng(it!!.lat!!.toDouble(), it.lng!!.toDouble()))
                                        .title(it.id.toString())
                                            .icon(createDrawableFromView(this@MapSalonsMarkerActivity, marker)?.let { it1 ->
                                                BitmapDescriptorFactory.fromBitmap(
                                                    it1
                                                )
                                            })
                                )
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        googleMap.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener { marker ->
            marker.hideInfoWindow()
            marker.setInfoWindowAnchor(-9999f, -9999f)

            salonsMarkerViewModel.allSalonsList.forEach {
                if (marker.title!!.toInt() == it.id)
                {


                    salonId = marker.title?.toInt() ?: 0
                    binding.addressLayout.visibility = View.VISIBLE
                    binding.tvSalonName.text = it.name
                    binding.tvSalonAddress.text = it.city?.name
                    Glide.with(this).load(it.img?.img).into(binding.imgSalon)
//                    salonsMarkerViewModel.obsSalonImg.set(it.img.toString())
//                    salonsMarkerViewModel.notifyChange()
                }
            }

            false
        })
    }

    override fun onChanged(it: Any?) {
        when (it) {
            null -> return
            else -> when (it) {

                /* When user click CURRENT LOCATION button ... Get current user location */
                Codes.GETTING_CURRENT_LOCATION -> {
                    when {
                        isPermissionGranted() -> {
                            openLocationFromApp()
                        }
                        else -> {
                            requestLocationPermission()
                        }
                    }
                }

                /* When User clicked the search toolbar */
                Codes.SEARCH_LOCATION_CLICKED -> {
                    when {
                        isPermissionGranted() -> {
                            MapUtil.openSearchPlaceScreen(this, it as Int)
                        }
                        else -> {
                            requestLocationPermission()
                        }
                    }
                }

                /* When user click confirm button ... Add this address item to Addresses list and finish the activity*/
                Codes.CONFIRM_CLICKED -> {
                    when (salonsMarkerViewModel.latitude) {
                        /*
                        * If latitude and longitude is not selected before ... get current location
                        * and if location permission is not granted .. as user to allow it then turn gps on and get current user location
                        */
                        0.0 -> {
                            when {
                                isPermissionGranted() -> {
                                    requestLocationUpdates()
                                }
                                else -> {
                                    requestLocationPermission()
                                }
                            }
                        }
                        /* If location got save it and close activity */
                        else -> {
                            val userLocation = AddressItem()
                            userLocation.run {
                                lat = salonsMarkerViewModel.latitude
                                lng = salonsMarkerViewModel.longitude
                                address = MapUtil.getLocationAddress(
                                    getGeoCoder(),
                                    salonsMarkerViewModel.latitude,
                                    salonsMarkerViewModel.longitude
                                )
                            }
                            saveLocationAndCloseActivity(userLocation)
                        }
                    }
                }

                /* When user press back button of toolbar >>> Don't save any data, JUST finish the activity*/
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    intent.putExtra(Params.SALON_DETAILS_DATA, 0)
                    setResult(Codes.SALON_MAP_REQUEST, intent)
                    finish()
                }

                Codes.CLOSE_CLICKED -> {
                    // binding.mapDialog.visibility = View.GONE
                }
            }
        }
    }

    private fun saveLocationAndCloseActivity(addressItem: AddressItem) {
        val bundle = Bundle()
        bundle.putParcelable(Params.ADDRESS_ITEM, addressItem)
        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Codes.SALON_MAP_REQUEST, intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode)
        {
            /* Back From Setting page */
            Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE -> {
                when {
                    PermissionUtil.isGranted(
                        this@MapSalonsMarkerActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) -> {
                        PrefMethods.saveIsPermissionDeniedForEver(false)
                        when {
                            isGpsEnabled() -> {
                                requestLocationUpdates()
                            }
                            else -> {
                                openLocationFromApp()
                            }
                        }
                    }
                }
            }

            /* When user clicked confirm to open setting page and allow permission from there */
            Codes.OPEN_SETTING_DIALOG_REQUEST_CODE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        openAppDetails()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun requestLocationUpdates()
    {
        salonsMarkerViewModel.isShown.set(true)
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (10 * 1000).toLong()
        locationRequest.fastestInterval = (2 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    Timber.e("couldn't get location update")
                } else {
                    Timber.e("$locationResult")
                    if (locationResult.locations.size > 0)
                    {
                        val location = locationResult.locations[0]

                        mMap.clear()
                        salonsMarkerViewModel.gotLocation(location!!, getGeoCoder())
                        salonsMarkerViewModel.latitude = location.latitude
                        salonsMarkerViewModel.longitude = location.longitude
                        salonsMarkerViewModel.obsAddress.set(
                            MapUtil.getLocationAddress(
                                getGeoCoder(),
                                location.latitude,
                                location.longitude
                            )
                        )
//                        mMap.addMarker(
//                            MarkerOptions().position(
//                                LatLng(
//                                    location.latitude,
//                                    location.longitude
//                                )
//                            )
//                        )
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    location.latitude,
                                    location.longitude
                                ), zoomLevel
                            )
                        )

                        salonsMarkerViewModel.getAllSalons()
                        stopLocationUpdates()
                    }
                }
                salonsMarkerViewModel.isShown.set(false)
            }
        }
        try {
            fusedLocationClient?.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient?.removeLocationUpdates(it)
        }
    }

    fun getGeoCoder(): Geocoder {
        return geocoder
    }

    /* Open app setting details page If user selected deny and don't ask again For the ACCESS_FINE_LOCATION permission */
    private fun openAppDetails() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE)
    }

    private fun isPermissionGranted() : Boolean {
        return when {
            !PermissionUtil.isGranted(
                this@MapSalonsMarkerActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> {
                false
            }
            else -> {
                true
            }
        }
    }

    private fun isGpsEnabled() : Boolean
    {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return when {
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) -> {
                true
            }
            else -> {
                false
            }
        }
    }

    override fun onBackPressed()
    {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.SALON_MAP_REQUEST, intent)
        finish()
    }

    private fun createDrawableFromView(context: Context, view: View): Bitmap?
    {
        val displayMetrics = DisplayMetrics()
        (context as AppCompatActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        view.layoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(
                view.measuredWidth,
                view.measuredHeight,
                Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
}