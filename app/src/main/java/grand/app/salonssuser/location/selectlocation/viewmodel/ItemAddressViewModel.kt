package grand.app.salonssuser.location.selectlocation.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.location.util.AddressItem

class ItemAddressViewModel(var item: AddressItem) : BaseViewModel()