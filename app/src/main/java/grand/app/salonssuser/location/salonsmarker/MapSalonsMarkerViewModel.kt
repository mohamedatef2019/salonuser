package grand.app.salonssuser.location.salonsmarker

import android.location.Geocoder
import android.location.Location
import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.location.util.MapUtil
import grand.app.salonssuser.main.allsalonss.home.request.HomeShopsRequest
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.main.allsalonss.response.SalonItem
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class MapSalonsMarkerViewModel : BaseViewModel()
{
    val obsAddress = ObservableField<String>()
    val obsSalonImg = ObservableField<String>()
    var latitude =  Const.latitude
    var longitude = Const.longitude
    var allSalonsResponse = AllSalonsResponse()
    var allSalonsList = arrayListOf<SalonItem>()

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    fun getAllSalons() {
        val request = HomeShopsRequest(latitude, longitude, Params.categoryid, 2)

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<AllSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHomeShops(request) }
        })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    allSalonsResponse = res
                    res.allSalonsData?.salonsList?.let {
                        allSalonsList = res.allSalonsData?.salonsList as ArrayList<SalonItem>
                    }
                    Timber.e("Mou3aaaz : " + res.allSalonsData?.salonsList?.size)
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun gotLocation(location: Location, geocoder: Geocoder) {
        obsAddress.set(MapUtil.getLocationAddress(geocoder, location.latitude, location.longitude))
        latitude = location.latitude
        longitude = location.longitude
    }
}