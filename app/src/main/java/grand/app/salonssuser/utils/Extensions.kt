package grand.app.salonssuser.utils

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.HIDE_IMPLICIT_ONLY
import android.view.inputmethod.InputMethodManager.SHOW_FORCED
import android.webkit.URLUtil
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.base.BaseActivity
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.network.ExceptionUtil.getExceptionMessage
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Mou3aaz on 14/1/2020.
 **/

fun Context.shortToast(message: String?, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.shortToast(@StringRes resId: Int, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, this.resources.getText(resId), duration).show()
}

fun Fragment.shortToast(message: String?, length: Int = Toast.LENGTH_SHORT) {
    requireActivity().shortToast(message, length)
}
fun <T : Parcelable> Bundle.getParcelableListFromArgs(key: String): ArrayList<T>? {
    return getParcelableArrayList(key)
}

fun isEmailValid(email: String): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun Fragment.shortToast(@StringRes resId: Int, length: Int = Toast.LENGTH_SHORT) {
    requireActivity().shortToast(getString(resId), length)
}

fun Fragment.longToast(message: String?) {
    requireActivity().shortToast(message, Toast.LENGTH_LONG)
}

inline fun <reified T : ViewModel> ViewModelStoreOwner.initViewModel(
    body: T.() -> Unit
): T {
    val vm = ViewModelProvider(this)[T::class.java]
    vm.body()
    return vm
}

inline fun <reified T : AppCompatActivity> Fragment.castActivity(
    callback: (T?) -> Unit
): T? {
    return if (requireActivity() is T) {
        callback(requireActivity() as T)
        requireActivity() as T
    } else {
        Timber.e("class cast exception")
        callback(null)
        null
    }
}

fun getRandomString(): String? {
    return try {
        val mPattern = "yyyy_MM_dd_HH_mm_ss.SSSSS"
        val date = Date();
        val formatter = SimpleDateFormat(mPattern, Locale.ROOT)
        val answer: String = formatter.format(date)
        answer
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

fun <T : Any> T.toRequestBodyParam(): RequestBody =
    this.toString().toRequestBody("text/plain".toMediaTypeOrNull())

inline fun <reified T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, noinline body: (T) -> Unit) {
    liveData.observe(this, Observer {
        if (lifecycle.currentState == Lifecycle.State.RESUMED) body(it)
    })
}

/*
inline fun <reified T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, noinline body: (T) -> Unit) {
    liveData.observe(this, Observer(body))
}
*/

fun String.isValidUrl(): Boolean {
    return try {
        URLUtil.isValidUrl(this) && Patterns.WEB_URL.matcher(this).matches()
    } catch (e: Exception) {
        Timber.e(e)
        false
    }
}

fun AppCompatActivity.findFragmentById(id: Int): Fragment {
    supportFragmentManager.let {
        return it.findFragmentById(id)!!
    }
}

fun Context.getActivity(): AppCompatActivity? {
    return when (this) {
        is AppCompatActivity -> this
        is Activity -> this as AppCompatActivity
        is ContextWrapper -> this.baseContext.getActivity()
        is Fragment -> this.requireActivity() as AppCompatActivity
        else -> null
    }
}

fun Context.showKeyboard(view: View, show: Boolean) {
    with(view) {
        val inputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        if (show)
            inputMethodManager.toggleSoftInput(SHOW_FORCED, HIDE_IMPLICIT_ONLY)
        else
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }
}

fun Fragment.showKeyboard(show: Boolean) {
    view?.let { activity?.showKeyboard(it, show) }
}

fun Activity.showKeyboard(show: Boolean) {
    showKeyboard(currentFocus ?: View(this), show)
}

inline fun <reified T : Any> Any.castTo(): T? {
    return if (this is T) {
        this
    } else {
        Timber.e("class cast exception")
        null
    }
}

fun Activity.restartApp() {
    showActivity(MainActivity::class.java)
    finishAffinity()
}

fun String.stringPathToFile(): File? {
    FileManager(BaseApp.getInstance.cacheDir).extractAndCompress(this)?.let {
        return it
    }?:return null
}


fun Fragment.clearAllFragments() {
    //this will clear the back stack and displays no animation on the screen
    this.parentFragmentManager.popBackStackImmediate(
            null,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
    )
}


fun <responseBody : Any?> BaseViewModel.requestCall(networkCall: suspend () -> responseBody?, callback: (responseBody?) -> Unit = {})
{
    viewModelScope.launch {
        setResult(ApiResponse.progressLoading(null))
        setResult(ApiResponse.shimmerLoading(null))
        try {
            val res = networkCall()
            callback(res)
        }
        catch (e: Exception) {

            postResult(ApiResponse.errorMessage(e.getExceptionMessage()))
        }
    }
}

fun Activity.showActivity(
        destActivity: Class<out BaseActivity>,
        intent: Intent = Intent(this, destActivity)
) {
    this.startActivity(intent)
}

fun Fragment.showActivity(
        destActivity: Class<out BaseActivity>,
        intent: Intent = Intent(this.requireActivity(), destActivity)
) {
    this.startActivity(intent)
}

fun Fragment.showActivityAndFinish(
        destActivity: Class<out MainActivity>,
        intent: Intent = Intent(this.requireActivity(), destActivity)
) {
    this.startActivity(intent)
    this.requireActivity().finish()
}

//fun File.toMultiPart(key: String): MultipartBody.Part {
//    val reqFile = asRequestBody("image/*".toMediaTypeOrNull())
//    return MultipartBody.Part.createFormData(
//            key,
//            name, // filename, this is optional
//            reqFile
//    )
//}

/*inline fun <reified T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, noinline body: (T) -> Unit) {
    liveData.observe(this, Observer {
        if (lifecycle.currentState == Lifecycle.State.RESUMED) body(it)
    })
}*/

/*fun Any.getUserData(): com.hajaty.user.model.auth.UserModel? {
    PrefMethods.getUserData()?.let {
        return it
    } ?: return null
}*/
