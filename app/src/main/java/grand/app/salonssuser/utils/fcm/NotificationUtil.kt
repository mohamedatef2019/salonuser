package grand.app.salonssuser.utils.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.RemoteMessage
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.utils.PrefMethods.getUserData
import grand.app.salonssuser.utils.constants.Codes

/**
 * Created by Mouaz Salah on 3/29/2020.
 */
object NotificationUtil {
    fun sendNotification(response: FirebaseNotficationResponse) {
        when {
            getUserData() != null -> {
                sendNow(response.body, response)
            }
        }
    }

    fun sendNotification(notification: RemoteMessage.Notification) {
        sendNow(notification.title, notification.body)
    }

    fun sendNotification(data: Map<String?, String?>) {
        sendNow(data["title"], data["body"])
    }

    fun sendNow(title: String?, body: String?) {
        val notificationBuilder = notificationBuilder
                .setContentTitle(title)
                .setContentText(body)
        notify(notificationBuilder)
    }

    private fun sendNow(body: String?, response: FirebaseNotficationResponse) {
        val notificationBuilder = getNotificationBuilder(response).setContentText(body)
        notify(notificationBuilder)
    }

    private fun getNotificationBuilder(response: FirebaseNotficationResponse): NotificationCompat.Builder {
        val builder = NotificationCompat.Builder(BaseApp.getInstance, "0")
        val intent = Intent()
        intent.putExtra(Codes.NOTIFICATION_RESPONSE, response)
        intent.setClass(BaseApp.getInstance, MainActivity::class.java)
        //        if (response.getNotificationType() != null)
//        {
//            if (response.getNotificationType() == Codes.NOTIFICATION_CHAT)
//            {
//                //chat
//            }
//            else if (response.getNotificationType() == Codes.NOTIFICATION_ADMIN)
//            {
//                //from admin
//            }
//        
//        }
        setPendingIntent(intent, builder)
        return builder
    }

    private fun setPendingIntent(intent: Intent, builder: NotificationCompat.Builder) {
        val pendingIntent = PendingIntent.getActivity(BaseApp.getInstance.applicationContext,
                System.currentTimeMillis().toInt(),
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)
        builder.setSmallIcon(R.mipmap.ic_logo)
                .setContentTitle(BaseApp.getInstance.getString(R.string.app_name))
                .setAutoCancel(true)
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        builder.setDefaults(Notification.DEFAULT_VIBRATE)
    }

    //Intent intent = new Intent(MyApplication.getAppContext(), HomeActivity.class);
    private val notificationBuilder: NotificationCompat.Builder
        private get() {
            val builder = NotificationCompat.Builder(BaseApp.getInstance, "0")
            //Intent intent = new Intent(MyApplication.getAppContext(), HomeActivity.class);
            val intent = Intent(BaseApp.getInstance, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            setPendingIntent(intent, builder)
            return builder
        }

    private fun notify(notificationBuilder: NotificationCompat.Builder) {
        val notificationManager = BaseApp.getInstance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("0",
                    "notification",
                    NotificationManager.IMPORTANCE_HIGH)
            val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            channel.enableLights(true)
            channel.enableVibration(true)
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), audioAttributes)
            notificationManager?.createNotificationChannel(channel)
        }
        notificationManager?.notify(0, notificationBuilder.build())
    }
}