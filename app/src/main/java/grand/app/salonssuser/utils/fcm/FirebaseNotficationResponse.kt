package grand.app.salonssuser.utils.fcm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FirebaseNotficationResponse(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("body")
	val body: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("chat")
	val chat: NotificationChatItem? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		TODO("chat")
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeValue(id)
		parcel.writeString(body)
		parcel.writeValue(type)
		parcel.writeString(title)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<FirebaseNotficationResponse> {
		override fun createFromParcel(parcel: Parcel): FirebaseNotficationResponse {
			return FirebaseNotficationResponse(parcel)
		}

		override fun newArray(size: Int): Array<FirebaseNotficationResponse?> {
			return arrayOfNulls(size)
		}
	}
}