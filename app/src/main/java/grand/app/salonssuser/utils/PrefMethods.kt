package grand.app.salonssuser.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import grand.app.salonssuser.auth.login.response.UserData
import grand.app.salonssuser.auth.splash.response.SplashResponse
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.location.util.AddressItem
import timber.log.Timber
import java.util.ArrayList

object Const{
   // var DEFAULT_LANG: String = Locale.getDefault().language   // Getting default mobile language
    var DEFAULT_LANG: String = "ar"
    const val APP_PREF_NAME = "PREF_SALONAT_USER"
    const val PREF_LANG = "PREF_LANG"
    const val PREF_USER_DATA = "PREF_USER_DATA"
    const val PREF_SPLASH_DATA = "PREF_SPLASH_DATA"
    const val PREF_GOOGLE_TOKEN = "PREF_GOOGLE_TOKEN"
    const val PREF_USER_ADDRESSES = "PREF_USER_ADDRESSES"
    const val PREF_USER_LOCATION = "PREF_USER_LOCATION"
    const val PREF_LOGIN_STATE = "PREF_LOGIN_STATE"
    const val PREF_COUNTRY_ID = "PREF_COUNTRY_ID"
    const val PREF_IS_PERMISSION_DENIED_FOR_EVER = "PREF_IS_PERMISSION_DENIED_FOR_EVER"
    const val PREF_IS_NOTIFICATIONS_ENABLED = "PREF_IS_NOTIFICATIONS_ENABLED"
    const val PREF_IS_FIRST_TIME = "PREF_IS_FIRST_TIME"
    const val PREF_USER_CART = "PREF_USER_CART"
}

object PrefMethods {

    private var PRIVATE_MODE = 0

    private fun getSharedPreference(): SharedPreferences {
        val appCtx = BaseApp.getInstance.applicationContext
        return appCtx.getSharedPreferences(Const.APP_PREF_NAME, PRIVATE_MODE)
    }

    private fun getSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences(Const.APP_PREF_NAME, PRIVATE_MODE)
    }

    fun getLanguage(context: Context? = null): String {
        context?.let {
            return getSharedPreference(it).getString(Const.PREF_LANG, Const.DEFAULT_LANG)!!
        } ?: return getString(Const.PREF_LANG, Const.DEFAULT_LANG)!!
    }

    fun setLanguage( value: String, context: Context?=null) {
        context?.let {
            getSharedPreference(it).edit {
                putString(Const.PREF_LANG, value)
            }
        }?: setString(Const.PREF_LANG, value)
    }

    fun getGoogleToken(context: Context? = null): String {
        context?.let {
            return getSharedPreference(it).getString(Const.PREF_GOOGLE_TOKEN, "")!!
        } ?: return getString(Const.PREF_GOOGLE_TOKEN, "")!!
    }

    fun saveGoogleToken( value: String, context: Context?=null) {
        context?.let {
            getSharedPreference(it).edit {
                putString(Const.PREF_GOOGLE_TOKEN, value)
            }
        }?: setString(Const.PREF_GOOGLE_TOKEN, value)
    }

    fun saveSplashData(data: SplashResponse?) {
        data?.let {
            val gSon = Gson()
                setString(Const.PREF_SPLASH_DATA, gSon.toJson(it))
        }
    }

    fun getSplashData(): SplashResponse? {
        getString(Const.PREF_SPLASH_DATA, null)?.let {
            val gSon = Gson()
            return gSon.fromJson(it, SplashResponse::class.java)
        } ?: return null
    }

    fun saveUserData(data: UserData?) {
        data?.let {
            val gSon = Gson()
            setString(Const.PREF_USER_DATA, gSon.toJson(it))
        }
    }

    fun getUserData(): UserData? {
        getString(Const.PREF_USER_DATA, null)?.let {
            val gSon = Gson()
            return gSon.fromJson(it, UserData::class.java)
        } ?: return null
    }

    fun saveUserLocation(data: AddressItem?) {
        data?.let {
            val gSon = Gson()
            setString(Const.PREF_USER_LOCATION, gSon.toJson(it))
        }
    }

    fun getUserLocation(): AddressItem? {
        getString(Const.PREF_USER_LOCATION, null)?.let {
            val gSon = Gson()
            return gSon.fromJson(it, AddressItem::class.java)
        } ?: return null
    }

    fun saveAddressesList(searchesList: ArrayList<AddressItem>) {
        searchesList.let {
            val gSon = Gson()
            setString(Const.PREF_USER_ADDRESSES, gSon.toJson(it))
        }
    }

    fun getSavedAddress(): ArrayList<AddressItem>?
    {
        val gson = Gson()
        val string: String? = getString(Const.PREF_USER_ADDRESSES, null)
        return if (string != null)
        {
            try {
                val type = object : TypeToken<ArrayList<AddressItem>>() {}.type
                gson.fromJson<ArrayList<AddressItem>>(string, type)
            } catch (e: Exception) {
                Timber.e(e)
                ArrayList<String>()
            } as ArrayList<AddressItem>?
        } else ArrayList()
    }

    fun getIsFirstTime(context: Context? = null): Boolean {
        context?.let {
            return getSharedPreference(it).getBoolean(Const.PREF_IS_FIRST_TIME,true)
        } ?: return getBoolean(Const.PREF_IS_FIRST_TIME, true)!!
    }

    fun setIsFirstTime( value: Boolean, context: Context? = null) {
        context?.let {
            getSharedPreference(it).edit {
                putBoolean(Const.PREF_IS_FIRST_TIME, value)
            }
        }?: setBoolean(Const.PREF_IS_FIRST_TIME, value)
    }

    fun getIsPermissionDeniedForEver(context: Context? = null): Boolean {
        context?.let {
            return getSharedPreference(it).getBoolean(Const.PREF_IS_PERMISSION_DENIED_FOR_EVER,false)
        } ?: return getBoolean(Const.PREF_IS_PERMISSION_DENIED_FOR_EVER, false)!!
    }

    fun saveIsPermissionDeniedForEver( value: Boolean, context: Context? = null) {
        context?.let {
            getSharedPreference(it).edit {
                putBoolean(Const.PREF_IS_PERMISSION_DENIED_FOR_EVER, value)
            }
        }?: setBoolean(Const.PREF_IS_PERMISSION_DENIED_FOR_EVER, value)
    }

    fun getCountryId(context: Context? = null): String {
        context?.let {
            return getSharedPreference(it).getString(Const.PREF_COUNTRY_ID,"195")!!
        } ?: return getString(Const.PREF_COUNTRY_ID, "195")!!
    }

    fun setCountryId( value: String, context: Context?=null) {
        context?.let {
            getSharedPreference(it).edit {
                putString(Const.PREF_COUNTRY_ID, value)
            }
        }?: setString(Const.PREF_COUNTRY_ID, value)
    }

    /* ------ Deleting Cash --------  */
    fun deleteUserData() {
        remove(Const.PREF_USER_DATA)
    }

    fun getString(key: String, defaultValue: String? = null): String? {
        return getSharedPreference().getString(key, defaultValue)
    }

    private fun remove(key: String) {
        getSharedPreference().edit { remove(key) }
    }

    private fun setString(key: String, value: String) {
        getSharedPreference().edit { putString(key, value) }
    }

    private fun getBoolean(key: String, defaultValue: Boolean? = null): Boolean {
        return getSharedPreference().getBoolean(key, defaultValue!!)
    }

    private fun setBoolean(key: String, value: Boolean) {
        getSharedPreference().edit { putBoolean(key, value) }
    }
}