package grand.app.salonssuser.utils.fcm

import com.google.gson.annotations.SerializedName

data class NotificationChatItem(

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("sender")
	val sender: Sender? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sender_id")
	val senderId: Int? = null
)

data class Sender(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("social_id")
	val socialId: Any? = null,

	@field:SerializedName("wallet")
	val wallet: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("jwt")
	val jwt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("firebase_token")
	val firebaseToken: String? = null,

	@field:SerializedName("salon_name")
	val salonName: Any? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
