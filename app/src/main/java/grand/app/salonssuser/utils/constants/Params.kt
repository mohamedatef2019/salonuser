package grand.app.salonssuser.utils.constants

import androidx.lifecycle.MutableLiveData

object Params
{
    var categoryid : Int = 1
    var isHome : Int = 0
    var flag : Int = 0
    var isAskedToLogin : Int = 0

    const val INTENT_PAGE_DIALOG = "INTENT_PAGE_DIALOG"
    const val BUNDLE_DIALOG = "BUNDLE_DIALOG"

    var msgLiveData = MutableLiveData<Int>()

    const val DIALOG_CONFIRM_MESSAGE = "DIALOG_CONFIRM_MESSAGE"
    const val DIALOG_CONFIRM_SUB_MESSAGE = "DIALOG_CONFIRM_SUB_MESSAGE"
    const val DIALOG_CONFIRM_POSITIVE = "DIALOG_CONFIRM_POSITIVE"
    const val DIALOG_CONFIRM_NEGATIVE = "DIALOG_CONFIRM_NEGATIVE"
    const val DIALOG_RATE_ORDER = "DIALOG_RATE_ORDER"
    const val ORDER_ID = "ORDER_ID"

    const val DIALOG_CART_ITEM = "DIALOG_CART_ITEM"
    const val DIALOG_STORE_ITEM = "DIALOG_STORE_ITEM"
    const val DIALOG_ADDRESS_ITEM = "DIALOG_ADDRESS_ITEM"
    const val DIALOG_ORDER_ITEM = "DIALOG_ORDER_ITEM"

    const val DIALOG_CLICK_ACTION = "DIALOG_CLICK_ACTION"
    const val DELIVERY_ADDRESS_EMPTY = "DELIVERY_ADDRESS_EMPTY"

    const val DIALOG_TOAST_MESSAGE = "DIALOG_TOAST_MESSAGE"
    const val DIALOG_TOAST_TYPE = "DIALOG_TOAST_TYPE"
    const val DIALOG_ORDER_SUCCESS = "DIALOG_ORDER_SUCCESS"

    const val DIALOG_SHOW = "DIALOG_SHOW"
    const val DIALOG_SEARCH_FILTER = "DIALOG_SEARCH_FILTER"
    const val STORE_PRODUCT_ITEM = "STORE_PRODUCT_ITEM"
    const val STORE_CART_ITEM = "STORE_CART_ITEM"
    const val SALON_DETAILS_DATA = "SALON_DETAILS_DATA"
    const val ORDER_TIME = "ORDER_TIME"
    const val ORDER_DATE = "ORDER_DATE"
    const val ORDER_NOTE = "ORDER_NOTE"

    const val ALL_SALONS = "ALL_SALONS"

    const val ADDED_ADDRESS = "ADDED_ADDRESS"
    const val EDITED_ADDRESS = "EDITED_ADDRESS"
    const val ADDRESS_ITEM = "ADDRESS_ITEM"
    const val FIRST_TIME = "FIRST_TIME"

    const val DELIVERY_ADDRESSES_FLAG = "DELIVERY_ADDRESSES_FLAG"
    const val STORE_TIMES_RESPONSE = "STORE_TIMES_RESPONSE"
    const val DELIVERY_TIME = "DELIVERY_TIME"
    const val VERIFIED_PHONE = "VERIFIED_PHONE"
    const val VERIFIED_CODE = "VERIFIED_CODE"
    const val ADDRESS_ID = "ADDRESS_ID"
    const val PAYMENT_WAY = "PAYMENT_WAY"
    const val ORDER_MSG = "ORDER_MSG"
    const val SERVICE_TYPE = "SERVICE_TYPE"
    const val FILTER_REQUEST = "FILTER_REQUEST"
    const val REQUEST_CODE = "REQUEST_CODE"
    const val CITIES_RESPONSE = "CITIES_RESPONSE"
    const val CITY_ITEM = "CITY_ITEM"
    const val PROVIDER_TYPE = "PROVIDER_TYPE"
    const val RATE_MESSAGE = "RATE_MESSAGE"
    const val SALON_ID = "SALON_ID"
    const val NOTIFICATION_RESPONSE = "NOTIFICATION_RESPONSE"
}