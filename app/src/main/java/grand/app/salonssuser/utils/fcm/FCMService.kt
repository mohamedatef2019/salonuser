package grand.app.salonssuser.utils.fcm

import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils.ObjectFromJson
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import timber.log.Timber

class FCMService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Timber.e("notification onMessageReceived")
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Timber.e("Message data payload: %s", remoteMessage.data)
            val data = remoteMessage.data
            Params.msgLiveData.postValue(1)
            if (data.containsKey(MAIN_KEY)) {
                val response: FirebaseNotficationResponse = ObjectFromJson(
                    data[MAIN_KEY],
                    FirebaseNotficationResponse::class.java
                )
                NotificationUtil.sendNotification(response)
                sendIntentViaBroadcast(response)
                return
            }
            //this for testing from firebase console
            NotificationUtil.sendNotification(data)
        } else if (remoteMessage.notification != null) {
            NotificationUtil.sendNotification(remoteMessage.notification!!)
        }
    }

    private fun sendIntentViaBroadcast(response: FirebaseNotficationResponse) {
        val intent = Intent() //used to receive in intent filter when register the broadcast;
        intent.action = Codes.HOME_PAGE
        intent.putExtra(Codes.NOTIFICATION_RESPONSE, response)
        when (response.type) {
            3 -> {
                // intent.putExtra(Codes.NOTIFICATION_RESPONSE, response)
            }
            2 -> {
            }
        }
        sendBroadcast(intent)
    }

    override fun onNewToken(token: String) {
        PrefMethods.saveGoogleToken(token)
        Timber.e("Refreshed token: %s", token)
    }

    companion object {
        private const val MAIN_KEY = "message"
    }
}