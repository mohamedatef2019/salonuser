package grand.app.salonssuser.activity.home

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel

class MainViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsProgressBar = ObservableField(false)
}