package grand.app.salonssuser.activity.home

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseFragment
import me.ibrahimsn.lib.SmoothBottomBar
import timber.log.Timber

open class BaseHomeFragment : BaseFragment()
{
    override fun onDestroyView() {
        super.onDestroyView()
        when {
            !isNavigated -> requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                when {
                    navController.currentBackStackEntry?.destination?.id != null -> {
                        findNavController().navigateUp()
                    }
                    else -> navController.popBackStack()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
    }

    fun setBarName(title: String)
    {
        (requireActivity() as MainActivity).run {
            viewModel!!.obsTitle.set(title)
        }
    }
    
    fun showBottomBar(show: Boolean)
    {
        try
        {
            val navView: SmoothBottomBar = requireActivity().findViewById(R.id.bottomBar)
            when {
                show -> {
                    navView.visibility = View.VISIBLE
                }
                else -> navView.visibility = View.GONE
            }
        }
        catch (e: Exception)
        {
            Timber.e(e)
        }

//        try
//        {
//            val navView: BottomNavigationView = requireActivity().findViewById(R.id.bottomBar)
//            when {
//                show -> {
//                    navView.visibility = View.VISIBLE
//                }
//                else -> navView.visibility = View.GONE
//            }
//        }
//        catch (e: Exception)
//        {
//            Timber.e(e)
//        }
    }
}

