package grand.app.salonssuser.activity.auth

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel

class AuthViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsShowSkipBtn = ObservableBoolean()
    val obsProgressBar = ObservableField<Boolean>()
}