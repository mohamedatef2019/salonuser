package grand.app.salonssuser.activity.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseActivity
import grand.app.salonssuser.databinding.ActivityMainBinding
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.fcm.FirebaseNotficationResponse
import timber.log.Timber

class MainActivity : BaseActivity()
{
    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
   // private lateinit var navView: BottomNavigationView
    var viewModel: MainViewModel? = null
    private lateinit var navController: NavController
    var response: FirebaseNotficationResponse? = null
    var chatMutableLiveData = MutableLiveData<FirebaseNotficationResponse>()

    private val localBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent != null) {
                if (intent.hasExtra(Params.NOTIFICATION_RESPONSE)) {
                    if (intent.getParcelableExtra<Parcelable>(Params.NOTIFICATION_RESPONSE) is FirebaseNotficationResponse) {
                        val notficationResponse: FirebaseNotficationResponse = intent.getParcelableExtra(Codes.NOTIFICATION_RESPONSE)!!
                        if (notficationResponse.type != null && notficationResponse.type == 1) {
                            chatMutableLiveData.value = notficationResponse
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //init view model
        initViewModel()
        binding.viewModel = viewModel

       // navView = findViewById(R.id.bottomBar)
        val popupMenu = PopupMenu(this, null)
        popupMenu.inflate(R.menu.bottom_nav_menu)
        val menu = popupMenu.menu
        navController = findNavController(R.id.nav_home_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_home, R.id.navigation_favorite, R.id.navigation_chats, R.id.navigation_profile, R.id.navigation_more))
       // navView.setupWithNavController(navController)
        binding.bottomBar.setupWithNavController(menu, navController)

        Utils.requestFCMToken()

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_home -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }
        setUpToolbarAndStatusBar()
        registerReceiver(localBroadcastReceiver, IntentFilter(Codes.HOME_PAGE))
        getData(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.bottom_nav_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> {
                Timber.e("Mou3aaaaaz home")
            }
            R.id.navigation_favorite -> {
                Timber.e("Mou3aaaaaz favorites")
            }
            R.id.navigation_more -> {
                Timber.e("Mou3aaaaaz more")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        getData(intent)
    }

    private fun getData(intent: Intent) {
        if (intent.hasExtra(Codes.NOTIFICATION_RESPONSE)) {
            response = intent.getParcelableExtra(Codes.NOTIFICATION_RESPONSE)
            if (response != null) {
                Timber.e("" + response.toString())
            }
        }
    }

    private fun setUpToolbarAndStatusBar()
    {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_salon_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_salon_details_more -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_home -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_all_salons -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_all_salons))
                }
                R.id.navigation_search -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_search))
                }
                R.id.navigation_chats -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_chat))
                }
                R.id.navigation_rates -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_rates))
                }
                R.id.navigation_order_review -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                    viewModel?.obsTitle!!.set(getString(R.string.title_order_review))
                }
                R.id.navigation_favorite -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_favorite))
                }
                R.id.navigation_change_pass -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_change_password))
                }
                R.id.navigation_select_address -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_select_address))
                }
                R.id.navigation_bookings -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_my_bookings))
                }
                R.id.navigation_booking_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                    viewModel?.obsTitle!!.set(getString(R.string.title_booking_details))
                    binding.bottomBar.visibility = View.VISIBLE
                }
                R.id.navigation_profile -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_profile))
                }
                R.id.navigation_more -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_more))
                }
                R.id.navigation_messages -> {
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_contact_us -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_contact_us))
                }
                R.id.navigation_terms, R.id.navigation_about_app, R.id.navigation_wallet, R.id.navigation_add_salon -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_edit_profile -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                else -> {
                    viewModel?.obsShowToolbar!!.set(true)
                    customBarColor(ContextCompat.getColor(this, R.color.white))
                }
            }
        }
    }
    private fun initViewModel() {
        if (viewModel == null) {
           // viewModel = ViewModelProvider(this@MainActivity).get(MainViewModel::class.java)
            viewModel = MainViewModel()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_home_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun customBarColor(color: Int) {

        binding.toolbar.setBackgroundColor(color)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = color
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        when (navController.currentDestination!!.id) {
            R.id.navigation_salon_details -> {
                Params.flag = 1
            }
        }
    }
}