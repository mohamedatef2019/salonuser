package grand.app.salonssuser.activity.auth

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import grand.app.salonssuser.base.BaseActivity
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.databinding.ActivityAuthBinding
import grand.app.salonssuser.utils.LocalUtil
import grand.app.salonssuser.utils.constants.Const
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AuthActivity : BaseActivity()
{
    lateinit var binding : ActivityAuthBinding
    var viewModel: AuthViewModel? = null
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?)
    {
        LocalUtil.changeLanguage(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth)

        //init view model
        initViewModel()
        binding.viewModel = viewModel
        navController = findNavController(R.id.nav_auth_host_fragment)

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_login -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }

        viewModel!!.clickableLiveData.observe(this) {
            viewModel!!.obsIsClickable.set(false)
            lifecycleScope.launch {
                delay(2000)
                viewModel!!.clickableLiveData.value = false
                viewModel!!.obsIsClickable.set(true)
            }
        }

         setUpToolbarAndStatusBar()

        binding.btnSkip.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finishAffinity()
        }

        when {
            intent.hasExtra(Const.ACCESS_LOGIN) -> {
                when {
                    intent.getBooleanExtra(Const.ACCESS_LOGIN, false) -> {
                        navController.navigate(R.id.navigation_login)
                    }
                }
            }
        }
    }

    private fun initViewModel() {
        when (viewModel) {
            null -> {
                //viewModel = ViewModelProvider(this@AuthActivity).get(AuthViewModel::class.java)
                viewModel = AuthViewModel()
            }
        }
    }

    private fun setUpToolbarAndStatusBar() {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_splash, R.id.navigation_intro_one, R.id.navigation_intro_two, R.id.navigation_intro_three -> {
                    viewModel?.obsShowToolbar!!.set(false)
                    viewModel?.obsShowSkipBtn!!.set(false)
                }
                R.id.navigation_login, R.id.navigation_register -> {
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsShowSkipBtn!!.set(true)
                    customBarColor(ContextCompat.getColor(this, R.color.white))
                }
                else -> {
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsShowSkipBtn!!.set(false)
                    customBarColor(ContextCompat.getColor(this, R.color.white))
                }
            }
        }
    }

    private fun customBarColor(color: Int) {

        binding.toolbar.setBackgroundColor(color)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = color
            }
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()

        when (navController.currentDestination!!.id) {
            R.id.navigation_login, R.id.navigation_intro_one -> {
                finishAffinity()
            }
        }
    }
}