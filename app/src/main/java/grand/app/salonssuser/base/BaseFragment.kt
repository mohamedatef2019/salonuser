package grand.app.salonssuser.base

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Options
import com.fxn.pix.Pix
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.location.util.PermissionUtil
import grand.app.salonssuser.utils.observe

open class BaseFragment : Fragment()
{
    var isNavigated = false

    fun navigateWithAction(action: NavDirections) {
        isNavigated = true
        findNavController().navigate(action)
    }

    fun navigate(resId: Int) {
        isNavigated = true
        findNavController().navigate(resId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (!isNavigated)
            requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                if (navController.currentBackStackEntry?.destination?.id != null)
                {
                    findNavController().navigateUp()
                }
                else
                    navController.popBackStack()
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
    }

    fun showToast(msg : String, type : Int) {

        when (type) {
            1 -> {
                Toasty.error(requireActivity(), msg).show()
            }
            else -> {
                Toasty.success(requireActivity(), msg).show()
            }
        }
    }

    fun pickImage(requestCode: Int) {
        val options = Options.init()
                .setRequestCode(requestCode) //Request code for activity results
                .setFrontfacing(false) //Front Facing camera on start
                .setExcludeVideos(true) //Option to exclude videos
        if (PermissionUtil.hasImagePermission(requireActivity())) {
            Pix.start(this, options)
        } else {
            observe(
                PermissionUtil.requestPermission(
                    requireActivity(),
                    PermissionUtil.getImagePermissions()
            )
            ) {
                when (it) {
                    PermissionUtil.AppPermissionResult.AllGood -> Pix.start(
                            this,
                            options
                    )
                }
            }
        }
    }
}

