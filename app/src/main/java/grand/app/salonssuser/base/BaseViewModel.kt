package grand.app.salonssuser.base

import androidx.databinding.Observable
import androidx.databinding.Observable.OnPropertyChangedCallback
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import androidx.lifecycle.viewModelScope
import grand.app.salonssuser.network.ApiRepo
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.network.RetrofitBuilder
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.reflect.KParameter

open class BaseViewModel : ViewModel(), Observable
{
    fun getApiRepo(): ApiRepo = ApiRepo(RetrofitBuilder.API_SERVICE)

    val obsLayout = ObservableField<LoadingStatus>()
    var obsIsProgress = ObservableBoolean()

    // for network - Api Response
    val apiResponseLiveData = MutableLiveData<ApiResponse<Any?>>()

    val clickableLiveData = SingleLiveEvent<Boolean>().apply { postValue(true)}
    var obsIsClickable = ObservableBoolean(true)

    val mutableLiveData = SingleLiveEvent<Any?>()

    private val message = ObservableField<String>()
    private val mCallBacks: PropertyChangeRegistry = PropertyChangeRegistry()
    var isShownOld = ObservableBoolean()
    var isShown = ObservableBoolean()
    var isConfirmShown = ObservableBoolean()

    var currentPage : Int = 1
    var PAGE_START : Int = 0
    var totalPages : Int = 0
    var canLoadMore : Boolean = false

    fun setValue(item: Any?) {
        mutableLiveData.value = item
        mutableLiveData.value = null
    }

    fun postValue(item: Any?) {
        mutableLiveData.postValue(item)
        mutableLiveData.postValue(null)
    }

    fun onEyeOldClicked() {
        if (isShownOld.get())
        {
            isShownOld.set(false)
        }
        else
        {
            isShownOld.set(true)
        }
    }

    fun onEyeClicked() {
        if (isShown.get())
        {
            isShown.set(false)
        }
        else
        {
            isShown.set(true)
        }
    }

    fun onConfirmEyeClicked() {
        if (isConfirmShown.get())
        {
            isConfirmShown.set(false)
        }
        else
        {
            isConfirmShown.set(true)
        }
    }

    protected open fun getDouble(value: String?): Double {
        return value?.toDouble() ?: 0.0
    }

    fun getMessage(): String? {
        return message.get()
    }

    fun setMessage(message: Any) {
        this.message.set(message.toString())
    }

    fun setMessageFromRes(stringRes: Int) {
        message.set(getString(stringRes))
    }

    fun getString(stringRes: Int): String {
        return BaseApp.getInstance.resources.getString(stringRes)
    }

    fun getStringArray(resArray: Int): Array<String> {
        return BaseApp.getInstance.resources.getStringArray(resArray)
    }

    fun accessLoadingBar(visible: Int) {
        setValue(visible)
    }

    override fun addOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
        mCallBacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
        mCallBacks.remove(callback)
    }

    fun notifyChange() {
        mCallBacks.notifyChange(this, 0)
    }

    fun notifyChange(propertyId: Int) {
        mCallBacks.notifyChange(this, propertyId)
    }

    fun setClickable() {
        obsIsClickable.set(false)
        viewModelScope.launch {
            delay(2000)
            obsIsClickable.set(true)
        }
    }

    fun setResult(o: ApiResponse<Any?>?) {
        apiResponseLiveData.value = o
    }

    fun postResult(o: ApiResponse<Any?>?) {
        apiResponseLiveData.postValue(o)
    }

}