package grand.app.salonssuser.auth.resetpass.request

data class ChangePassRequest (
        var phone: String? = null,
        var password: String? = null,
        var old_password: String? = null
)