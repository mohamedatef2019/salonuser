package grand.app.salonssuser.auth.forgotpass.request

data class ForgotRequest (
    var phone: String? = null,
    var type: Int = 0)