package grand.app.salonssuser.auth.login.view

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64.DEFAULT
import android.util.Base64.encodeToString
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.auth.login.response.LoginResponse
import grand.app.salonssuser.auth.login.response.SocialLoginResponse
import grand.app.salonssuser.auth.login.viewmodel.LoginViewModel
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.FragmentLoginBinding
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import org.json.JSONException
import timber.log.Timber
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class LoginFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var loginBinding: FragmentLoginBinding
    lateinit var loginViewModel: LoginViewModel
    var mGoogleSignInClient: GoogleSignInClient? = null
    val RC_SIGN_IN = 1001
    var social_id: String? = ""
    var email:String? = ""
    var name:String? = ""
    var image:String? = ""

    private var callbackManager: CallbackManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        loginBinding = DataBindingUtil.inflate(inflater, grand.app.salonssuser.R.layout.fragment_login, container, false)
        return loginBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
       // loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        loginViewModel = LoginViewModel()
        loginBinding.loginViewModel = loginViewModel
        loginViewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            when {
                !task.isSuccessful -> {
                    Timber.tag("fcm_token").e(task.exception)
                    return@addOnCompleteListener
                }
                task.result != null -> {
                    loginViewModel.loginRequest.firebase_token = task.result!!
                    loginViewModel.socialRequest.firebase_token = task.result!!
                    Timber.tag("fcm_token").e(task.result!!)
                }
            }
        }

        initGoogle()

        observe(loginViewModel.apiResponseLiveData) {
            showProgressBar(false)
            when (it.status) {
                Status.PROGRESS_LOADING -> {
                    when (it.data) {
                        false -> {
                            showProgressBar(false)
                        }
                        else -> {
                            showProgressBar(true)
                        }
                    }
                }
                Status.ERROR_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    showProgressBar(false)
                    when (it.data) {
                        is LoginResponse -> {
                            when (it.data.code) {
                                405 -> {
                                    val action = LoginFragmentDirections.loginToVerify(loginViewModel.loginRequest.loginkey!!, Codes.REGISTER_INTENT)
                                    findNavController().navigate(action)
                                }
                                else -> {
                                    PrefMethods.saveUserData(it.data.userData)
                                    when (Params.isAskedToLogin) {
                                        1 -> {
                                            requireActivity().finish()
                                        }
                                        else -> {
                                            requireActivity().finishAffinity()
                                            startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        }
                                    }
                                }
                            }
                        }
                        is SocialLoginResponse -> {
                            when (it.data.code) {
                                    405 -> {
                                    val action = LoginFragmentDirections.loginToVerify(
                                            loginViewModel.loginRequest.loginkey!!,
                                            Codes.REGISTER_INTENT
                                    )
                                    findNavController().navigate(action)
                                }
                                else -> {
                                    PrefMethods.saveUserData(it.data.socialData)
                                    when (Params.isAskedToLogin) {
                                        1 -> {
                                            requireActivity().finish()
                                        }
                                        else -> {
                                            requireActivity().finishAffinity()
                                            startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else -> {
                    showProgressBar(false)
                    Timber.e(it.message)
                }
            }
        }

        printHashKey()

        callbackManager = CallbackManager.Factory.create();
        val btnLoginFacebook = loginBinding.btnFacebook
        btnLoginFacebook.setPermissions(listOf("public_profile", "email"))
        btnLoginFacebook.fragment = this

        btnLoginFacebook.registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {

                    override fun onSuccess(loginResult: LoginResult?) {
                        Timber.tag("social_login").e("onSuccess")

                        //  val accessToken = AccessToken.getCurrentAccessToken()
                        //  val isLoggedIn = accessToken != null && !accessToken.isExpired

                        Timber.tag("social_login")
                                .e("accesstoken : " + AccessToken.getCurrentAccessToken())
                        Timber.tag("social_login").e("login token : " + loginResult?.accessToken)

                        // App code
                        Log.d("MainActivity", "Facebook token: " + loginResult!!.accessToken.token)

                        val access_token = loginResult.accessToken
                        var facebookToken = access_token?.getToken()
                        Timber.d(access_token.toString())
                        val request = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(),
                                GraphRequest.GraphJSONObjectCallback { `object`, response ->
                                    val json = response.jsonObject
                                    try {
                                        if (json != null) {
                                            Timber.d(json.toString())
                                            try {
                                                email = json.getString("email")
                                                // val loginRequest = LoginRequest(email, facebookToken, firebaseToken)
                                            } catch (e: Exception) {
                                                Toast.makeText(
                                                        BaseApp.getInstance,
                                                        "Sorry!!! Your email is not verified on facebook.",
                                                        Toast.LENGTH_LONG
                                                ).show()
                                                return@GraphJSONObjectCallback
                                            }
                                            val facebook_uid: String = json.getString("id")
                                            social_id = json.getString("id")
                                            val first_name = json.getString("first_name")
                                            val last_name = json.getString("last_name")
                                            name = json.getString("name")
                                            val picture =
                                                    "https://graph.facebook.com/" + facebook_uid.toString() + "/picture?type=large"

                                            loginViewModel.socialRequest.social_id = social_id
                                            loginViewModel.socialRequest.name =
                                                    first_name + " " + last_name
                                            loginViewModel.socialRequest.img = picture

                                            loginViewModel.socialLogin()
                                            Timber.e("Mou3aaaz: " + social_id)
                                            Timber.e("Mou3aaaz: " + first_name)
                                            Timber.e("Mou3aaaz: " + name)
                                            Timber.e("Mou3aaaz: " + picture)
                                        }
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                        Log.d("response problem", "problem" + e.message)
                                    }
                                })
                        val parameters = Bundle()
                        parameters.putString(
                                "fields",
                                "id,name,first_name,last_name,link,email,picture.type(large)"
                        )
                        request.parameters = parameters
                        request.executeAsync()
                    }

                    override fun onCancel() {
                        Timber.tag("social_login").e("onCancel")
                    }

                    override fun onError(error: FacebookException) {
                        Timber.tag("social_login").e("onError : " + error.cause)
                        Timber.tag("social_login").e("onError : " + error)
                        Timber.tag("social_login").e("onError : " + error.message)
                    }
                })

        loginBinding.ivFacebookLogin.setOnClickListener {
            LoginManager.getInstance().logOut()
            btnLoginFacebook.performClick()
        }

        val signInButton: SignInButton = loginBinding.btnGoogle
        loginBinding.ivGoogleLogin.setOnClickListener {
            signIn()
            signInButton.performClick()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) {
            Timber.tag("social_login").e("on activity result")
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }

        callbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        Timber.tag("social_login").e("handel sign in result")

        var account: GoogleSignInAccount? = null
        try {
            account = completedTask.getResult(ApiException::class.java)
            Timber.tag("social_login").e("account : " + account.toString())
        } catch (e: ApiException) {
            Timber.tag("social_login").e("exception : " + e.cause)
            e.printStackTrace()
        }
        if (account != null) {
            Timber.tag("social_login").e("account != null")
            if (account.id != null) social_id = account.id
            if (account.email != null) {
                email = account.email
            }
            if (account.displayName != null) name = account.displayName
            if (account.photoUrl != null) {
                Log.e("image", account.photoUrl.toString())
                image = account.photoUrl.toString()
            } else {
                image = "https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png"
            }

            loginViewModel.socialRequest.social_id = social_id
            loginViewModel.socialRequest.name = name
            loginViewModel.socialRequest.img = image
            loginViewModel.socialLogin()
        }
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun initGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestId()
            .requestEmail()
            .requestProfile()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(BaseApp.getInstance, gso)
    }

    override fun onChanged(it: Any?) {
        when (it) {
            null -> return
            else -> when (it) {
                Codes.EMPTY_PHONE -> {
                    showToast(getString(grand.app.salonssuser.R.string.msg_empty_login_key), 1)
                }
                Codes.INVALID_PHONE -> {
                    showToast(getString(grand.app.salonssuser.R.string.msg_invalid_phone), 1)
                }
                Codes.PASSWORD_EMPTY -> {
                    showToast(getString(grand.app.salonssuser.R.string.msg_empty_password), 1)
                }
                Codes.PASSWORD_SHORT -> {
                    showToast(getString(grand.app.salonssuser.R.string.msg_invalid_password), 1)
                }
                Codes.REGISTER_INTENT -> {
                    findNavController().navigate(grand.app.salonssuser.R.id.login_to_register)
                }
                Codes.FORGOT_INTENT -> {
                    findNavController().navigate(grand.app.salonssuser.R.id.login_to_forgot_password)
                }
            }
        }
    }

    fun printHashKey() {
        try {
            val info: PackageInfo = BaseApp.getInstance.packageManager.getPackageInfo(
                    BaseApp.getInstance.applicationContext.getPackageName(),
                    PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Timber.d("KeyHash: " + encodeToString(md.digest(), DEFAULT));
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }
}