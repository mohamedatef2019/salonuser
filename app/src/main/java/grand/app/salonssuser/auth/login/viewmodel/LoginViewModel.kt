package grand.app.salonssuser.auth.login.viewmodel

import grand.app.salonssuser.auth.login.request.LoginRequest
import grand.app.salonssuser.auth.login.request.SocialLoginRequest
import grand.app.salonssuser.auth.login.response.LoginResponse
import grand.app.salonssuser.auth.login.response.SocialLoginResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class LoginViewModel : BaseViewModel()
{
    var loginRequest = LoginRequest()
    var socialRequest = SocialLoginRequest()

    fun onLoginClicked() {
        setClickable()

        when {
            loginRequest.loginkey.isNullOrEmpty() || loginRequest.loginkey.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            loginRequest.password.isNullOrEmpty() || loginRequest.password.isNullOrBlank() -> {
                setValue(Codes.PASSWORD_EMPTY)
            }
            loginRequest.password!!.length < 6 -> {
                setValue(Codes.PASSWORD_SHORT)
            }
            else -> {
                login()
            }
        }
    }

    fun login() {

        obsIsProgress.set(true)
        requestCall<LoginResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().login(loginRequest) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun socialLogin() {

        Timber.e("firebase_token : " + socialRequest.firebase_token.toString())
        obsIsProgress.set(true)
        requestCall<SocialLoginResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().socialLogin(socialRequest) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onForgotClicked() {
        setClickable()
        setValue(Codes.FORGOT_INTENT)
    }

    fun onRegisterClicked() {
        setClickable()
        setValue(Codes.REGISTER_INTENT)
    }
}