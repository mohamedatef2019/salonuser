package grand.app.salonssuser.auth.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.base.BaseFragment
import grand.app.salonssuser.databinding.FragmentIntroTwoBinding
import grand.app.salonssuser.utils.PrefMethods

class IntroSecondFragment : BaseAuthFragment() {
    lateinit var binding: FragmentIntroTwoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro_two, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvIntroTitle.text = PrefMethods.getSplashData()!!.slidersList!![1]!!.title
        binding.tvIntroDescription.text = PrefMethods.getSplashData()!!.slidersList!![1]!!.desc
        Glide.with(this).load(PrefMethods.getSplashData()!!.slidersList!![1]!!.img).into(binding.imgIntro)

        binding.btnIntroNext.setOnClickListener {
            findNavController().navigate(R.id.intro_two_to_three)
        }

        binding.btnIntroBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}