package grand.app.salonssuser.auth.login.request

data class UpdateFirebaseRequest (
    var firebase_token: String? = null
)