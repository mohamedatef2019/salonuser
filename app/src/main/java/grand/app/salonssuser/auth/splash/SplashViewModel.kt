package grand.app.salonssuser.auth.splash

import grand.app.salonssuser.auth.splash.response.SplashResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SplashViewModel : BaseViewModel()
{
    init {
        getSliders()
    }

    fun getSliders()
    {
        requestCall<SplashResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().splash() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }
}