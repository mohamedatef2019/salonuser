package grand.app.salonssuser.auth.resetpass.response

import com.google.gson.annotations.SerializedName

data class ResetPassResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
