package grand.app.salonssuser.auth.splash.response

import com.google.gson.annotations.SerializedName

data class SplashResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val slidersList: List<SplashSliderItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class SplashSliderItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
