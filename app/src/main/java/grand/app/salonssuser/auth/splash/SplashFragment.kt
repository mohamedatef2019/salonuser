package grand.app.salonssuser.auth.splash

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.auth.splash.response.SplashResponse
import grand.app.salonssuser.databinding.FragmentSplashBinding
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class SplashFragment : BaseAuthFragment()
{
    lateinit var binding: FragmentSplashBinding
    lateinit var viewModel: SplashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        viewModel = SplashViewModel()

        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getSliders()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SplashResponse -> {
                            PrefMethods.saveSplashData(it.data)
                            takeAction()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

     fun takeAction() {
         when {
             /**
              * User logged in
              */
             PrefMethods.getIsFirstTime() -> {
                 findNavController().navigate(R.id.splash_to_intro)
             }
             /**
              * If user open the app for the first time
              * */
             else -> {
                 when {
                     PrefMethods.getUserData() != null -> {
                         startActivity(Intent(requireActivity(), MainActivity::class.java))
                         requireActivity().finishAffinity()
                     }
                     else -> {
                         findNavController().navigate(R.id.splash_to_login)
                     }
                 }
             }
         }
    }
}