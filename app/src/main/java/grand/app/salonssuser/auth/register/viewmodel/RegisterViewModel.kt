package grand.app.salonssuser.auth.register.viewmodel

import grand.app.salonssuser.auth.register.request.RegisterRequest
import grand.app.salonssuser.auth.register.response.RegisterResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.isEmailValid
import grand.app.salonssuser.utils.requestCall
import grand.app.salonssuser.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RegisterViewModel : BaseViewModel()
{
    var request = RegisterRequest()

    fun onRegisterClicked()
    {
        setClickable()
        when {
            request.userImg == null -> {
                setValue(Codes.EMPTY_USER_IMAGE)
            }
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                setValue(Codes.EMPTY_NAME)
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                setValue(Codes.EMAIL_EMPTY)
            }
            !isEmailValid(request.email!!) -> {
                setValue(Codes.INVALID_EMAIL)
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                setValue(Codes.PASSWORD_EMPTY)
            }
            request.password!!.length < 6 -> {
                setValue(Codes.PASSWORD_SHORT)
            }
            else -> {
                register()
            }
        }
    }

    fun onChooseImgClicked() {
        setClickable()
        setValue(Codes.CHOOSE_IMAGE_CLICKED)
    }

    fun onLoginClicked() {
        setClickable()
        setValue(Codes.LOGIN_INTENT)
    }

    fun register() {
        request.type = 0
        request.firebase_token = ""

        obsIsProgress.set(true)
        requestCall<RegisterResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().register(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.USER_PROFILE_IMAGE_REQUEST -> {
                request.userImg = path.stringPathToFile()
            }
        }
    }
}