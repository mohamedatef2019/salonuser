package grand.app.salonssuser.auth.forgotpass.response

import com.google.gson.annotations.SerializedName

data class ForgotResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
