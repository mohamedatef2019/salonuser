package grand.app.salonssuser.auth.verifycode.viewmodel

import android.os.CountDownTimer
import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.auth.forgotpass.request.ForgotRequest
import grand.app.salonssuser.auth.forgotpass.response.ForgotResponse
import grand.app.salonssuser.auth.verifycode.request.VerifyRequest
import grand.app.salonssuser.auth.verifycode.response.VerifyResponse
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class VerifyViewModel : BaseViewModel()
{
    var request = VerifyRequest()
    var obsTimer = ObservableField<String>()
    var obsClickable = ObservableField(false)

    fun onSendClicked()
    {
        setClickable()
        when {
            request.code.isNullOrEmpty() || request.code.isNullOrBlank() -> {
                setValue(Codes.EMPTY_CODE)
            }
            request.code!!.length < 4 -> {
                setValue(Codes.SHORT_CODE)
            }
            else -> {
                verifyCode()
            }
        }
    }

    fun onResendClicked() {
        setClickable()
        resendCode()
        startTimer()
    }

    private fun startTimer()
    {
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                obsTimer.set("${"00:"}${millisUntilFinished / 1000}")
                obsClickable.set(false)
            }
            override fun onFinish() {
                obsTimer.set(BaseApp.getInstance.getString(R.string.label_time_zero) )
                obsClickable.set(true)
            }
        }.start()
    }

    init {
        startTimer()
    }

    private fun verifyCode()
    {
        obsIsProgress.set(true)
        requestCall<VerifyResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().verifyCode(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    private fun resendCode()
    {
        obsIsProgress.set(true)
        requestCall<ForgotResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().forgotPassword(ForgotRequest(request.phone)) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}