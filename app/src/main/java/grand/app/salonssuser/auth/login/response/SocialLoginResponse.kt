package grand.app.salonssuser.auth.login.response

import com.google.gson.annotations.SerializedName

data class SocialLoginResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val socialData: UserData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class SocialLoginData(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("social_id")
	val socialId: String? = null,

	@field:SerializedName("jwt")
	val jwt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("firebase_token")
	val firebaseToken: String? = null,

	@field:SerializedName("type")
	val type: Int? = null
)
