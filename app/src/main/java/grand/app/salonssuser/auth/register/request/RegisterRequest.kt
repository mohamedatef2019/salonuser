package grand.app.salonssuser.auth.register.request

import java.io.File

data class RegisterRequest (
        var name: String? = null,
        var email: String? = null,
        var password: String? = null,
        var firebase_token: String? = null,
        var phone: String? = null,
        var type: Int = 0,
        var userImg: File? = null
        )