package grand.app.salonssuser.auth.register.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Pix
import com.google.firebase.messaging.FirebaseMessaging
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.auth.register.response.RegisterResponse
import grand.app.salonssuser.auth.register.viewmodel.RegisterViewModel
import grand.app.salonssuser.databinding.FragmentRegisterBinding
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class RegisterFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding : FragmentRegisterBinding
    lateinit var viewModel : RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
      //  viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        viewModel = RegisterViewModel()
        binding.viewModel = viewModel
        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            when {
                !task.isSuccessful -> {
                    Timber.tag("fcm_token").e(task.exception)
                    return@addOnCompleteListener
                }
                task.result != null -> {
                    viewModel.request.firebase_token = task.result!!
                    Timber.tag("fcm_token").e(task.result!!)
                }
            }
        }


        observe(viewModel.apiResponseLiveData) {
            showProgressBar(false)
            when (it.status) {
                Status.PROGRESS_LOADING -> {
                    when (it.data) {
                        false -> {
                            showProgressBar(false)
                        }
                        else -> {
                            showProgressBar(true)
                        }
                    }
                }
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is RegisterResponse -> {
                            val action = RegisterFragmentDirections.registerToVerify(
                                viewModel.request.phone!!,
                                Codes.REGISTER_INTENT
                            )
                            findNavController().navigate(action)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?)
    {
        if(it == null) return
        when (it) {
            Codes.EMPTY_USER_IMAGE -> {
                showToast(getString(R.string.msg_empty_user_image), 1)
            }
            Codes.CHOOSE_IMAGE_CLICKED -> {
                pickImage(Codes.USER_PROFILE_IMAGE_REQUEST)
            }
            Codes.LOGIN_INTENT -> {
                findNavController().navigate(R.id.register_to_login)
            }
            Codes.EMPTY_NAME -> {
                showToast(getString(R.string.msg_empty_name), 1)
            }
            Codes.INVALID_EMAIL -> {
                showToast(getString(R.string.msg_invalid_email), 1)
            }
            Codes.EMAIL_EMPTY -> {
                showToast(getString(R.string.msg_empty_email), 1)
            }
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_phone), 1)
            }
            Codes.INVALID_PHONE -> {
                showToast(getString(R.string.msg_invalid_phone), 1)
            }
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password), 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password), 1)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        showProgressBar(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.USER_PROFILE_IMAGE_REQUEST -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                if (requestCode == Codes.USER_PROFILE_IMAGE_REQUEST) {
                                    val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                                    binding.ivUserImg.setImageURI(returnValue[0].toUri())
                                    viewModel.gotImage(requestCode, array[0])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}