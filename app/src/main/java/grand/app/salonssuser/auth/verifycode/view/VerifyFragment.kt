package grand.app.salonssuser.auth.verifycode.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.auth.verifycode.response.VerifyResponse
import grand.app.salonssuser.auth.verifycode.viewmodel.VerifyViewModel
import grand.app.salonssuser.databinding.FragmentVerifyCodeBinding
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class VerifyFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentVerifyCodeBinding
    lateinit var viewModel: VerifyViewModel
    private val fragmentArgs : VerifyFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_code, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(VerifyViewModel::class.java)
        viewModel = VerifyViewModel()
        binding.verifyViewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        viewModel.request.phone = fragmentArgs.phone

        observe(viewModel.apiResponseLiveData) {
            showProgressBar(false)
            when (it.status) {
                Status.PROGRESS_LOADING -> {
                    when (it.data) {
                        false -> {
                            showProgressBar(false)
                        }
                        else -> {
                            showProgressBar(true)
                        }
                    }
                }
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is VerifyResponse -> {
                            when (fragmentArgs.flag) {
                                Codes.REGISTER_INTENT -> {
                                    PrefMethods.saveUserData(it.data.userData)
                                    startActivity(Intent(requireActivity(), MainActivity::class.java))
                                    requireActivity().finishAffinity()
                                }
                                else -> {
                                    val action = VerifyFragmentDirections.verifyToReset(viewModel.request.phone!!)
                                    findNavController().navigate(action)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.EMPTY_CODE -> {
                showToast(getString(R.string.msg_empty_code) , 1)
            }
            Codes.SHORT_CODE -> {
                showToast(getString(R.string.msg_short_code) , 1)
            }
        }
    }
    override fun onPause() {
        super.onPause()
        showProgressBar(false)
    }
}