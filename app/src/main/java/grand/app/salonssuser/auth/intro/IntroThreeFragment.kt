package grand.app.salonssuser.auth.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.base.BaseFragment
import grand.app.salonssuser.databinding.FragmentIntroThreeBinding
import grand.app.salonssuser.utils.PrefMethods

class IntroThreeFragment : BaseAuthFragment() {
    lateinit var binding: FragmentIntroThreeBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro_three, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvIntroTitle.text = PrefMethods.getSplashData()!!.slidersList!![2]!!.title
        binding.tvIntroDescription.text = PrefMethods.getSplashData()!!.slidersList!![2]!!.desc
        Glide.with(this).load(PrefMethods.getSplashData()!!.slidersList!![2]!!.img).into(binding.imgIntro)

        binding.btnIntroNext.setOnClickListener {
            PrefMethods.setIsFirstTime(false)
            findNavController().navigate(R.id.intro_three_to_login)
        }

        binding.btnIntroBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}