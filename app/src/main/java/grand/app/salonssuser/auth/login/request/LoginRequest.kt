package grand.app.salonssuser.auth.login.request

data class LoginRequest (
    var loginkey: String? = null,
    var password: String? = null,
    var type: Int = 0,
    var firebase_token: String? = null)