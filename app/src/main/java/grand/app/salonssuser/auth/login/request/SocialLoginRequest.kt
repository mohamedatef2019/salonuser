package grand.app.salonssuser.auth.login.request

data class SocialLoginRequest (
    var name: String? = null,
    var img: String? = null,
    var firebase_token: String? = null,
    var social_id: String? = null
)