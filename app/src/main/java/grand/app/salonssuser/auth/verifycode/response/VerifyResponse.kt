package grand.app.salonssuser.auth.verifycode.response

import com.google.gson.annotations.SerializedName
import grand.app.salonssuser.auth.login.response.UserData

data class VerifyResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val userData: UserData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

