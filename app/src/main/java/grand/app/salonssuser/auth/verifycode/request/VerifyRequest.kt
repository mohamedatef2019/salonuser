package grand.app.salonssuser.auth.verifycode.request

data class VerifyRequest (
    var phone: String? = null,
    var code: String? = null
)