package grand.app.salonssuser.auth.resetpass.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.BaseAuthFragment
import grand.app.salonssuser.auth.resetpass.response.ResetPassResponse
import grand.app.salonssuser.auth.resetpass.viewmodel.ResetPassViewModel
import grand.app.salonssuser.databinding.FragmentResetPasswordBinding
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class ResetPassFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentResetPasswordBinding
    lateinit var viewModel: ResetPassViewModel
    private val fragmentArgs : ResetPassFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reset_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(ResetPassViewModel::class.java)
        viewModel = ResetPassViewModel()
        binding.resetViwModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        viewModel.request.phone = fragmentArgs.phone

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.PROGRESS_LOADING -> {
                    when (it.data) {
                        false -> {
                            showProgressBar(false)
                        }
                        else -> {
                            showProgressBar(true)
                        }
                    }
                }
                Status.ERROR_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showProgressBar(false)
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ResetPassResponse -> {
                            findNavController().navigate(R.id.reset_to_login)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password) , 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password) , 1)
            }
            Codes.CONFIRM_PASS_EMPTY -> {
                showToast(getString(R.string.msg_empty_confirm_password) , 1)
            }
            Codes.PASSWORD_NOT_MATCH -> {
                showToast(getString(R.string.msg_not_match) , 1)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        showProgressBar(false)
    }
}