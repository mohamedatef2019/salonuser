package grand.app.salonssuser.auth.forgotpass.viewmodel

import grand.app.salonssuser.auth.forgotpass.request.ForgotRequest
import grand.app.salonssuser.auth.forgotpass.response.ForgotResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ForgotPassViewModel : BaseViewModel()
{
    var request = ForgotRequest()

    fun onSendClicked() {
        setClickable()

        when {
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.phone!!.length < 11 -> {
                setValue(Codes.INVALID_PHONE)
            }
            else -> {
                sendCodeToVerify()
            }
        }
    }

    private fun sendCodeToVerify() {
        obsIsProgress.set(true)
        requestCall<ForgotResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().forgotPassword(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onRememberClicked() {
        setClickable()
        setValue(Codes.LOGIN_INTENT)
    }
}