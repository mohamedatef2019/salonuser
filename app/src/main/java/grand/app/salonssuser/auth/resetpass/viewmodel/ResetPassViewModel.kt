package grand.app.salonssuser.auth.resetpass.viewmodel

import grand.app.salonssuser.auth.resetpass.request.ResetRequest
import grand.app.salonssuser.auth.resetpass.response.ResetPassResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ResetPassViewModel : BaseViewModel()
{
    var request = ResetRequest()

    fun onChangeClicked()
    {
        setClickable()

        when {
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                setValue(Codes.PASSWORD_EMPTY)
            }
            request.password!!.length < 6 -> {
                setValue(Codes.PASSWORD_SHORT)
            }
            request.confirmPass.isNullOrEmpty() || request.confirmPass.isNullOrBlank() -> {
                setValue(Codes.CONFIRM_PASS_EMPTY)
            }
            request.confirmPass != request.password -> {
                setValue(Codes.PASSWORD_NOT_MATCH)
            }
            else -> {
                resetPassword()
            }
        }
    }

    private fun resetPassword() {
        val resetRequest = ResetRequest(request.phone, request.password)
        obsIsProgress.set(true)

        requestCall<ResetPassResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().resetPassword(resetRequest) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}