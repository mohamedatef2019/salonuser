package grand.app.salonssuser.auth.resetpass.request

data class ResetRequest (
        var phone: String? = null,
        var password: String? = null,
        var confirmPass: String? = null
)