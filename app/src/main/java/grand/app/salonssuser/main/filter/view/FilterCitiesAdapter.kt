package grand.app.salonssuser.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemFilterCityBinding
import grand.app.salonssuser.main.filter.response.FilterDataCityItem
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterCityViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class FilterCitiesAdapter : RecyclerView.Adapter<FilterCitiesAdapter.FilterCitiesHolder>()
{
    var itemsList: ArrayList<FilterDataCityItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterDataCityItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterCitiesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFilterCityBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter_city, parent, false)
        return FilterCitiesHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterCitiesHolder, position: Int) {
        val itemViewModel = ItemFilterCityViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): FilterDataCityItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterDataCityItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterCitiesHolder(val binding: ItemFilterCityBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
