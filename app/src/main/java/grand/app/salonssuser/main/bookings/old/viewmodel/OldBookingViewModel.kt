package grand.app.salonssuser.main.bookings.old.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.bookings.old.view.OldBookingsAdapter
import grand.app.salonssuser.main.bookings.response.MyBookingsItem
import grand.app.salonssuser.main.bookings.response.MyBookingsResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class OldBookingViewModel : BaseViewModel()
{
    var adapter = OldBookingsAdapter()
    var pageNumber = 0

    fun getOldBookings() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MyBookingsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyBookings(2, pageNumber) }
        })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    when {
                        res.myBookingsData!!.myBookingsList!!.isNotEmpty() -> {
                            obsLayout.set(LoadingStatus.FULL)
                            apiResponseLiveData.value = ApiResponse.success(res)
                            adapter.updateList(res.myBookingsData!!.myBookingsList as ArrayList<MyBookingsItem>)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getOldBookings()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }
}