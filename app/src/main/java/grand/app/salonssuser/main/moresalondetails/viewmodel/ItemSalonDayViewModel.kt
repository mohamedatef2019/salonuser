package grand.app.salonssuser.main.moresalondetails.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemSalonDayViewModel(var item: SalonDayItem) : BaseViewModel(){
    var obsWorkingHours = ObservableField<String>()
    var obsStartTime = ObservableField<String>()
    var date = Date()

    init {
//        when {
//            item.startTime != null && item.endTime != null -> {
//                val format = SimpleDateFormat("hh:mm")
//                try {
//                    date = format.parse(item.startTime)
//                } catch (e: ParseException) {
//                    e.printStackTrace()
//                }
//
//                val timeFormat = SimpleDateFormat("hh:mm", Locale("ar"))
//                obsStartTime.set(timeFormat.format(date).toString())
//            }
//        }
        obsWorkingHours.set("${getString(R.string.label_from)}  ${item.startTime}  ${getString(R.string.label_to)}  ${item.endTime}")
    }
}