package grand.app.salonssuser.main.home.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.ItemBannerBinding
import grand.app.salonssuser.main.home.response.HomeSliderItem
import grand.app.salonssuser.main.home.viewmodel.ItemBannerViewModel
import grand.app.salonssuser.utils.SingleLiveEvent

class BannersAdapter : SliderViewAdapter<BannersAdapter.BannersHolder>()
{
    var itemsList =  ArrayList<HomeSliderItem>()
    var itemLiveData = SingleLiveEvent<HomeSliderItem>()

    override fun onCreateViewHolder(parent: ViewGroup?): BannersHolder {
        val context = parent!!.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBannerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_banner, parent, false)
        return BannersHolder(binding)
    }

    override fun onBindViewHolder(holder: BannersHolder, position: Int) {
        val itemViewModel = ItemBannerViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        Glide.with(BaseApp.getInstance).load(itemViewModel.item.img).into(holder.binding.ivAutoImageSlider)

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    fun updateList(models: ArrayList<HomeSliderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BannersHolder(val binding: ItemBannerBinding) : SliderViewAdapter.ViewHolder(binding.root)

    override fun getCount(): Int {
        return itemsList.size
    }
}
