package grand.app.salonssuser.main.bookingdetails.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.bookingdetails.response.BookingDetailsData
import grand.app.salonssuser.main.bookingdetails.response.BookingDetailsResponse
import grand.app.salonssuser.main.bookingdetails.response.BookingServiceItem
import grand.app.salonssuser.main.bookingdetails.view.BookingDetailsServicesAdapter
import grand.app.salonssuser.main.bookings.response.CancelOrderResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class BookingDetailsViewModel : BaseViewModel()
{
    var adapter = BookingDetailsServicesAdapter()
    var bookingDetails = BookingDetailsData()
    var obsPaymentWay = ObservableField<String>()
    var obsStatus = ObservableField<String>()
    var obsType = ObservableField<String>()

    fun getBookingDetails(bookingId : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<BookingDetailsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getBookingDetails(bookingId) } })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    bookingDetails = res.bookingData!!
                    apiResponseLiveData.value = ApiResponse.success(res)
                    adapter.updateList(res.bookingData.bookingServicesList as ArrayList<BookingServiceItem>)
                    setBookingPayment()
                    setBookingStatus()
                    setServiceType()
                    notifyChange()
                    Timber.e(res.bookingData.bookingServicesList.size.toString())
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }


    private fun setBookingStatus() {
        when (bookingDetails.status) {
            -1 -> {
                obsStatus.set(getString(R.string.label_cancelled))
            }
            0 -> {
                obsStatus.set(getString(R.string.label_pending))
            }
            1 -> {
                obsStatus.set(getString(R.string.label_on_the_way))
            }
            2 -> {
                obsStatus.set(getString(R.string.label_completed))
            }
        }
    }

    private fun setServiceType() {
        when (bookingDetails.type) {
            2 -> {
                obsType.set(getString(R.string.label_home_service))
            }
            1 -> {
                obsType.set(getString(R.string.label_salon_service))
            }
        }
    }

    fun setBookingPayment() {
        when (bookingDetails.paymentMethod) {
            0 -> {
                obsPaymentWay.set(getString(R.string.label_cash))
            }
            1 -> {
                obsPaymentWay.set(getString(R.string.label_online))
            }
        }
    }

    fun onRateClicked(){
        when (bookingDetails.status) {
            1 -> {
                cancelOrder()
            }
            2 -> {
                setValue(Codes.RATE_ORDER_CLICKED)
            }
        }
    }

    fun cancelOrder()
    {
        obsIsProgress.set(true)
        requestCall<CancelOrderResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().cancelOrder(bookingDetails.id!!.toInt()) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}