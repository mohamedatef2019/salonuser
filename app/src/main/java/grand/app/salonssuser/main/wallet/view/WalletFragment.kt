package grand.app.salonssuser.main.wallet.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.databinding.FragmentWalletBinding
import grand.app.salonssuser.main.wallet.response.WalletResponse
import grand.app.salonssuser.main.wallet.viewmodel.WalletViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class WalletFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentWalletBinding
    lateinit var viewModel: WalletViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wallet, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(WalletViewModel::class.java)
        viewModel = WalletViewModel()

        binding.viewModel = viewModel

        showBottomBar(false)

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getWallet()
        }

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is WalletResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Params.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Params.isAskedToLogin = 0
            }
        }
    }
}