package grand.app.salonssuser.main.orderreview.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.orderreview.response.CreateOrderResponse
import grand.app.salonssuser.main.orderreview.view.OrderReviewAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OrderReviewViewModel : BaseViewModel()
{
    var request = CreateOrderRequest()
    var adapter = OrderReviewAdapter()
    var orderType : Int = 1

    fun createOrder(request : CreateOrderRequest) {
        obsIsProgress.set(true)
        requestCall<CreateOrderResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().createOrder(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun prepareOrderRequest() {
        when (orderType) {
            Codes.ORDER_HOME -> {
                var homeOrderRequest = CreateOrderRequest()
                homeOrderRequest.salon_id = request.salon_id
                homeOrderRequest.date = request.date
                homeOrderRequest.time = request.time
                homeOrderRequest.extra_notes = request.extra_notes
                when {
                    request.extra_phone != null -> {
                        homeOrderRequest.extra_phone = request.extra_phone
                    }
                }
                homeOrderRequest.payment_method = request.payment_method

                homeOrderRequest.sub_category_id = request.sub_category_id
                homeOrderRequest.total_price = request.total_price
                homeOrderRequest.city_id = request.city_id
                homeOrderRequest.area = request.area
                homeOrderRequest.st = request.st
                homeOrderRequest.building_no = request.building_no
                homeOrderRequest.floor_no = request.floor_no
                homeOrderRequest.mark = request.mark
                homeOrderRequest.phone = request.phone

                createOrder(homeOrderRequest)
            }
            Codes.ORDER_SALON -> {

                var salonOrderRequest = CreateOrderRequest()
                salonOrderRequest.salon_id = request.salon_id
                salonOrderRequest.date = request.date
                salonOrderRequest.time = request.time
                salonOrderRequest.extra_notes = request.extra_notes
                salonOrderRequest.payment_method = request.payment_method

                salonOrderRequest.sub_category_id = request.sub_category_id
                salonOrderRequest.total_price = request.total_price

                createOrder(salonOrderRequest)
            }
        }
    }


    fun onConfirmClicked()
    {
        clickableLiveData.value = true
    }
}