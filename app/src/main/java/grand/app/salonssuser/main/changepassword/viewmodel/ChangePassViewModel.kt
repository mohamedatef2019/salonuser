package grand.app.salonssuser.main.changepassword.viewmodel

import grand.app.salonssuser.auth.resetpass.response.ResetPassResponse
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.changepassword.request.ChangePassRequest
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ChangePassViewModel : BaseViewModel()
{
    var request = ChangePassRequest()

    fun onChangeClicked()
    {
        setClickable()

        when {
            request.old_password.isNullOrEmpty() || request.old_password.isNullOrBlank() -> {
                setValue(Codes.OLD_PASSWORD_EMPTY)
            }
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                setValue(Codes.PASSWORD_EMPTY)
            }
            request.password!!.length < 6 -> {
                setValue(Codes.PASSWORD_SHORT)
            }
            request.confirm_pass.isNullOrEmpty() || request.confirm_pass.isNullOrBlank() -> {
                setValue(Codes.CONFIRM_PASS_EMPTY)
            }
            request.confirm_pass != request.password -> {
                setValue(Codes.PASSWORD_NOT_MATCH)
            }
            else -> {
                resetPassword()
            }
        }
    }

    private fun resetPassword() {
        val resetRequest = ChangePassRequest(PrefMethods.getUserData()?.phone, request.password, request.old_password)
        obsIsProgress.set(true)

        requestCall<ResetPassResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().changePass(resetRequest) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }


    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsLayout.set(LoadingStatus.FULL)
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}