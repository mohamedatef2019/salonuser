package grand.app.salonssuser.main.more.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.utils.constants.Codes

class MoreViewModel : BaseViewModel()
{
    fun onAboutClicked() {
        setValue(Codes.ABOUT_US_CLICKED)
    }

    fun onContactClicked() {
        setValue(Codes.CONTACT_US_CLICKED)
    }

    fun onShareClicked() {
        setValue(Codes.SHARE_APP_CLICKED)
    }

    fun onRateClicked() {
        setValue(Codes.RATE_APP_CLICKED)
    }

    fun onTermsClicked() {
        setValue(Codes.TERMS_CLICKED)
    }
}