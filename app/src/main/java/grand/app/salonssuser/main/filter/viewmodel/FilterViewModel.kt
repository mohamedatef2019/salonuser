package grand.app.salonssuser.main.filter.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.filter.request.FilterRateItem
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.request.FilterServiceTypeItem
import grand.app.salonssuser.main.filter.request.FilterSortItem
import grand.app.salonssuser.main.filter.response.FilterDataCityItem
import grand.app.salonssuser.main.filter.response.FilterDataResponse
import grand.app.salonssuser.main.filter.view.FilterCitiesAdapter
import grand.app.salonssuser.main.filter.view.FilterRateAdapter
import grand.app.salonssuser.main.filter.view.FilterServiceTypeAdapter
import grand.app.salonssuser.main.filter.view.FilterSortAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class FilterViewModel : BaseViewModel()
{
    var filterRequest = FilterRequest()

    var serviceTypeAdapter = FilterServiceTypeAdapter()
    var sortAdapter = FilterSortAdapter()
    var rateAdapter = FilterRateAdapter()
    var citiesAdapter = FilterCitiesAdapter()

    var obsMinPrice = ObservableField<Int>()
    var obsMaxPrice = ObservableField<Int>()

    var filterResponse = FilterDataResponse()
    var isPriceChanged : Int = 0

    fun onSearchClicked() {
        setValue(Codes.SEARCH_CLICKED)
    }

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    private fun getFilterData() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<FilterDataResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getFilterData(Params.categoryid) }
        })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    filterResponse = res
                    obsMinPrice.set(res.filterData?.minPrice!!)
                    obsMaxPrice.set(res.filterData.maxPrice)

                    apiResponseLiveData.value = ApiResponse.success(res)
                    citiesAdapter.updateList(res.filterData.citiesList as ArrayList<FilterDataCityItem>)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    init {
        getFilterData()

        val serviceTypesList = arrayListOf(
            FilterServiceTypeItem(1, "أفراد"),
            FilterServiceTypeItem(2, "صالونات"),
            FilterServiceTypeItem(3, "الكل")
        )
        serviceTypeAdapter.updateList(serviceTypesList)

        val sortList = arrayListOf(
            FilterSortItem(1, "الاحدث"),
            FilterSortItem(2, "الاقدم")
        )
        sortAdapter.updateList(sortList)

        val rateList = arrayListOf(
            FilterRateItem(1, "الاعلي تقييما"),
            FilterRateItem(2, "الاقل تقييما")
        )
        rateAdapter.updateList(rateList)
    }

    fun onConfirmClicked(){
        setValue(Codes.SEARCH_CLICKED)

//        when {
//            filterRequest.city_id == null -> {
//                setValue(Codes.EMPTY_CITY)
//            }
//            filterRequest.date == null -> {
//                setValue(Codes.EMPTY_SORT_METHOD)
//            }
//            filterRequest.sortby == null -> {
//                setValue(Codes.EMPTY_RATE_METHOD)
//            }
//            else -> {
//                setValue(Codes.SEARCH_CLICKED)
//            }
//        }
    }

    fun onResetClicked() {
        filterRequest = FilterRequest()

        sortAdapter.selectedPosition = -1
        serviceTypeAdapter.selectedPosition = -1
        rateAdapter.selectedPosition = -1
        citiesAdapter.selectedPosition = -1

        obsMinPrice.set(filterResponse.filterData?.minPrice!!)
        obsMaxPrice.set(filterResponse.filterData?.maxPrice!!)

        notifyChange()
        setValue(Codes.RESET_CLICKED)
    }
}