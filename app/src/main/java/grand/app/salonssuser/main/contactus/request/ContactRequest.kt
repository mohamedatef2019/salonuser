package grand.app.salonssuser.main.contactus.request

class ContactRequest {
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var message: String? = null
}