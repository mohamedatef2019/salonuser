package grand.app.salonssuser.main.rates.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.rates.response.RatesItem

class ItemRateViewModel(var item: RatesItem) : BaseViewModel()
{
    var obsRate = ObservableField<Float>()

    init {
        obsRate.set(item.rate!!.toFloat())
    }
}