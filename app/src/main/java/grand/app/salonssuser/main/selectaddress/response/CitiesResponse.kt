package grand.app.salonssuser.main.selectaddress.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CitiesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val citiesList: List<CityItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Serializable

data class CityItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	var isSelected : Boolean = false
) : Serializable
