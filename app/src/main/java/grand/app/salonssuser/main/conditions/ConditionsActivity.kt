package grand.app.salonssuser.main.conditions

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ActivityTermsBinding
import grand.app.salonssuser.main.terms.viewmodel.TermsViewModel
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params

class ConditionsActivity : AppCompatActivity()
{
    lateinit var binding: ActivityTermsBinding
    lateinit var viewModel: TermsViewModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms)
        viewModel = ViewModelProvider(this).get(TermsViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.getTerms()

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        binding.ibBack.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            setResult(Codes.ACCEPT_CONDITIONS, intent)
            finish()
        }
    }
    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
        setResult(Codes.ACCEPT_CONDITIONS, intent)
        finish()
    }
}

