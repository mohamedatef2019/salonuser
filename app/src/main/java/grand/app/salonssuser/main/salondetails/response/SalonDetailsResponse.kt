package grand.app.salonssuser.main.salondetails.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SalonDetailsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val salonDetailsData: SalonDetailsData? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Serializable

data class SalonDayItem(

	@field:SerializedName("start_time")
	var startTime: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("end_time")
	var endTime: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("day")
	var day: String? = null,

	@field:SerializedName("switch")
	var jsonMemberSwitch: Int? = null,

	var date: String? = null,

	var isSelected : Boolean = false

) : Serializable

data class SalonSliderItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Serializable

data class SalonDetailsData(

	@field:SerializedName("is_favorite")
	val isFavorite: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("categories_names")
	val salonCategoriesList: List<SalonCategoryItem?>? = null,

	@field:SerializedName("city")
	val city: City? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("certificate")
	val certificate: String? = null,

	@field:SerializedName("media")
	val mediaList: List<SalonSliderItem?>? = null,

	@field:SerializedName("taken_times")
	val reservedTimesList: List<ReservedTimeItem?>? = null,

	@field:SerializedName("license")
	val license: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("times")
	val daysList: List<SalonDayItem?>? = null,

	@field:SerializedName("min_price")
	val minPrice: Int? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("salon_type")
	val salonType: Int? = null,

	@field:SerializedName("logo")
	val logo: Logo? = null,

	@field:SerializedName("step")
	val step: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItem?>? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
) : Serializable

data class SalonServiceItem(

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("switch")
	val jsonMemberSwitch: Int? = null,

	var isSelected: Boolean = false
): Serializable

data class Logo(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Serializable

data class SalonCategoryItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("subs")
	val salonServicesList: List<SalonServiceItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	var isSelected : Boolean = false

) : Serializable

data class CategoriesItem(

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("sub_category_id")
	val subCategoryId: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("switch")
	val jsonMemberSwitch: Int? = null
)  : Serializable

data class City(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Serializable

data class ReservedTimeItem(

		@field:SerializedName("date")
		val date: String? = null,

		@field:SerializedName("salon_id")
		val salonId: Int? = null,

		@field:SerializedName("user_name")
		val userName: String? = null,

		@field:SerializedName("time")
		val time: String? = null,

		@field:SerializedName("user_img")
		val userImg: String? = null
) : Serializable

