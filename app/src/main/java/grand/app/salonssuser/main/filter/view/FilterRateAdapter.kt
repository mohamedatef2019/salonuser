package grand.app.salonssuser.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemFilterRateBinding
import grand.app.salonssuser.databinding.ItemPaymentWayBinding
import grand.app.salonssuser.main.filter.request.FilterRateItem
import grand.app.salonssuser.main.filter.request.FilterServiceTypeItem
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterRateViewModel
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterServiceViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class FilterRateAdapter : RecyclerView.Adapter<FilterRateAdapter.FilterRateHolder>()
{
    var itemsList: ArrayList<FilterRateItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterRateItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterRateHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFilterRateBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter_rate, parent, false)
        return FilterRateHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterRateHolder, position: Int) {
        val itemViewModel = ItemFilterRateViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): FilterRateItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterRateItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterRateHolder(val binding: ItemFilterRateBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
