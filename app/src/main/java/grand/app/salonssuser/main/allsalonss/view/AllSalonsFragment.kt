package grand.app.salonssuser.main.allsalonss.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentAllSalonsBinding
import grand.app.salonssuser.main.allsalonss.home.view.HomeShopsFragment
import grand.app.salonssuser.main.allsalonss.salon.view.SalonShopsFragment

class AllSalonsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentAllSalonsBinding
    lateinit var viewModel : AllSalonsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentAllSalonsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(AllSalonsViewModel::class.java)
        binding.viewModel =viewModel
        init()

        showBottomBar(false)
    }

    private fun init() {

        val adapter = FragmentStatePagerItemAdapter(
                childFragmentManager, FragmentPagerItems.with(requireActivity())
                .add(R.string.label_home_services, HomeShopsFragment::class.java)
                .add(R.string.label_at_salon, SalonShopsFragment::class.java)
                .create()
        )

        val viewPager = binding.pager
        binding.pager.adapter = adapter

        viewPager.currentItem = 1

        val viewPagerTab = binding.tabLayout
        viewPagerTab.setViewPager(viewPager)
    }
}

