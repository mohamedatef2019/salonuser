package grand.app.salonssuser.main.addsalon.viewmodel

import androidx.databinding.ObservableBoolean
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.addsalon.request.AddSalonRequest
import grand.app.salonssuser.main.addsalon.response.AddSalonResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import grand.app.salonssuser.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AddSalonViewModel : BaseViewModel()
{
    var request = AddSalonRequest()
    var obsIsTermsAccepted = ObservableBoolean()

    fun selectCertificateImg() {
        setValue(Codes.SELECT_CERTIFICATE_IMG)
    }

    fun selectLicenseImg() {
        setValue(Codes.SELECT_LICENSE_IMG)
    }

    fun onTypeClicked() {
        setValue(Codes.SHOW_PROVIDER_DIALOG)
    }

    fun onAddClicked()
    {
        setClickable()
        when {
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                setValue(Codes.EMPTY_NAME)
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                setValue(Codes.EMAIL_EMPTY)
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.type == null -> {
                setValue(Codes.EMPTY_TYPE)
            }
            request.salon_name.isNullOrEmpty() || request.salon_name.isNullOrBlank() -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            request.iban_number.isNullOrEmpty() || request.iban_number.isNullOrBlank() -> {
                setValue(Codes.EMPTY_IPAN_NUMBER)
            }
            request.payment_number.isNullOrEmpty() || request.payment_number.isNullOrBlank() -> {
                setValue(Codes.EMPTY_payment_NUMBER)
            }
            request.salonLicense == null -> {
                setValue(Codes.EMPTY_SALON_LICENSE)
            }
            request.salonCertificate == null -> {
                setValue(Codes.EMPTY_SALON_CERTIFICATE)
            }
            !obsIsTermsAccepted.get() -> {
                setValue(Codes.TERMS_REJECTED)
            }
            else -> {
                addNewSalon()
            }
        }
    }

    fun onTermsClicked() {
        setClickable()
        setValue(Codes.TERMS_CLICKED)
    }

    private fun addNewSalon() {
        obsIsProgress.set(true)
        requestCall<AddSalonResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().addSalon(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.SELECT_CERTIFICATE_IMG -> {
                request.salonCertificate = path.stringPathToFile()
            }
            Codes.SELECT_LICENSE_IMG -> {
                request.salonLicense = path.stringPathToFile()
            }
        }
    }
}