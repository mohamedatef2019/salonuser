package grand.app.salonssuser.main.filter.response

import com.google.gson.annotations.SerializedName

data class FilterDataResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val filterData: FilterData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class FilterData(

	@field:SerializedName("max_price")
	val maxPrice: Int? = null,

	@field:SerializedName("min_price")
	val minPrice: Int? = null,

	@field:SerializedName("cities")
	val citiesList: List<FilterDataCityItem?>? = null
)

data class FilterDataCityItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
