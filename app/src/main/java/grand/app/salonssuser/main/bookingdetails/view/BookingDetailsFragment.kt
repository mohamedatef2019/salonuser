package grand.app.salonssuser.main.bookingdetails.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentBookingDetailsBinding
import grand.app.salonssuser.dialogs.addrate.view.DialogAddRateFragment
import grand.app.salonssuser.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonssuser.main.bookingdetails.response.BookingDetailsResponse
import grand.app.salonssuser.main.bookingdetails.viewmodel.BookingDetailsViewModel
import grand.app.salonssuser.main.bookings.response.CancelOrderResponse
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class BookingDetailsFragment : BaseHomeFragment(), androidx.lifecycle.Observer<Any?>
{
    lateinit var binding: FragmentBookingDetailsBinding
    lateinit var viewModel: BookingDetailsViewModel
    private val fragmentArgs : BookingDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(BookingDetailsViewModel::class.java)
        viewModel = BookingDetailsViewModel()
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(false)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        viewModel.getBookingDetails(fragmentArgs.bookingId)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is BookingDetailsResponse -> {
                            when (viewModel.bookingDetails.status) {
                                -1 -> {
                                    binding.btnOrderRate.visibility = View.GONE
                                }
                                0, 1 -> {
                                    binding.btnOrderRate.text = getString(R.string.action_cancel_booking)
                                }
                                2 -> {
                                    binding.btnOrderRate.text = getString(R.string.action_rate_order)
                                }
                            }
                        }
                        is CancelOrderResponse -> {
                            val bundle = Bundle()
                            bundle.putString(Params.ORDER_MSG, getString(R.string.msg_order_cancelled))
                            Utils.startDialogActivity(requireActivity(), DialogOrderBookedFragment::class.java.name, Codes.DIALOG_ORDER_SUCCESS, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        when (it) {
            null -> return
            else -> when (it) {
                Codes.RATE_ORDER_CLICKED -> {
                    val bundle = Bundle()
                    bundle.putInt(Params.ORDER_ID, viewModel.bookingDetails.salonId!!)
                    Utils.startDialogActivity(requireActivity(), DialogAddRateFragment::class.java.name, Codes.DIALOG_RATE_ORDER, bundle)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_RATE_ORDER -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.RATE_MESSAGE) -> {
                                                Toasty.success(requireActivity(), data.getStringExtra(Params.RATE_MESSAGE).toString()).show()
                                                viewModel.getBookingDetails(viewModel.bookingDetails.id!!)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_CANCEL_ORDER -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.RATE_MESSAGE) -> {
                                                viewModel.getBookingDetails(fragmentArgs.bookingId)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {

                                        viewModel.getBookingDetails(fragmentArgs.bookingId)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}