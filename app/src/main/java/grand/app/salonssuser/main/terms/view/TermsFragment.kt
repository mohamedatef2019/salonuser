package grand.app.salonssuser.main.terms.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentTermsBinding
import grand.app.salonssuser.main.selectaddress.viewmodel.SelectAddressViewModel
import grand.app.salonssuser.main.terms.response.TermsResponse
import grand.app.salonssuser.main.terms.viewmodel.TermsViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class TermsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentTermsBinding
    lateinit var viewModel: TermsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
      //  viewModel = ViewModelProvider(this).get(TermsViewModel::class.java)
        viewModel = TermsViewModel()

        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getTerms()
        }

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is TermsResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}