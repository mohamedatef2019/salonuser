package grand.app.salonssuser.main.home.response

import com.google.gson.annotations.SerializedName

data class HomeResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val homeData: HomeData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class HomeData(

	@field:SerializedName("slider")
	val homeSlidersList: List<HomeSliderItem?>? = null,

	@field:SerializedName("categories")
	val homeCategoriesList: List<CategoriesItem?>? = null
)

data class CategoriesItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class HomeSliderItem(

	@field:SerializedName("link_type")
	val linkType: Int? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("attribute_id")
	val attributeId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
