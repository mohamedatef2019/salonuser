package grand.app.salonssuser.main.bookings.current.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.databinding.FragmentCurrentBookingsBinding
import grand.app.salonssuser.dialogs.confirm.DialogConfirmFragment
import grand.app.salonssuser.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonssuser.main.bookings.current.viewmodel.CurrentBookingViewModel
import grand.app.salonssuser.main.bookings.response.CancelOrderResponse
import grand.app.salonssuser.main.bookings.view.MyBookingsFragmentDirections
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class CurrentBookingsFragment : BaseHomeFragment()
{
    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    lateinit var binding: FragmentCurrentBookingsBinding
    lateinit var viewModel: CurrentBookingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_bookings, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.getUserStatus()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CurrentBookingViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.detailsLiveData.observe(viewLifecycleOwner, Observer {
            val action = MyBookingsFragmentDirections.bookingsToBookingDetails(it!!.id!!)
            navController.navigate(action)
        })

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getCurrentBookings()
        }

        viewModel.adapter.cancelLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.cancelOrder(it?.id!!)
//            var bundle = Bundle()
//            bundle.putString(Params.DIALOG_CONFIRM_MESSAGE,  " سيتم خصم قيمة الحجز ولا يمكن استرجاعها لان الالغاء لابد ان يكون خلال 24 ساعة")
//            bundle.putInt(Params.ORDER_ID, it?.id!!)
//            bundle.putInt(Params.REQUEST_CODE, Codes.DIALOG_CONFIRM_REQUEST)
//            Utils.startDialogActivity(requireActivity(), DialogConfirmFragment::class.java.name, Codes.DIALOG_CONFIRM_REQUEST, bundle)
        })

        binding.rvBookings.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            var lastX = 0
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                when (e.action) {
                    MotionEvent.ACTION_DOWN -> lastX = e.x.toInt()
                    MotionEvent.ACTION_MOVE -> {
                        val isScrollingRight = e.x < lastX
                        if (isScrollingRight && (binding.rvBookings.layoutManager as LinearLayoutManager).
                                findLastCompletelyVisibleItemPosition() == binding.rvBookings.adapter!!.itemCount - 1 ||
                                !isScrollingRight && (binding.rvBookings.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0
                        ) {
                            //  binding.p.setUserInputEnabled(true)
                        } else {
                            //  viewPager.setUserInputEnabled(false)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        lastX = 0
                        // viewPager.setUserInputEnabled(true)
                    }
                }
                return false
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
        })

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getCurrentBookings()
        }

        observe(viewModel.apiResponseLiveData) {

            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CancelOrderResponse -> {
                            val bundle = Bundle()
                            bundle.putString(Params.ORDER_MSG, getString(R.string.msg_order_cancelled))
                            Utils.startDialogActivity(requireActivity(), DialogOrderBookedFragment::class.java.name, Codes.DIALOG_ORDER_SUCCESS, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Params.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.RATE_MESSAGE) -> {
                                                viewModel.getCurrentBookings()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_CONFIRM_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.ORDER_ID) -> {
                                                Timber.e("Mou3aaaz dialog feedback")
                                                val bundle = Bundle()
                                                bundle.putString(Params.DIALOG_CONFIRM_MESSAGE,  "سيتم خصم قيمة الحجز من محفظتك وفي حالة عدم وجود رصيد كاف سيتم احتساب الخصم بالسالب وتكون العملية القادمة يستوجب فيها الدفع اونلاين للخدمتين")
                                                bundle.putInt(Params.ORDER_ID, data.getIntExtra(Params.ORDER_ID, 0))
                                                bundle.putInt(Params.REQUEST_CODE, Codes.DIALOG_CANCEL_ORDER_REQUEST)
                                                Utils.startDialogActivity(requireActivity(), DialogConfirmFragment::class.java.name, Codes.DIALOG_CANCEL_ORDER_REQUEST, bundle)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_CANCEL_ORDER_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.ORDER_ID) -> {
                                                Timber.e("Mou3aaaz cancel dialog")
                                                viewModel.cancelOrder(data.getIntExtra(Params.ORDER_ID, 0))
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun registerPaginationIntoRV(recyclerView: RecyclerView, adapter: CurrentBookingsAdapter, canLoadMore: Boolean, callBack: () -> Unit) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (canLoadMore) {
                    val lManager =
                            (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                    if (lManager == adapter.itemsList.size - 1) {
                        callBack()
                    }
                }
            }
        })

        adapter.registerAdapterDataObserver(object :
                RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
                        positionStart,
                        0
                )
            }
        })


    }

    override fun onResume() {
        super.onResume()
        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Params.isAskedToLogin = 0
            }
        }
    }
}