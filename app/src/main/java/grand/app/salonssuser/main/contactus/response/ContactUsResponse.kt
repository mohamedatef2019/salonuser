package grand.app.salonssuser.main.contactus.response

import com.google.gson.annotations.SerializedName

data class ContactUsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
