package grand.app.salonssuser.main.salondetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonSliderItem

class ItemSalonSliderViewModel(var item: SalonSliderItem) : BaseViewModel()