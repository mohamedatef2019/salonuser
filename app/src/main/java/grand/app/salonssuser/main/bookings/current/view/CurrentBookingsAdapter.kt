package grand.app.salonssuser.main.bookings.current.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemCurrentBookingBinding
import grand.app.salonssuser.main.bookings.current.viewmodel.ItemCurrentBookingViewModel
import grand.app.salonssuser.main.bookings.response.MyBookingsItem
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class CurrentBookingsAdapter : RecyclerView.Adapter<CurrentBookingsAdapter.CurrentBookingsHolder>()
{
    var itemsList: ArrayList<MyBookingsItem> = ArrayList()
    var detailsLiveData = SingleLiveEvent<MyBookingsItem>()
    var cancelLiveData = SingleLiveEvent<MyBookingsItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentBookingsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCurrentBookingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_current_booking, parent, false)
        return CurrentBookingsHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrentBookingsHolder, position: Int) {
        val itemViewModel = ItemCurrentBookingViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.btnDetailsBooking.setOnClickListener {
            detailsLiveData.value = itemViewModel.item
        }

        holder.binding.btnCancelBooking.setOnClickListener {
            cancelLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<MyBookingsItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CurrentBookingsHolder(val binding: ItemCurrentBookingBinding) : RecyclerView.ViewHolder(binding.root)
}
