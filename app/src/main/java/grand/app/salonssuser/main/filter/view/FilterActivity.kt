package grand.app.salonssuser.main.filter.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseActivity
import grand.app.salonssuser.databinding.ActivityFilterBinding
import grand.app.salonssuser.main.editprofile.viewmodel.EditProfileViewModel
import grand.app.salonssuser.main.filter.response.FilterDataResponse
import grand.app.salonssuser.main.filter.viewmodel.FilterViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class FilterActivity : BaseActivity() {

    lateinit var binding: ActivityFilterBinding
    lateinit var viewModel: FilterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter)
        //viewModel = ViewModelProvider(this).get(FilterViewModel::class.java)
        viewModel = FilterViewModel()
        binding.viewModel = viewModel

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    //showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    //showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is FilterDataResponse -> {
                            binding.priceSeekbar.setMinValue(viewModel.obsMinPrice.get()?.toFloat()!!)
                                    .setMaxValue(viewModel.obsMaxPrice.get()?.toFloat()!!).apply();
                            viewModel.isPriceChanged = 2
                            Timber.e("Filteeer " + viewModel.obsMinPrice.get() + " / " + viewModel.obsMaxPrice.get())
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        binding.priceSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            Timber.e("Filteeeeer : " + viewModel.isPriceChanged)

            binding.tvMinPrice.text = "$minValue ريال "
            binding.tvMaxPrice.text = "$maxValue ريال "
            viewModel.isPriceChanged = 1

            Timber.e("Filteeeeer : " + viewModel.isPriceChanged)

            viewModel.obsMinPrice.set(minValue.toInt())
            viewModel.obsMaxPrice.set(maxValue.toInt())
        }

        observe(viewModel.citiesAdapter.itemLiveData){
            viewModel.filterRequest.city_id = it!!.id
        }

        observe(viewModel.rateAdapter.itemLiveData){
            viewModel.filterRequest.sortby = it!!.id
        }

        observe(viewModel.sortAdapter.itemLiveData){
            when (it!!.id) {
                1 -> {
                    viewModel.filterRequest.date = 2
                }
                else -> {
                    viewModel.filterRequest.date = 1
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_CITY -> {
                    Toasty.warning(this, getString(R.string.msg_empty_city)).show()
                }
                Codes.RESET_CLICKED -> {
                    binding.priceSeekbar.setMinValue(viewModel.obsMinPrice.get()?.toFloat()!!)
                        .setMaxValue(viewModel.obsMaxPrice.get()?.toFloat()!!).apply()
                }
                Codes.EMPTY_SORT_METHOD -> {
                    Toasty.warning(this, getString(R.string.msg_empty_sort)).show()
                }
                Codes.EMPTY_RATE_METHOD -> {
                    Toasty.warning(this, getString(R.string.msg_empty_rate)).show()
                }
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.FILTER_REQUEST, intent)
                    finish()
                }
                Codes.SEARCH_CLICKED -> {
                    Timber.e("Filteeeeer : " + viewModel.isPriceChanged)
                    if (viewModel.isPriceChanged == 1)
                    {
                        viewModel.filterRequest.min_price = viewModel.obsMinPrice.get()
                        viewModel.filterRequest.max_price = viewModel.obsMaxPrice.get()
                    }
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.FILTER_REQUEST, viewModel.filterRequest)
                    setResult(Codes.FILTER_REQUEST, intent)
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.FILTER_REQUEST, intent)
        finish()
    }
}
