package grand.app.salonssuser.main.messages.response

import com.google.gson.annotations.SerializedName

data class SendMessageResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val sendMsgData: SendMsgData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class SendMsgData(

	@field:SerializedName("salon_id")
	val salonId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sender_id")
	val senderId: Int? = null
)
