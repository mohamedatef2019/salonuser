package grand.app.salonssuser.main.orderreview.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentResponse
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCurrencyISO
import com.myfatoorah.sdk.views.MFResult
import com.myfatoorah.sdk.views.MFSDK
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.databinding.FragmentOrderReviewBinding
import grand.app.salonssuser.dialogs.login.DialogLoginFragment
import grand.app.salonssuser.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonssuser.dialogs.payment.DialogPaymentFragment
import grand.app.salonssuser.main.moresalondetails.viewmodel.MoreSalonDetailsViewModel
import grand.app.salonssuser.main.orderreview.response.CreateOrderResponse
import grand.app.salonssuser.main.orderreview.viewmodel.OrderReviewViewModel
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.util.ArrayList

class OrderReviewFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentOrderReviewBinding
    lateinit var viewModel: OrderReviewViewModel
    private val fragmentArgs : OrderReviewFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_review, container, false)


        MFSDK.init("https://api-sa.myfatoorah.com/",
            "cd-SAnf1wAjztqVa3K6OkGLDi7N7a9TbFH_d96VJ3Pa2tkl0yX9XdZYd58y8uDpIkzJKRVza5MPkiEOG0m85miURrWhxg1FfAcOFlrugp0KdoIWozI_B5faHoxoPt94STrovc5l0DshdecUZ4pRZNs5UmAog9kwzbD1tUk4kpjqL-e27gmHSDzfH3ckfhN-28WbdWUGB71zrBahGhB5hkIbTU9OCsxIc7NCX19prw_MQDsWnph_UT-FZf-fyqBzM3YamtqDwPr8XV9hRdZrp9gmwXHor_cIGetPI1nDagqzvmSVbIIu8JMyl-0sv_2cV1hwfmSoqX1c8z0h9sLxqA2JzlI_gtL7ug-VHRjJ4AETFho6onDVSIfluXr0Pvhsj610Hq_if0c82r-nOrTJCcAdC9oCyhRWe6nODjx2EZco4IxJnLg0CZRPUbpwUB3vX4iXOR8WtogQT9IkSn-EYBrD8c8fHSLnd12xCuuWzrC9mn4GOvkn6EDJK8r8znm-HdPpzHln-VyUIhs8U67KPVvwhqHHDSxRMAqhODwrfH3uvM_1d2UKzje3qPA9lbIoiFzKAzXokNemsSnbMW-96bWuMUkCAIHF_wY65FtjXmNeClrgzc-Cn1jr7u9VwJuXXt7RguhmduUAn0URsNnzwWrtvVDV8LN88BzMfSOYk2gA_iAwbyANNVh6tUNq7C5QFykMhBSM4pzyckU4FxG4eQvOW_AM")
        MFSDK.setUpActionBar("MyFatoorah Payment", R.color.colorAccent, R.color.colorPrimary, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(OrderReviewViewModel::class.java)
        viewModel = OrderReviewViewModel()

        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.adapter.updateList(fragmentArgs.orderData.servicesList as ArrayList<SalonServiceItem>)
        viewModel.request = fragmentArgs.orderData
        viewModel.orderType = fragmentArgs.flag

        binding.btnOrderConfirm.setOnClickListener {
            when {
                PrefMethods.getUserData() == null -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
                else -> {
                    Utils.startDialogActivity(requireActivity(), DialogPaymentFragment::class.java.name, Codes.SELECT_ORDER_PAYMENT_DIALOG, null)
                }
            }
        }

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CreateOrderResponse -> {
                            val bundle = Bundle()
                           // bundle.putString(Params.ORDER_MSG, it.data.msg) back from Api
                            bundle.putString(Params.ORDER_MSG, getString(R.string.msg_order_booked))  // Static
                            Utils.startDialogActivity(requireActivity(), DialogOrderBookedFragment::class.java.name, Codes.DIALOG_ORDER_SUCCESS, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode)
        {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                       Params.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* When User back from Select Date and Time */
            Codes.SELECT_ORDER_PAYMENT_DIALOG -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.getIntExtra(Params.PAYMENT_WAY, 0) == 0 -> {
                                                viewModel.request.payment_method = 0
                                                viewModel.prepareOrderRequest()
                                            }
                                            data.getIntExtra(Params.PAYMENT_WAY, 0) == 1 -> {
                                                initPayment()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* When User back from Select Date and Time */
            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        requireActivity().startActivity(Intent (requireActivity(), MainActivity::class.java))
                                        requireActivity().finishAffinity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    var TAG = "TAGSW"
    private fun initPayment() {

        val request = MFInitiatePaymentRequest( viewModel.request.total_price, MFCurrencyISO.SAUDI_ARABIA_SAR)
        MFSDK.initiatePayment(
            request,
            MFAPILanguage.EN
        ) { result: MFResult<MFInitiatePaymentResponse?> ->
            if (result is MFResult.Success) {

                executePayment()

            } else if (result is MFResult.Fail)
                Unit
        }
    }

    private fun executePayment() {

        val request = MFExecutePaymentRequest(2, viewModel.request.total_price)
        MFSDK.executePayment(
            requireActivity(), request, MFAPILanguage.EN
        ) { invoiceId: String, result: MFResult<MFGetPaymentStatusResponse?> ->
            if (result is MFResult.Success) {
                viewModel.request.payment_method = 1
                viewModel.prepareOrderRequest()
            } else if (result is MFResult.Fail)
                Unit
        }
    }



    override fun onResume() {
        super.onResume()
        if (Params.isAskedToLogin == 1 && PrefMethods.getUserData() != null) {
            Params.isAskedToLogin = 0
            Utils.startDialogActivity(requireActivity(), DialogPaymentFragment::class.java.name, Codes.SELECT_ORDER_PAYMENT_DIALOG, null)
        }
    }
}