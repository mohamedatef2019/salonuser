package grand.app.salonssuser.main.orderreview.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemOrderReviewBinding
import grand.app.salonssuser.main.orderreview.viewmodel.ItemOrderReviewViewModel
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem
import java.util.*

class OrderReviewAdapter : RecyclerView.Adapter<OrderReviewAdapter.OrderReviewHolder>()
{
    var itemsList: ArrayList<SalonServiceItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderReviewHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderReviewBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_review, parent, false)
        return OrderReviewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderReviewHolder, position: Int) {
        val itemViewModel = ItemOrderReviewViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class OrderReviewHolder(val binding: ItemOrderReviewBinding) : RecyclerView.ViewHolder(binding.root)
}
