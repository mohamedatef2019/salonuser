package grand.app.salonssuser.main.bookings.response

import com.google.gson.annotations.SerializedName

data class CancelOrderResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
