package grand.app.salonssuser.main.aboutapp.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.aboutapp.response.AboutItem

class ItemAboutViewModel(var item: AboutItem) : BaseViewModel()