package grand.app.salonssuser.main.allsalonss.home.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemHomeShopBinding
import grand.app.salonssuser.main.allsalonss.home.viewmodel.ItemHomeShopViewModel
import grand.app.salonssuser.main.allsalonss.response.SalonItem
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class HomeShopsAdapter : RecyclerView.Adapter<HomeShopsAdapter.HomeShopsHolder>()
{
    var itemsList: ArrayList<SalonItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SalonItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeShopsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemHomeShopBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_home_shop, parent, false)
        return HomeShopsHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeShopsHolder, position: Int) {
        val itemViewModel = ItemHomeShopViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class HomeShopsHolder(val binding: ItemHomeShopBinding) : RecyclerView.ViewHolder(binding.root)
}
