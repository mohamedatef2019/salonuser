package grand.app.salonssuser.main.allsalonss.home.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentHomeShopsBinding
import grand.app.salonssuser.location.selectlocation.view.SelectLocationActivity
import grand.app.salonssuser.location.util.AddressItem
import grand.app.salonssuser.main.allsalonss.home.viewmodel.HomeShopsViewModel
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.main.allsalonss.view.AllSalonsFragmentArgs
import grand.app.salonssuser.main.allsalonss.view.AllSalonsFragmentDirections
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.view.FilterActivity
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class HomeShopsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentHomeShopsBinding
    lateinit var viewModel: HomeShopsViewModel
    val fragmentArgs : AllSalonsFragmentArgs by navArgs()

    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_shops, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeShopsViewModel::class.java)
        binding.viewModel = viewModel

        when {
            PrefMethods.getUserLocation() == null -> {
                val intent = Intent(requireActivity(), SelectLocationActivity::class.java)
                startActivityForResult(intent, Codes.LOCATION_REQUEST)
            }
            else -> {
                viewModel.request.lat = PrefMethods.getUserLocation()!!.lat
                viewModel.request.lng = PrefMethods.getUserLocation()!!.lng
                viewModel.getHomeSalons()
                viewModel.obsAddress.set(PrefMethods.getUserLocation()!!.address.toString())
            }
        }

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getHomeSalons()
        }

        binding.inputHomeSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        binding.rvSalons.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            var lastX = 0
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                when (e.action) {
                    MotionEvent.ACTION_DOWN -> lastX = e.x.toInt()
                    MotionEvent.ACTION_MOVE -> {
                        val isScrollingRight = e.x < lastX
                        if (isScrollingRight && (binding.rvSalons.layoutManager as LinearLayoutManager).
                                findLastCompletelyVisibleItemPosition() == binding.rvSalons.adapter!!.itemCount - 1 ||
                                !isScrollingRight && (binding.rvSalons.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0)
                                {
                            //  binding.p.setUserInputEnabled(true)
                        } else {
                            //  viewPager.setUserInputEnabled(false)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        lastX = 0
                        // viewPager.setUserInputEnabled(true)
                    }
                }
                return false
            }
            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
        })

        observe(viewModel.adapter.itemLiveData) {
            val action = AllSalonsFragmentDirections.allSalonsToSalonDetails(it!!.id!!)
            navController.navigate(action)
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.FILTER_CLICKED -> {
                    val intent = Intent(requireActivity(), FilterActivity::class.java)
                    intent.putExtra(Params.SERVICE_TYPE, 1)
                    startActivityForResult(intent, Codes.FILTER_REQUEST)
                }
                Codes.EMPTY_SALON_NAME -> {
                    showToast(getString(R.string.msg_empty_salon_name), 1)
                }
                Codes.ADDRESS_CLICKED -> {
                    val intent = Intent(requireActivity(), SelectLocationActivity::class.java)
                    startActivityForResult(intent, Codes.LOCATION_REQUEST)
                }
                Codes.SEARCH_CLICKED -> {
                    navController.navigate(AllSalonsFragmentDirections.allSalonsToSearch(viewModel.obsSearchNam.get().toString(), 1))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {

            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AllSalonsResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            /* When User back from Select Date and Time */
            Codes.FILTER_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        val filterRequest = data.getSerializableExtra(Params.FILTER_REQUEST) as FilterRequest
                                        filterRequest.type = 1
                                        viewModel.getFilteredSalons(filterRequest)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.LOCATION_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Params.isHome = 0
                                        val userAddress = data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM)
                                        viewModel.request.lat = userAddress!!.lat
                                        viewModel.request.lng = userAddress.lng
                                        viewModel.getHomeSalons()
                                        viewModel.obsAddress.set(userAddress.address)
                                        viewModel.obsAddress.notifyChange()
                                    }
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 0 -> {
                                        Params.isHome = 0
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}