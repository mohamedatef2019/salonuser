package grand.app.salonssuser.main.bookings.current.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.bookings.current.view.CurrentBookingsAdapter
import grand.app.salonssuser.main.bookings.response.CancelOrderResponse
import grand.app.salonssuser.main.bookings.response.MyBookingsItem
import grand.app.salonssuser.main.bookings.response.MyBookingsResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class CurrentBookingViewModel : BaseViewModel()
{
    var adapter = CurrentBookingsAdapter()
    var pageNumber = 0

    private fun getItemsHandler(isStart: Boolean) {
        getCurrentBookings()
        canLoadMore = true
    }

    fun loadData(isStart: Boolean = false) {
        canLoadMore = false
        if (isStart) {
            currentPage = PAGE_START
            getItemsHandler(isStart)
        } else {
            currentPage += 1
            if (currentPage <= totalPages)
                getItemsHandler(isStart)
            else
                Timber.e("you have reached all pages: current page-> $currentPage")
        }
    }

    fun getCurrentBookings()
    {
        Timber.e("Mou3aaaz api")

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MyBookingsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyBookings(0, pageNumber) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    when {
                        res.myBookingsData!!.myBookingsList!!.isNotEmpty() -> {
                            obsLayout.set(LoadingStatus.FULL)
                            apiResponseLiveData.value = ApiResponse.success(res)
                            adapter.updateList(res.myBookingsData!!.myBookingsList as ArrayList<MyBookingsItem>)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun cancelOrder(id : Int)
    {
        obsIsProgress.set(true)
        requestCall<CancelOrderResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().cancelOrder(id) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getCurrentBookings()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}