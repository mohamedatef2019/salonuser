package grand.app.salonssuser.main.salondetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemSalonSliderBinding
import grand.app.salonssuser.main.salondetails.response.SalonSliderItem
import grand.app.salonssuser.main.salondetails.viewmodel.ItemSalonSliderViewModel

class SalonSlidersAdapter : SliderViewAdapter<SalonSlidersAdapter.SalonSliderHolder>()
{
    var itemsList: List<SalonSliderItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup?): SalonSliderHolder {
        val context = parent!!.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonSliderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_slider, parent, false)
        return SalonSliderHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonSliderHolder, position: Int) {
        val itemViewModel = ItemSalonSliderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    fun updateList(models: List<SalonSliderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonSliderHolder(val binding: ItemSalonSliderBinding) : SliderViewAdapter.ViewHolder(binding.root)

    override fun getCount(): Int {
        return itemsList.size
    }
}
