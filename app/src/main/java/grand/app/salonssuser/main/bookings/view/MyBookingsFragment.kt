package grand.app.salonssuser.main.bookings.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentMyBookingBinding
import grand.app.salonssuser.main.bookings.current.view.CurrentBookingsFragment
import grand.app.salonssuser.main.bookings.old.view.OldBookingsFragment

class MyBookingsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentMyBookingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyBookingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        showBottomBar(false)
    }

    private fun init() {
        val adapter = FragmentPagerItemAdapter(
            childFragmentManager, FragmentPagerItems.with(requireActivity())
                .add(R.string.label_current_bookings, CurrentBookingsFragment::class.java)
                .add(R.string.label_old_bookings, OldBookingsFragment::class.java)
                .create()
        )

        val viewPager = binding.pager
        binding.pager.adapter = adapter

        val viewPagerTab = binding.tabLayout
        viewPagerTab.setViewPager(viewPager)
    }
}

