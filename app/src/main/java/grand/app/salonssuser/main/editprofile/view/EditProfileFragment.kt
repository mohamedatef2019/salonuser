package grand.app.salonssuser.main.editprofile.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.fxn.pix.Pix
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.databinding.FragmentEditProfileBinding
import grand.app.salonssuser.main.editprofile.response.UpdateProfileResponse
import grand.app.salonssuser.main.editprofile.viewmodel.EditProfileViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class EditProfileFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding : FragmentEditProfileBinding
    lateinit var viewModel : EditProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        when {
            PrefMethods.getUserData() != null -> {
                Glide.with(this).load(PrefMethods.getUserData()!!.img).into(binding.ivUserImg)
            }
        }

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is UpdateProfileResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                            requireActivity().finishAffinity()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.USER_PROFILE_IMAGE_REQUEST -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                if (requestCode == Codes.USER_PROFILE_IMAGE_REQUEST) {
                                    val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                                    binding.ivUserImg.setImageURI(returnValue[0].toUri())
                                    viewModel.gotImage(requestCode, array[0])
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onChanged(it: Any?)
    {
        if(it == null) return
        when (it) {
            Codes.CHOOSE_IMAGE_CLICKED -> {
                pickImage(Codes.USER_PROFILE_IMAGE_REQUEST)
            }
            Codes.LOGIN_CLICKED -> {
                Params.isAskedToLogin = 1
                requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Glide.with(this).load(PrefMethods.getUserData()!!.img).into(binding.ivUserImg)
                Params.isAskedToLogin = 0
            }
        }
    }
}