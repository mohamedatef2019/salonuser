package grand.app.salonssuser.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemFilterServiceTypeBinding
import grand.app.salonssuser.main.filter.request.FilterServiceTypeItem
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterServiceViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class FilterServiceTypeAdapter : RecyclerView.Adapter<FilterServiceTypeAdapter.FilterServiceTypeHolder>()
{
    var itemsList: ArrayList<FilterServiceTypeItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterServiceTypeItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterServiceTypeHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFilterServiceTypeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter_service_type, parent, false)
        return FilterServiceTypeHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterServiceTypeHolder, position: Int) {
        val itemViewModel = ItemFilterServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): FilterServiceTypeItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterServiceTypeItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterServiceTypeHolder(val binding: ItemFilterServiceTypeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
