package grand.app.salonssuser.main.payment.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.orderreview.response.CreateOrderResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PaymentViewModel : BaseViewModel()
{
    var request = CreateOrderRequest()

    fun createOrder(salonId : Int)
    {
        apiResponseLiveData.value = ApiResponse.progressLoading(true)
        requestCall<CreateOrderResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().createOrder(request) }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.progressLoading(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onConfirmClicked()
    {
        clickableLiveData.value = true
    }
}