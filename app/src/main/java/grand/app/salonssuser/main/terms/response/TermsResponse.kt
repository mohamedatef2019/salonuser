package grand.app.salonssuser.main.terms.response

import com.google.gson.annotations.SerializedName

data class TermsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val termsData: TermsData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class TermsData(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("terms")
	val termsList: List<TermsItem?>? = null
)

data class TermsItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
