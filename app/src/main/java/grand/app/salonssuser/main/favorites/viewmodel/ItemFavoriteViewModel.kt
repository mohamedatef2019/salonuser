package grand.app.salonssuser.main.favorites.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.favorites.response.FavoriteItem

class ItemFavoriteViewModel(var item: FavoriteItem) : BaseViewModel() {
    var obsMinPrice = ObservableField<String>()

    init {
        obsMinPrice.set("${getString(R.string.min_price_start_from)}${item.minPrice} ${"ريال"}")
    }
}