package grand.app.salonssuser.main.salondetails.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.FragmentSalonDetailsBinding
import grand.app.salonssuser.dialogs.dialogtime.view.DialogOrderTimeFragment
import grand.app.salonssuser.dialogs.login.DialogLoginFragment
import grand.app.salonssuser.main.favorites.response.AddToFavResponse
import grand.app.salonssuser.main.salondetails.response.SalonDetailsResponse
import grand.app.salonssuser.main.salondetails.response.SalonSliderItem
import grand.app.salonssuser.main.salondetails.viewmodel.SalonDetailsViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class SalonDetailsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentSalonDetailsBinding
    lateinit var viewModel: SalonDetailsViewModel
    lateinit var sliderView : SliderView
    private val fragmentArgs : SalonDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SalonDetailsViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.getSalonDetails(fragmentArgs.salonId)
        viewModel.createOrderRequest.salon_id = fragmentArgs.salonId

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getSalonDetails(fragmentArgs.salonId)
        }

        binding.rbSalonRate.setOnClickListener {
            findNavController().navigate(R.id.salon_details_to_rates)
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SalonDetailsResponse -> {
                            setupSlider(it.data.salonDetailsData!!.mediaList as List<SalonSliderItem>)
                            when (it.data.salonDetailsData.isFavorite) {
                                1 -> {
                                    Glide.with(requireActivity()).load(R.drawable.ic_fav).into(binding.ibFav)
                                }
                                else -> {
                                    Glide.with(requireActivity()).load(R.drawable.ic_un_fav).into(binding.ibFav)
                                }
                            }
                            when (it.data.salonDetailsData.salonType) {
                                1 -> {
                                    binding.btnSalonServices.visibility = View.GONE
                                    viewModel.serviceType = 0
                                    binding.btnHomeServices.setBackgroundResource(R.drawable.selected_home_bg)
                                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                                    binding.btnSalonServices.setBackgroundResource(R.drawable.unselected_home_bg)
                                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                                }
                                2 -> {
                                    binding.btnHomeServices.visibility = View.GONE
                                    viewModel.serviceType = 1
                                    binding.btnSalonServices.setBackgroundResource(R.drawable.selected_home_bg)
                                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                                    binding.btnHomeServices.setBackgroundResource(R.drawable.unselected_home_bg)
                                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                                }
                            }
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            if (it.data.addFavData!!.isFavorite == 1) {
                                Glide.with(this).load(R.drawable.ic_fav).into(binding.ibFav)
                            } else {
                                Glide.with(this).load(R.drawable.ic_un_fav).into(binding.ibFav)
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.NOT_LOGIN -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
                Codes.BACK_PRESSED -> {
                    findNavController().navigateUp()
                }
                Codes.SHARE_CLICKED -> {
                    Utils.shareApp(requireActivity(), "مشاركة تطبيق صالونات مع اصدقائك",
                        "https://play.google.com/store/apps/details?id=grand.app.salonssuser")
                }
                Codes.RATE_ORDER_CLICKED -> {
                    val action = SalonDetailsFragmentDirections.salonDetailsToRates(fragmentArgs.salonId)
                    findNavController().navigate(action)
                }
                Codes.CHAT_CLICKED -> {
                    val action = SalonDetailsFragmentDirections.salonDetailsToMessages(fragmentArgs.salonId, viewModel.salonDetails.userId!!,
                            viewModel.salonDetails.name.toString(), viewModel.salonDetails.logo!!.img.toString())
                    findNavController().navigate(action)
                }
                Codes.BOOK_ORDER_CLICKED -> {
                    when (viewModel.categoriesAdapter.selectedServices.size) {
                        0 -> {
                            Toasty.warning(requireActivity(), getString(R.string.msg_empty_services)).show()
                        }
                        else -> {
                            viewModel.createOrderRequest.servicesList = viewModel.categoriesAdapter.selectedServices
                            viewModel.createOrderRequest.sub_category_id = viewModel.categoriesAdapter.servicesIds
                            viewModel.createOrderRequest.total_price = viewModel.categoriesAdapter.totalPrice
                            val bundle = Bundle()
                            bundle.putSerializable(Params.SALON_DETAILS_DATA, viewModel.salonDetails)
                            Utils.startDialogActivity(requireActivity(), DialogOrderTimeFragment::class.java.name, Codes.SELECT_ORDER_TIME_DIALOG, bundle)
                        }
                    }
                }
                Codes.HOME_CLICKED -> {
                    binding.btnHomeServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.btnSalonServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
                Codes.SALON_CLICKED -> {
                    binding.btnSalonServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.btnHomeServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
            }
        }

        binding.btnShowMoreDetails.setOnClickListener {
            val action = SalonDetailsFragmentDirections.salonDetailsToMore(viewModel.salonDetails)
            findNavController().navigate(action)
        }
    }

    private fun setupSlider(covers: List<SalonSliderItem>) {
        viewModel.sliderAdapter.updateList(covers)
        sliderView = requireActivity().findViewById(R.id.slider_salon_ads)
        sliderView.setSliderAdapter(viewModel.sliderAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode)
        {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                       Params.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true)) }
                                }
                            }
                        }
                    }
                }
            }
            /* When User back from Select Date and Time */
            Codes.SELECT_ORDER_TIME_DIALOG -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.createOrderRequest.time = data.getStringExtra(Params.ORDER_TIME)
                                        viewModel.createOrderRequest.date = data.getStringExtra(Params.ORDER_DATE)
                                        viewModel.createOrderRequest.extra_notes = data.getStringExtra(Params.ORDER_NOTE).toString()
                                        when (viewModel.serviceType) {
                                            0 -> {
                                                val action = SalonDetailsFragmentDirections.salonDetailsToSelectAddress(viewModel.createOrderRequest)
                                                findNavController().navigate(action)
                                            }
                                            else -> {
                                                val action = SalonDetailsFragmentDirections.salonDetailToReview(viewModel.createOrderRequest, Codes.ORDER_SALON)
                                                findNavController().navigate(action)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (Params.isAskedToLogin == 1 && PrefMethods.getUserData() != null) {
            Params.isAskedToLogin = 0
            when (viewModel.type) {
                1 -> {
                    viewModel.addToFav(fragmentArgs.salonId)
                }
                else -> {
                    val action = SalonDetailsFragmentDirections.salonDetailsToMessages(fragmentArgs.salonId, viewModel.salonDetails.userId!!,
                            viewModel.salonDetails.name.toString(), viewModel.salonDetails.logo!!.img.toString())
                    findNavController().navigate(action)
                }
            }
        }
    }
}