package grand.app.salonssuser.main.search.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemSearchSalonBinding
import grand.app.salonssuser.main.search.response.SearchSalonItem
import grand.app.salonssuser.main.search.viewmodel.ItemSearchViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchHolder>()
{
    var itemsList: ArrayList<SearchSalonItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SearchSalonItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSearchSalonBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_search_salon, parent, false)
        return SearchHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchHolder, position: Int) {
        val itemViewModel = ItemSearchViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SearchSalonItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun clearList() {
        itemsList.clear()
        notifyDataSetChanged()
    }

    inner class SearchHolder(val binding: ItemSearchSalonBinding) : RecyclerView.ViewHolder(binding.root)
}
