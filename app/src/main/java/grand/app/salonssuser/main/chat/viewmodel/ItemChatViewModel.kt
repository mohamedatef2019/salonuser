package grand.app.salonssuser.main.chat.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.chat.response.ChatItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemChatViewModel(var item: ChatItem) : BaseViewModel()
{
    var obsReadtAt = ObservableField<String>()
    var date = Date()

    init {
//        val format = SimpleDateFormat("HH:mm")
//        try {
//            date = format.parse(item.readAt)
//        } catch (e: ParseException) {
//            e.printStackTrace()
//        }
//
//        val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
//        obsReadtAt.set(timeFormat.format(date).toString())
        obsReadtAt.set(item.readAt.toString())
    }
}