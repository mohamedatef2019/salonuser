package grand.app.salonssuser.main.home.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.home.response.CategoriesItem

class ItemCategoryViewModel(var item: CategoriesItem) : BaseViewModel()