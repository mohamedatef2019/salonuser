package grand.app.salonssuser.main.bookingdetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.bookingdetails.response.BookingServiceItem

class ItemBookingDetailsServiceViewModel(var item: BookingServiceItem) : BaseViewModel()