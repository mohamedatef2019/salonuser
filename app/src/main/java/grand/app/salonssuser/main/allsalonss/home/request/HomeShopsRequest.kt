package grand.app.salonssuser.main.allsalonss.home.request

data class HomeShopsRequest (
    var lat: Double? = null,
    var lng: Double? = null,
    var category_id: Int ? = null,
    var type : Int =  1
)