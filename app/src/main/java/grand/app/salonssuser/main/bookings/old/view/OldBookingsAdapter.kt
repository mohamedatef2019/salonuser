package grand.app.salonssuser.main.bookings.old.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemOldBookingBinding
import grand.app.salonssuser.main.allsalonss.home.viewmodel.ItemHomeShopViewModel
import grand.app.salonssuser.main.bookings.response.MyBookingsItem
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.main.allsalonss.home.view.HomeShopsAdapter
import grand.app.salonssuser.main.bookings.old.viewmodel.ItemOldBookingViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class OldBookingsAdapter : RecyclerView.Adapter<OldBookingsAdapter.OldBookingsHolder>()
{
    var itemsList: ArrayList<MyBookingsItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<MyBookingsItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OldBookingsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOldBookingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_old_booking, parent, false)
        return OldBookingsHolder(binding)
    }

    override fun onBindViewHolder(holder: OldBookingsHolder, position: Int) {
        val itemViewModel = ItemOldBookingViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.btnDetailsBooking.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<MyBookingsItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class OldBookingsHolder(val binding: ItemOldBookingBinding) : RecyclerView.ViewHolder(binding.root)
}
