package grand.app.salonssuser.main.favorites.response

import com.google.gson.annotations.SerializedName

data class AddToFavResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val addFavData: AddToFavData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class AddToFavData(

	@field:SerializedName("is_favorite")
	val isFavorite: Int? = null
)
