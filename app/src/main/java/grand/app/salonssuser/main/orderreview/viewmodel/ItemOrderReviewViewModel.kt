package grand.app.salonssuser.main.orderreview.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem

class ItemOrderReviewViewModel(var item: SalonServiceItem) : BaseViewModel()