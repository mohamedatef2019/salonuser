package grand.app.salonssuser.main.editprofile.request

import java.io.File

data class EditProfileRequest (
        var name: String? = null,
        var email: String? = null,
        var password: String? = null,
        var phone: String? = null,
        var userImg: File? = null
)