package grand.app.salonssuser.main.messages.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.messages.response.MessageItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemMessageViewModel(var item: MessageItem) : BaseViewModel() {

    var obsReadtAt = ObservableField<String>()
    var date = Date()

    init {
//        val format = SimpleDateFormat("HH:mm")
//        try {
//            date = format.parse(item.readAt)
//        } catch (e: ParseException) {
//            e.printStackTrace()
//        }
//
//        val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
//        obsReadtAt.set(timeFormat.format(date).toString())
        obsReadtAt.set(item.readAt.toString())
    }
}