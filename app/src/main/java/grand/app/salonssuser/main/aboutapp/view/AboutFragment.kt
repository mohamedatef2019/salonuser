package grand.app.salonssuser.main.aboutapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.main.aboutapp.viewmodel.AboutViewModel
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentAboutAppBinding
import grand.app.salonssuser.main.aboutapp.response.AboutAppResponse
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class AboutFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentAboutAppBinding
    lateinit var viewModel: AboutViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_app, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(AboutViewModel::class.java)
        viewModel = AboutViewModel()
        binding.viewModel = viewModel

        showBottomBar(false)

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getAboutData()
        }

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AboutAppResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}