package grand.app.salonssuser.main.chat.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.ItemChatBinding
import grand.app.salonssuser.main.chat.response.ChatItem
import grand.app.salonssuser.main.chat.viewmodel.ItemChatViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class ChatsAdapter : RecyclerView.Adapter<ChatsAdapter.ChatHolder>()
{
    var itemsList: ArrayList<ChatItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<ChatItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemChatBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chat, parent, false)
        return ChatHolder(binding)
    }

    override fun onBindViewHolder(holder: ChatHolder, position: Int) {
        val itemViewModel = ItemChatViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }

        when (itemViewModel.item.counter) {
            0 -> {
                holder.binding.tvChatUnreadCount.visibility = ViewGroup.GONE
                holder.binding.tvChatSeen.visibility = ViewGroup.VISIBLE
            }
            else -> {
                holder.binding.tvChatUnreadCount.text = itemViewModel.item.counter.toString()
                holder.binding.tvChatUnreadCount.visibility = ViewGroup.VISIBLE
                holder.binding.tvChatSeen.visibility = ViewGroup.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ChatItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ChatHolder(val binding: ItemChatBinding) : RecyclerView.ViewHolder(binding.root)
}
