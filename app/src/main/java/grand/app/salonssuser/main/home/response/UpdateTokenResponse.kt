package grand.app.salonssuser.main.home.response

import com.google.gson.annotations.SerializedName

data class UpdateTokenResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
