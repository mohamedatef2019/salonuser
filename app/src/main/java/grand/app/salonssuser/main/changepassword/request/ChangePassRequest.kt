package grand.app.salonssuser.main.changepassword.request

data class ChangePassRequest (
        var phone: String? = null,
        var password: String? = null,
        var old_password: String? = null,
        var confirm_pass: String? = null
)