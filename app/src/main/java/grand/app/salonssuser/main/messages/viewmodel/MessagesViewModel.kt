package grand.app.salonssuser.main.messages.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.messages.request.SendMessageRequest
import grand.app.salonssuser.main.messages.response.MessageItem
import grand.app.salonssuser.main.messages.response.MessagesResponse
import grand.app.salonssuser.main.messages.response.SendMessageResponse
import grand.app.salonssuser.main.messages.view.MessagesAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import grand.app.salonssuser.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class MessagesViewModel : BaseViewModel()
{
    var request = SendMessageRequest()
    var adapter = MessagesAdapter()
    var isImgSelected : Boolean = false
    var obsMsg = ObservableField<String>()
    var obsFriendName = ObservableField<String>()
    var obsStatus = ObservableField<String>()
    var obsImg = ObservableField<String>()
    var messagesList = ArrayList<MessageItem>()

    fun onSendMsgClicked() {
        when {
            obsMsg.get() == null || obsMsg.get() == "" -> {
                // empty msg
                setValue(Codes.EMPTY_MESSAGE)
            }
            else -> {
                when {
                    obsMsg.get() != null && !isImgSelected -> {
                        // text only
                        request.text = obsMsg.get()
                        sendTextMsg()
                    }
                    obsMsg.get() != null && isImgSelected -> {
                        // text with img
                        request.text = obsMsg.get()
                        sendMsgWithImg()
                    }
                }
            }
        }
    }

    fun onSelectImage() {
        setValue(Codes.PICK_CHAT_IMAGE)
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.PICK_CHAT_IMAGE -> {
                isImgSelected = true
                request.img = path.stringPathToFile()
            }
        }
    }

    fun getMessages(salonId : Int, name : String , img : String)
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MessagesResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getMessages(salonId) } })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    messagesList = res.messagesList!! as ArrayList<MessageItem>
                    adapter.updateList(messagesList)
                    when {
                        res.messagesList.isNotEmpty() -> {
                            request.receiver_id = res.messagesList[0]!!.receiver!!.id
                            request.salon_id = res.messagesList[0]!!.salonId!!
                        }
                    }
                    obsImg.set(img)
                    obsFriendName.set(name)
                    notifyChange()
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun refreshMessages(salonId : Int)
    {
        Timber.e("Mou3aaaz refresh api")
        requestCall<MessagesResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getMessages(salonId) } })
        { res ->
            when (res!!.code) {
                200 -> {
                    Timber.e("Mou3aaaz refresh resonse")
                    messagesList = res.messagesList!! as ArrayList<MessageItem>
                    adapter.updateList(messagesList)
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun sendTextMsg()
    {
        obsIsProgress.set(true)
        requestCall<SendMessageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().sendChatMsg(request) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    refreshMessages(request.salon_id!!)
                    obsMsg.set("")
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun sendImg()
    {
        obsIsProgress.set(true)
        request.text = ""
        requestCall<SendMessageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().sendChatMsg(request) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    refreshMessages(request.salon_id!!)
                    obsMsg.set("")
                    request.img = null
                    isImgSelected = false
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun sendMsgWithImg()
    {
        obsIsProgress.set(true)
        requestCall<SendMessageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().sendChatMsgWithImg(request) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {

                    refreshMessages(request.salon_id!!)
                    obsMsg.set("")
                    request.img = null
                    isImgSelected = false
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onBackPressed() {
        setValue(Codes.BACK_PRESSED)
    }

}