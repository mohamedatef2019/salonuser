package grand.app.salonssuser.main.filter.request

import java.io.Serializable

data class FilterRequest (
        var type: Int? = null,
        var min_price: Int? = null,
        var max_price: Int? = null,
        var city_id: Int? = null,
        var date: Int? = null,
        var sortby: Int? = null
        ) : Serializable