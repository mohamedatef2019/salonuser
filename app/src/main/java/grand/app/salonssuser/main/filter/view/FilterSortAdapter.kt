package grand.app.salonssuser.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemFilterSortBinding
import grand.app.salonssuser.databinding.ItemPaymentWayBinding
import grand.app.salonssuser.main.filter.request.FilterServiceTypeItem
import grand.app.salonssuser.main.filter.request.FilterSortItem
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterServiceViewModel
import grand.app.salonssuser.main.filter.viewmodel.ItemFilterSortViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class FilterSortAdapter : RecyclerView.Adapter<FilterSortAdapter.FilterSortHolder>()
{
    var itemsList: ArrayList<FilterSortItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterSortItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterSortHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFilterSortBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter_sort, parent, false)
        return FilterSortHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterSortHolder, position: Int) {
        val itemViewModel = ItemFilterSortViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): FilterSortItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterSortItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterSortHolder(val binding: ItemFilterSortBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
