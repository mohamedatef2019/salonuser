package grand.app.salonssuser.main.contactus.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.contactus.request.ContactRequest
import grand.app.salonssuser.main.contactus.response.ContactUsResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.isEmailValid
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContactUsViewModel : BaseViewModel()
{
    var request = ContactRequest()

    fun onSendClicked() {
        setClickable()
        when {
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                setValue(Codes.EMPTY_NAME)
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                setValue(Codes.EMAIL_EMPTY)
            }
            !isEmailValid(request.email!!) -> {
                setValue(Codes.INVALID_EMAIL)
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.message.isNullOrEmpty() || request.message.isNullOrBlank() -> {
                setValue(Codes.EMPTY_MESSAGE)
            }
            else -> {
                sendHelpMsg()
            }
        }
    }

    private fun sendHelpMsg()
    {
        obsIsProgress.set(true)
        requestCall<ContactUsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().sendHelpMessage(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}
