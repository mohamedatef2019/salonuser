package grand.app.salonssuser.main.wallet.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.wallet.response.WalletOrderItem
import grand.app.salonssuser.main.wallet.response.WalletResponse
import grand.app.salonssuser.main.wallet.view.WalletOrdersAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class WalletViewModel : BaseViewModel()
{
    var adapter = WalletOrdersAdapter()
    var obsCredit = ObservableField<String>()

    fun getWallet()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<WalletResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getWallet() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    obsCredit.set("${res.walletData!!.walletCredit.toString()} ريال")
                    adapter.updateList(res.walletData.walletOrdersList as ArrayList<WalletOrderItem>)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getWallet()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
