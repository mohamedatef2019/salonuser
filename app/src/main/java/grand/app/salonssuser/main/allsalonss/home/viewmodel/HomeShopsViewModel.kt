package grand.app.salonssuser.main.allsalonss.home.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.allsalonss.home.request.HomeShopsRequest
import grand.app.salonssuser.main.allsalonss.home.view.HomeShopsAdapter
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.main.allsalonss.response.SalonItem
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.response.FilterSalonsResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class HomeShopsViewModel : BaseViewModel()
{
    var request = HomeShopsRequest()
    var adapter = HomeShopsAdapter()
    var pageNumber = 0
    var obsAddress = ObservableField<String>()
    var obsSearchNam = ObservableField<String>()
    var flag : Int = 0

    fun onFilterClicked(){
        setValue(Codes.FILTER_CLICKED)
    }

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun onAddressClicked() {
        setValue(Codes.ADDRESS_CLICKED)
    }

    fun getHomeSalons() {
        obsLayout.set(LoadingStatus.SHIMMER)
        request.category_id = Params.categoryid
        request.type = 1
        requestCall<AllSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHomeShops(request)} })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.allSalonsData?.salonsList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.allSalonsData!!.salonsList as ArrayList<SalonItem>)
                        }
                    }
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun getFilteredSalons(filterRequest : FilterRequest) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<FilterSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().filterSalons(filterRequest) }
        })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    when (res.filterSalonsData!!.filterList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.filterSalonsData.filterList as ArrayList<SalonItem>)
                        }
                    }
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}