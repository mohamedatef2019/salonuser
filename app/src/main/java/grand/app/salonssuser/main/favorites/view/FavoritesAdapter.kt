package grand.app.salonssuser.main.favorites.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemFavoriteBinding
import grand.app.salonssuser.main.favorites.response.FavoriteItem
import grand.app.salonssuser.main.favorites.viewmodel.ItemFavoriteViewModel
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class FavoritesAdapter : RecyclerView.Adapter<FavoritesAdapter.FavoritesHolder>()
{
    var itemsList: ArrayList<FavoriteItem> = ArrayList()
    var removeLiveData = SingleLiveEvent<FavoriteItem>()
    var itemLiveData = SingleLiveEvent<FavoriteItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFavoriteBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_favorite, parent, false)
        return FavoritesHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoritesHolder, position: Int) {
        val itemViewModel = ItemFavoriteViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.ibRemoveFav.setOnClickListener {
            removeLiveData.value = itemViewModel.item
        }

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun removeItem(item: FavoriteItem) {
        itemsList.remove(item)
        notifyDataSetChanged()
    }

    fun updateList(models: ArrayList<FavoriteItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FavoritesHolder(val binding: ItemFavoriteBinding) : RecyclerView.ViewHolder(binding.root)
}
