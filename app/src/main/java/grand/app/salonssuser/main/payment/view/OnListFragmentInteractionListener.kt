package grand.app.salonssuser.main.payment.view

import com.myfatoorah.sdk.entity.initiatepayment.PaymentMethod

interface OnListFragmentInteractionListener {
    fun onListFragmentInteraction(position: Int, item: PaymentMethod)
}