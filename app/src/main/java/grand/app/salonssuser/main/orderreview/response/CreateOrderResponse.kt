package grand.app.salonssuser.main.orderreview.response

import com.google.gson.annotations.SerializedName

data class CreateOrderResponse(

        @field:SerializedName("msg")
	val msg: String? = null,

        @field:SerializedName("code")
	val code: Int? = null,

        @field:SerializedName("data")
	val data: Data? = null,

        @field:SerializedName("status")
	val status: String? = null
)

data class Data(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("extra_notes")
	val extraNotes: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
