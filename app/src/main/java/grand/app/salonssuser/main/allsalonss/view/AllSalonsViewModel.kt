package grand.app.salonssuser.main.allsalonss.view

import grand.app.salonssuser.base.BaseViewModel

class AllSalonsViewModel : BaseViewModel()
{
    var isFirstTime : Boolean = true

    init {
        isFirstTime = true
    }
}