package grand.app.salonssuser.main.filter.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.filter.request.FilterServiceTypeItem

class ItemFilterServiceViewModel(var item: FilterServiceTypeItem) : BaseViewModel()
