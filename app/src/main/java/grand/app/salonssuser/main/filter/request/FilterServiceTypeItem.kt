package grand.app.salonssuser.main.filter.request

data class FilterServiceTypeItem(
    var id: Int? = null,
    var name: String? = null
)
