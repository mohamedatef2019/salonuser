package grand.app.salonssuser.main.rates.response

import com.google.gson.annotations.SerializedName

data class RatesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val ratesData: RatesData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RatesItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("rate")
	val rate: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class RatesData(

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("rates")
	val ratesList: List<RatesItem?>? = null,

	@field:SerializedName("count")
	val count: Int? = null
)
