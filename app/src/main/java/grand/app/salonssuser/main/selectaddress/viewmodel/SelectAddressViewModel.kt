package grand.app.salonssuser.main.selectaddress.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.selectaddress.response.CitiesResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SelectAddressViewModel : BaseViewModel()
{
    var request = CreateOrderRequest()
    var citiesResponse  = CitiesResponse()

    fun onAddClicked() {
        setClickable()
        when {
            request.city_id == null -> {
                setValue(Codes.EMPTY_CITY)
            }
            request.area.isNullOrEmpty() || request.area.isNullOrBlank() -> {
                setValue(Codes.EMPTY_AREA)
            }
            request.st.isNullOrEmpty() || request.st.isNullOrBlank() -> {
                setValue(Codes.EMPTY_STREET_NAME)
            }
            request.building_no == null -> {
                setValue(Codes.EMPTY_BUILDING_NUM)
            }
            request.floor_no == null -> {
                setValue(Codes.EMPTY_FLOOR)
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.mark.isNullOrEmpty() || request.mark.isNullOrBlank() -> {
                setValue(Codes.EMPTY_MARK)
            }
            else -> {
                saveDeliveryAddress()
            }
        }
    }

    private fun saveDeliveryAddress() {
        setValue(Codes.ADDRESS_SAVED)
    }

    fun onCityClicked() {
        setValue(Codes.SHOW_CITY_DIALOG)
    }

    private fun getCities() {
        requestCall<CitiesResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getCities()}
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    citiesResponse = res
                }
            }
        }
    }

    init {
        getCities()
    }
}