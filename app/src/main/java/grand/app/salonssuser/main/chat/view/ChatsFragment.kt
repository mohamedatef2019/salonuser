package grand.app.salonssuser.main.chat.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentChatsBinding
import grand.app.salonssuser.main.chat.response.ChatsResponse
import grand.app.salonssuser.main.chat.viewmodel.ChatsViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class ChatsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentChatsBinding
    lateinit var viewModel: ChatsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chats, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       //viewModel = ViewModelProvider(this).get(ChatsViewModel::class.java)
        viewModel = ChatsViewModel()

        showBottomBar(true)

        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getUserStatus()
        }

        viewModel.adapter.itemLiveData.observe(viewLifecycleOwner, Observer {
            val action = ChatsFragmentDirections.chatToMessages(it!!.salonId!!, it.id!!,  it.name.toString(), it.img.toString())
            findNavController().navigate(action)
        })



        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ChatsResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Params.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Params.isAskedToLogin = 0
            }
        }
    }
}