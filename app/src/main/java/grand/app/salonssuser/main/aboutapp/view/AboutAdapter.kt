package grand.app.salonssuser.main.aboutapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemAboutAppBinding
import grand.app.salonssuser.databinding.ItemFavoriteBinding
import grand.app.salonssuser.main.aboutapp.response.AboutItem
import grand.app.salonssuser.main.aboutapp.viewmodel.ItemAboutViewModel
import grand.app.salonssuser.main.favorites.viewmodel.ItemFavoriteViewModel
import java.util.*

class AboutAdapter : RecyclerView.Adapter<AboutAdapter.FavoritesHolder>()
{
    var itemsList: ArrayList<AboutItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemAboutAppBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_about_app, parent, false)
        return FavoritesHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoritesHolder, position: Int) {
        val itemViewModel = ItemAboutViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<AboutItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FavoritesHolder(val binding: ItemAboutAppBinding) : RecyclerView.ViewHolder(binding.root)
}
