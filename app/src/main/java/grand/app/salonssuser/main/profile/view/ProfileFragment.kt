package grand.app.salonssuser.main.profile.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.databinding.FragmentProfileBinding
import grand.app.salonssuser.main.profile.viewmodel.ProfileViewModel
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes

class ProfileFragment : BaseHomeFragment(), androidx.lifecycle.Observer<Any?>
{
    lateinit var binding: FragmentProfileBinding
    lateinit var vieWModel: ProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // vieWModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        vieWModel = ProfileViewModel()

        binding.viewModel = vieWModel

        showBottomBar(true)
        vieWModel.mutableLiveData.observe(viewLifecycleOwner, this)

        when {
            PrefMethods.getUserData() == null -> {
                binding.tvProfileLogout.text = getString(R.string.label_login)
            }
            else -> {
                binding.tvProfileLogout.text = getString(R.string.label_log_out)
            }
        }
    }

    override fun onChanged(it: Any?) {

        if (it == null) return
        when (it) {
            Codes.EDIT_CLICKED -> {
                findNavController().navigate(R.id.profile_to_edit)
            }
            Codes.CHANGE_PASS_CLICKED -> {
                findNavController().navigate(R.id.profile_to_change_pass)
            }
            Codes.WALLET_CLICKED -> {
                findNavController().navigate(R.id.profile_to_wallet)
            }
            Codes.BOOKING_CLICKED-> {
                findNavController().navigate(R.id.profile_to_bookings)
            }
            Codes.ADD_SALON_CLICKED -> {
                findNavController().navigate(R.id.profile_to_add_salon)
            }
            Codes.LOGOUT_CLICK -> {
                when {
                    AccessToken.getCurrentAccessToken() != null -> {
                        GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, GraphRequest.Callback {
                            AccessToken.setCurrentAccessToken(null)
                            LoginManager.getInstance().logOut()

                        }).executeAsync()
                    }
                }
                PrefMethods.deleteUserData()
                requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java))
                requireActivity().finishAffinity()
            }
        }
    }
}