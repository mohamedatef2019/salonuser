package grand.app.salonssuser.main.messages.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemChatMessageBinding
import grand.app.salonssuser.databinding.ItemChatMessageWhiteBinding
import grand.app.salonssuser.main.messages.response.MessageItem
import grand.app.salonssuser.main.messages.viewmodel.ItemMessageViewModel
import grand.app.salonssuser.utils.PrefMethods
import java.util.*

class MessagesAdapter : RecyclerView.Adapter<MessagesAdapter.MessagesHolder>()
{
    var itemsList: ArrayList<MessageItem> = ArrayList()
    val ITEM_FROM = 0
    val ITEM_WHITE = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        var binding: ViewDataBinding? = null
        binding = when (viewType) {
            ITEM_WHITE -> DataBindingUtil.inflate(layoutInflater, R.layout.item_chat_message_white, parent, false)
            else -> DataBindingUtil.inflate(layoutInflater, R.layout.item_chat_message, parent, false)
        }
        return MessagesHolder(binding)
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemsList[position].senderId == PrefMethods.getUserData()!!.id) {
            ITEM_FROM
        } else {
            ITEM_WHITE
        }
    }

    override fun onBindViewHolder(holder: MessagesHolder, position: Int) {
        val itemViewModel = ItemMessageViewModel(itemsList[position])
        holder.bindViewModel(itemViewModel)
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<MessageItem>) {
        itemsList = models
        itemsList.reverse()
        notifyDataSetChanged()
    }

    inner class MessagesHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindViewModel(itemViewModel: ItemMessageViewModel) {
            when (binding) {
                is ItemChatMessageBinding -> {
                    binding.viewModel = itemViewModel
                }
                else -> {
                    (binding as ItemChatMessageWhiteBinding).viewModel = itemViewModel
                }
            }
        }
    }
}
