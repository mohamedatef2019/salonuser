package grand.app.salonssuser.main.salondetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.favorites.response.AddToFavResponse
import grand.app.salonssuser.main.orderreview.request.CreateOrderRequest
import grand.app.salonssuser.main.salondetails.response.SalonCategoryItem
import grand.app.salonssuser.main.salondetails.response.SalonDetailsData
import grand.app.salonssuser.main.salondetails.response.SalonDetailsResponse
import grand.app.salonssuser.main.salondetails.view.SalonCategoriesAdapter
import grand.app.salonssuser.main.salondetails.view.SalonSlidersAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class SalonDetailsViewModel : BaseViewModel()
{
    var salonDetails = SalonDetailsData()
    var obsRate : Float = 0f
    var type : Int = 0
    var sliderAdapter = SalonSlidersAdapter()
    var categoriesAdapter = SalonCategoriesAdapter()
    var serviceType = 1   // 0 For Home --- 1 for Salon
    var createOrderRequest = CreateOrderRequest()

    fun getSalonDetails(id : Int)
    {
        when {
            PrefMethods.getUserData() == null -> {
                getVisitorSalonDetails(id)
            }
            else -> {
                getLoginSalonDetails(id)
            }
        }
    }

    fun getVisitorSalonDetails(salonId : Int) {
        categoriesAdapter.selectedServices.clear()
        categoriesAdapter.servicesIds.clear()
        categoriesAdapter.totalPrice = 0.0
        createOrderRequest = CreateOrderRequest()

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<SalonDetailsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getVisitorSalonDetails(salonId) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    salonDetails = res.salonDetailsData!!
                    obsRate = res.salonDetailsData.rate!!.toFloat()
                    apiResponseLiveData.value = ApiResponse.success(res)
                    categoriesAdapter.updateList(res.salonDetailsData.salonCategoriesList as ArrayList<SalonCategoryItem>)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun getLoginSalonDetails(salonId : Int) {
        categoriesAdapter.selectedServices.clear()
        categoriesAdapter.servicesIds.clear()
        categoriesAdapter.totalPrice = 0.0
        createOrderRequest = CreateOrderRequest()

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<SalonDetailsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getLoginSalonDetails(salonId) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    salonDetails = res.salonDetailsData!!
                    obsRate = res.salonDetailsData.rate!!.toFloat()
                    apiResponseLiveData.value = ApiResponse.success(res)
                    categoriesAdapter.updateList(res.salonDetailsData.salonCategoriesList as ArrayList<SalonCategoryItem>)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun addToFav(salonId : Int) {
        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
                type = 1
            }
            else -> {
                obsIsProgress.set(true)
                requestCall<AddToFavResponse?>({
                    withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(salonId) }
                })
                { res ->
                    obsIsProgress.set(false)
                    when (res!!.code) {
                        200 -> {
                            apiResponseLiveData.value = ApiResponse.success(res)
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                        }
                    }
                }
            }
        }
    }

    fun onChatClicked() {
        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
                type = 2
            }
            else -> {
                setValue(Codes.CHAT_CLICKED)
            }
        }
    }

    fun onBookOrderClicked() {
        setValue(Codes.BOOK_ORDER_CLICKED)
    }

    fun onHomeClicked() {
        serviceType = 0
        setValue(Codes.HOME_CLICKED)
    }

    fun onSalonClicked() {
        serviceType = 1
        setValue(Codes.SALON_CLICKED)
    }

    fun onRateClicked() {
        setValue(Codes.RATE_ORDER_CLICKED)
    }

    fun onShareClicked() {
        setValue(Codes.SHARE_CLICKED)
    }

    fun onBackPressed() {
        setValue(Codes.BACK_PRESSED)
    }
}