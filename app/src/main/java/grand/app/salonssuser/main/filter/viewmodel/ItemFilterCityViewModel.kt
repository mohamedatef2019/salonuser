package grand.app.salonssuser.main.filter.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.filter.response.FilterDataCityItem

class ItemFilterCityViewModel(var item: FilterDataCityItem) : BaseViewModel()
