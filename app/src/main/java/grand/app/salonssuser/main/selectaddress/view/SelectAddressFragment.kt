package grand.app.salonssuser.main.selectaddress.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentSelectAddressBinding
import grand.app.salonssuser.dialogs.dialogcity.DialogCityFragment
import grand.app.salonssuser.main.selectaddress.response.CityItem
import grand.app.salonssuser.main.selectaddress.viewmodel.SelectAddressViewModel
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params

import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentResponse
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCurrencyISO
import com.myfatoorah.sdk.utils.MFCurrencyISO.SAUDI_ARABIA_SAR

import com.myfatoorah.sdk.views.MFResult

import com.myfatoorah.sdk.views.MFSDK
import com.myfatoorah.sdk.views.MFSDK.executePayment
import com.myfatoorah.sdk.views.MFSDK.init
import com.myfatoorah.sdk.views.MFSDK.initiatePayment
import com.myfatoorah.sdk.views.MFSDK.setUpActionBar


class SelectAddressFragment : BaseHomeFragment(), androidx.lifecycle.Observer<Any?>
{
    lateinit var binding: FragmentSelectAddressBinding
    lateinit var viewModel: SelectAddressViewModel
    val fragmentArgs : SelectAddressFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_address, container, false)




        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(SelectAddressViewModel::class.java)
        viewModel = SelectAddressViewModel()

        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.request = fragmentArgs.orderData

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)
    }

    override fun onChanged(it: Any?)
    {
        when (it) {
            null -> return
            else -> when (it) {
                Codes.ADDRESS_SAVED -> {
                    val action = SelectAddressFragmentDirections.selectAddressToOrderReview(viewModel.request, Codes.ORDER_HOME)
                    findNavController().navigate(action)
                }
                Codes.SHOW_CITY_DIALOG -> {
                    when {
                        viewModel.citiesResponse.citiesList!!.isNotEmpty() -> {
                            val bundle = Bundle()
                            bundle.putSerializable(Params.CITIES_RESPONSE, viewModel.citiesResponse)
                            Utils.startDialogActivity(requireActivity(), DialogCityFragment::class.java.name, Codes.DIALOG_CITY_REQUEST, bundle)
                        }
                    }
                }
                Codes.EMPTY_CITY -> {
                    showToast(getString(R.string.msg_empty_city), 1)
                }
                Codes.EMPTY_AREA -> {
                    showToast(getString(R.string.msg_empty_area), 1)
                }
                Codes.EMPTY_STREET_NAME -> {
                    showToast(getString(R.string.msg_empty_street_name), 1)
                }
                Codes.EMPTY_BUILDING_NUM -> {
                    showToast(getString(R.string.msg_empty_building_num), 1)
                }
                Codes.EMPTY_FLOOR -> {
                    showToast(getString(R.string.msg_empty_floor), 1)
                }
                Codes.EMPTY_MARK -> {
                    showToast(getString(R.string.msg_empty_mark), 1)
                }
                Codes.EMPTY_PHONE -> {
                    showToast(getString(R.string.msg_empty_phone), 1)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode)
        {
            /* When User back from Select Date and Time */
            Codes.DIALOG_CITY_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.CITY_ITEM) -> {
                                               var cityItem =  data.getSerializableExtra(Params.CITY_ITEM) as CityItem
                                                viewModel.request.city_id = cityItem.id
                                                binding.tvCity.text = cityItem.name
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



}