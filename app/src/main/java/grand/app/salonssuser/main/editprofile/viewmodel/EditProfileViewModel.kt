package grand.app.salonssuser.main.editprofile.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.editprofile.request.EditProfileRequest
import grand.app.salonssuser.main.editprofile.response.UpdateProfileResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import grand.app.salonssuser.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class EditProfileViewModel : BaseViewModel()
{
    var request = EditProfileRequest()

    fun onUpdateClicked() {
        setClickable()
        if (request.userImg != null) {
            updateProfileWithImg()
        }
        else {
            updateProfileWithoutImg()
        }
    }
    fun onChooseImgClicked() {
        setClickable()
        setValue(Codes.CHOOSE_IMAGE_CLICKED)
    }

    private fun updateProfileWithoutImg() {

        obsIsProgress.set(true)
        requestCall<UpdateProfileResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateProfileWithoutImg(request) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveUserData(res.userData)
                    apiResponseLiveData.value = ApiResponse.successMessage(res.msg)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    private fun updateProfileWithImg() {

        obsIsProgress.set(true)
        requestCall<UpdateProfileResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateProfileWithImg(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveUserData(res.userData)
                    apiResponseLiveData.value = ApiResponse.successMessage(res.msg)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.USER_PROFILE_IMAGE_REQUEST -> {
                request.userImg = path.stringPathToFile()
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsLayout.set(LoadingStatus.FULL)
                request.name = PrefMethods.getUserData()!!.name.toString()
                request.email = PrefMethods.getUserData()!!.email.toString()
                request.phone = PrefMethods.getUserData()!!.phone.toString()
                notifyChange()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}