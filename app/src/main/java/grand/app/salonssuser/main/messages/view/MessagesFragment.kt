package grand.app.salonssuser.main.messages.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.fxn.pix.Pix
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.databinding.FragmentMessagesBinding
import grand.app.salonssuser.main.messages.response.MessagesResponse
import grand.app.salonssuser.main.messages.response.SendMessageResponse
import grand.app.salonssuser.main.messages.viewmodel.MessagesViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.fcm.FirebaseNotficationResponse
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class MessagesFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding: FragmentMessagesBinding
    lateinit var viewModel: MessagesViewModel
    private val fragmentArgs : MessagesFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_messages, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(MessagesViewModel::class.java)
        viewModel = MessagesViewModel()
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(false)

        observe(Params.msgLiveData){
            when (it) {
                1 -> {
                    viewModel.refreshMessages(fragmentArgs.salonId)
                }
            }
        }

        viewModel.getMessages(fragmentArgs.salonId, fragmentArgs.salonName, fragmentArgs.salonImg)
        viewModel.request.salon_id = fragmentArgs.salonId
        viewModel.request.receiver_id = fragmentArgs.receiverId

        (requireActivity() as MainActivity).chatMutableLiveData.observe(viewLifecycleOwner) { o ->
            when (o) {
                is FirebaseNotficationResponse -> {
                    val response: FirebaseNotficationResponse = o as FirebaseNotficationResponse
                    viewModel.refreshMessages(fragmentArgs.salonId)
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SendMessageResponse -> {
                            binding.imgTest.visibility = View.GONE
                            binding.etMsg.setText("")
                        }
                        is MessagesResponse -> {
                            binding.rvChats.smoothScrollToPosition(viewModel.messagesList.size - 1)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }


    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it)
        {
            Codes.EMPTY_MESSAGE -> {
                showToast(getString(R.string.msg_empty_message), 1)
            }
            Codes.PICK_CHAT_IMAGE -> {
                pickImage(it as Int)
            }
            Codes.BACK_PRESSED -> {
                findNavController().navigateUp()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.PICK_CHAT_IMAGE -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                viewModel.gotImage(requestCode, array[0])
                                binding.imgTest.visibility = View.VISIBLE
                                binding.imgTest.setImageURI(returnValue[0].toUri())
                            }
                        }
                    }
                }
            }
        }
    }
}