package grand.app.salonssuser.main.filter.response

import com.google.gson.annotations.SerializedName
import grand.app.salonssuser.main.allsalonss.response.SalonItem

data class FilterSalonsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val filterSalonsData: FilterSalonsData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataItem(

	@field:SerializedName("img")
	val img: Img? = null,

	@field:SerializedName("is_favorite")
	val isFavorite: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("min_price")
	val minPrice: Int? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("city")
	val city: City? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("salon_type")
	val salonType: Int? = null,

	@field:SerializedName("logo")
	val logo: Logo? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("lat")
	val lat: String? = null
)

data class FilterSalonsData(

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val filterList: List<SalonItem?>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class Logo(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Img(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class City(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
