package grand.app.salonssuser.main.bookings.old.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.bookings.response.MyBookingsItem

class ItemOldBookingViewModel(var item: MyBookingsItem) : BaseViewModel()