package grand.app.salonssuser.main.addsalon.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Pix
import grand.app.salonssuser.main.conditions.ConditionsActivity
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.MainActivity
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.FragmentAddSalonBinding
import grand.app.salonssuser.dialogs.message.DialogMessageFragment
import grand.app.salonssuser.dialogs.providertype.DialogProviderTypeFragment
import grand.app.salonssuser.dialogs.providertype.ProviderTypeItem
import grand.app.salonssuser.main.addsalon.response.AddSalonResponse
import grand.app.salonssuser.main.addsalon.viewmodel.AddSalonViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class AddSalonFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding: FragmentAddSalonBinding
    lateinit var viewModel: AddSalonViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_salon, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(AddSalonViewModel::class.java)
        viewModel = AddSalonViewModel()
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddSalonResponse -> {
                            val bundle = Bundle()
                            bundle.putString(Params.DIALOG_CONFIRM_MESSAGE, BaseApp.getInstance.getString(R.string.msg_salon_added))
                            bundle.putString(Params.DIALOG_CONFIRM_SUB_MESSAGE, BaseApp.getInstance.getString(R.string.msg_salon_added_desc))
                            Utils.startDialogActivity(requireActivity(), DialogMessageFragment::class.java.name, Codes.DIALOG_MESSAGE_REQUEST, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?)
    {
        if(it == null) return
        when (it) {
            Codes.EMPTY_USER_IMAGE -> {
                showToast(getString(R.string.msg_empty_user_image), 1)
            }
            Codes.SHOW_PROVIDER_DIALOG -> {
                Utils.startDialogActivity(requireActivity(), DialogProviderTypeFragment::class.java.name, Codes.DIALOG_PROVIDER_REQUEST, null)
            }
            Codes.SELECT_CERTIFICATE_IMG -> {
                pickImage(it as Int)
            }
            Codes.SELECT_LICENSE_IMG -> {
                pickImage(it as Int)
            }
            Codes.LOGIN_INTENT -> {
                findNavController().navigate(R.id.register_to_login)
            }
            Codes.EMPTY_NAME -> {
                showToast(getString(R.string.msg_empty_name), 1)
            }
            Codes.EMAIL_EMPTY -> {
                showToast(getString(R.string.msg_empty_email), 1)
            }
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_phone), 1)
            }
            Codes.EMPTY_SALON_NAME -> {
                showToast(getString(R.string.msg_empty_salon_name), 1)
            }
            Codes.EMPTY_TYPE -> {
                showToast(getString(R.string.msg_empty_type), 1)
            }
            Codes.INVALID_PHONE -> {
                showToast(getString(R.string.msg_invalid_phone), 1)
            }
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password), 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password), 1)
            }
            Codes.EMPTY_SALON_LICENSE -> {
                showToast(getString(R.string.msg_empty_salon_license), 1)
            }
            Codes.EMPTY_SALON_CERTIFICATE -> {
                showToast(getString(R.string.msg_empty_salon_certificate), 1)
            }
            Codes.EMPTY_IPAN_NUMBER -> {
                showToast(getString(R.string.hint_salon_ipan), 1)
            }
            Codes.EMPTY_payment_NUMBER -> {
                showToast(getString(R.string.hint_salon_payment), 1)
            }
            Codes.TERMS_CLICKED -> {
                requireActivity().startActivityForResult(Intent(requireActivity(), ConditionsActivity::class.java), Codes.ACCEPT_CONDITIONS)
            }
            Codes.TERMS_REJECTED -> {
                showToast(getString(R.string.msg_agree_terms), 1)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.SELECT_CERTIFICATE_IMG -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                viewModel.gotImage(requestCode, array[0])
                                binding.imgCertificate.visibility = View.VISIBLE
                            }
                        }
                    }
                    Codes.SELECT_LICENSE_IMG -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                viewModel.gotImage(requestCode, array[0])
                                binding.imgLicense.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }
        }

        when {
            requestCode == Codes.ACCEPT_CONDITIONS && data != null -> {
                when {
                    data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                        when {
                            data.getIntExtra(Params.DIALOG_CLICK_ACTION,1) == 1 -> {
                                binding.cbTerms.isChecked = true
                                viewModel.obsIsTermsAccepted.set(true)
                                Timber.e("mou3az_terms : agree")
                            }
                        }
                    }
                }
            }
        }

        when {
            requestCode == Codes.DIALOG_MESSAGE_REQUEST && data != null -> {
                when {
                    data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                        when {
                            data.getIntExtra(Params.DIALOG_CLICK_ACTION,1) == 1 -> {
                                requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                                requireActivity().finishAffinity()
                            }
                        }
                    }
                }
            }
        }

        when (requestCode) {
            Codes.DIALOG_PROVIDER_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.PROVIDER_TYPE) -> {
                                                var item = data.getSerializableExtra(Params.PROVIDER_TYPE) as ProviderTypeItem
                                                viewModel.request.type = item.id
                                                binding.tvType.text = item.name
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}