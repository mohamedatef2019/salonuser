package grand.app.salonssuser.main.aboutapp.response

import com.google.gson.annotations.SerializedName

data class AboutAppResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val aboutData: AboutData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class AboutItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)

data class AboutData(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("abouts")
	val aboutsList: List<AboutItem?>? = null
)
