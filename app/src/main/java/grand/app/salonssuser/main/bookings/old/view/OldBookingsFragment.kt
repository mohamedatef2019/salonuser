package grand.app.salonssuser.main.bookings.old.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.databinding.FragmentOldBookingsBinding
import grand.app.salonssuser.main.bookings.old.viewmodel.OldBookingViewModel
import grand.app.salonssuser.main.bookings.view.MyBookingsFragmentDirections
import grand.app.salonssuser.main.home.response.HomeResponse
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class OldBookingsFragment : BaseHomeFragment()
{
    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    lateinit var binding: FragmentOldBookingsBinding
    lateinit var viewModel: OldBookingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_old_bookings, container, false)
        return binding.root
    }


    override fun onStart() {
        super.onStart()
        viewModel.getUserStatus()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(OldBookingViewModel::class.java)
        viewModel = OldBookingViewModel()
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData){
            val action = MyBookingsFragmentDirections.bookingsToBookingDetails(it!!.id!!)
            navController.navigate(action)
        }

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getOldBookings()
        }

        binding.rvBookings.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            var lastX = 0
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                when (e.action) {
                    MotionEvent.ACTION_DOWN -> lastX = e.x.toInt()
                    MotionEvent.ACTION_MOVE -> {
                        val isScrollingRight = e.x < lastX
                        if (isScrollingRight && (binding.rvBookings.layoutManager as LinearLayoutManager).
                                findLastCompletelyVisibleItemPosition() == binding.rvBookings.adapter!!.itemCount - 1 ||
                                !isScrollingRight && (binding.rvBookings.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0
                        ) {
                            //  binding.p.setUserInputEnabled(true)
                        } else {
                            //  viewPager.setUserInputEnabled(false)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        lastX = 0
                        // viewPager.setUserInputEnabled(true)
                    }
                }
                return false
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
        })

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Params.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {

            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is HomeResponse -> {
                            // setupSlider(it.data.homeData!!.homeSlidersList as List<SliderItem>)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()

        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Params.isAskedToLogin = 0
            }
        }
    }
}