package grand.app.salonssuser.main.salondetails.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemSalonCategoryBinding
import grand.app.salonssuser.main.salondetails.response.SalonCategoryItem
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem
import grand.app.salonssuser.main.salondetails.viewmodel.ItemSalonCategoryViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class SalonCategoriesAdapter : RecyclerView.Adapter<SalonCategoriesAdapter.SalonCategoriesHolder>()
{
    var itemsList: ArrayList<SalonCategoryItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SalonCategoryItem>()
    var selectedPosition = -1
    var selectedServices: ArrayList<SalonServiceItem> = ArrayList()
    var servicesIds: ArrayList<Int> = ArrayList()
    var totalPrice : Double = 0.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonCategoriesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonCategoryBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_category, parent, false)
        return SalonCategoriesHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonCategoriesHolder, position: Int) {
        val itemViewModel = ItemSalonCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.tvTitle.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemsList[selectedPosition].isSelected = true
                }
                selectedPosition == position -> {
                    notifyItemChanged(position)
                    itemsList[selectedPosition].isSelected = true
                    selectedPosition = -1
                }
            }
        }

        itemViewModel.adapter.itemServiceLiveData.observeForever {
            when {
                it!!.isSelected -> {
                    Timber.e("total_price " + totalPrice)
                    Timber.e("service_price " + it.price)
                    selectedServices.add(it)
                    servicesIds.add(it.id!!.toInt())
                    totalPrice += it.price!!.toDouble()
                    Timber.e("total_price " + totalPrice)
                }
                else -> {
                    Timber.e("total_price " + totalPrice)
                    Timber.e("service_price " + it.price)
                    selectedServices.remove(it)
                    servicesIds.remove(it.id!!.toInt())
                    totalPrice -= it.price!!.toDouble()
                    Timber.e("total_price " + totalPrice)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonCategoryItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonCategoriesHolder(val binding: ItemSalonCategoryBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rvService.visibility = View.VISIBLE
                }
                else -> {
                    binding.rvService.visibility = View.GONE
                }
            }
        }
    }
}
