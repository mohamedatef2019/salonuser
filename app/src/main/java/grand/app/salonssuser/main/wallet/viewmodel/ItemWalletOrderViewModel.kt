package grand.app.salonssuser.main.wallet.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.wallet.response.WalletOrderItem

class ItemWalletOrderViewModel(var item: WalletOrderItem) : BaseViewModel()