package grand.app.salonssuser.main.salondetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem

class ItemSalonServiceViewModel(var item: SalonServiceItem) : BaseViewModel()