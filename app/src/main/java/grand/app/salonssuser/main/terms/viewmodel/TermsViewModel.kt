package grand.app.salonssuser.main.terms.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.terms.response.TermsItem
import grand.app.salonssuser.main.terms.response.TermsResponse
import grand.app.salonssuser.main.terms.view.TermsAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class TermsViewModel : BaseViewModel()
{
    var adapter = TermsAdapter()
    var termsImg = ObservableField<String>()

    init {
        getTerms()
    }

    fun getTerms()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<TermsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getTerms() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    termsImg.set(res.termsData!!.img)
                    adapter.updateList(res.termsData.termsList as ArrayList<TermsItem>)
                    notifyChange()
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}