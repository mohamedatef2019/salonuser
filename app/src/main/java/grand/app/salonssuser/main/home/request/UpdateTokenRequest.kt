package grand.app.salonssuser.main.home.request

data class UpdateTokenRequest(
        var firebase_token : String ?= null
)
