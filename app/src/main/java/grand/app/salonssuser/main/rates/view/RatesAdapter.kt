package grand.app.salonssuser.main.rates.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemUserRateBinding
import grand.app.salonssuser.main.rates.response.RatesItem
import grand.app.salonssuser.main.rates.viewmodel.ItemRateViewModel
import java.util.*

class RatesAdapter : RecyclerView.Adapter<RatesAdapter.RatesHolder>()
{
    var itemsList: ArrayList<RatesItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemUserRateBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_user_rate, parent, false)
        return RatesHolder(binding)
    }

    override fun onBindViewHolder(holder: RatesHolder, position: Int) {
        val itemViewModel = ItemRateViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<RatesItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class RatesHolder(val binding: ItemUserRateBinding) : RecyclerView.ViewHolder(binding.root)
}
