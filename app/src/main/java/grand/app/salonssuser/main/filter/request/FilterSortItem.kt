package grand.app.salonssuser.main.filter.request

data class FilterSortItem(
    var id: Int? = null,
    var name: String? = null
)
