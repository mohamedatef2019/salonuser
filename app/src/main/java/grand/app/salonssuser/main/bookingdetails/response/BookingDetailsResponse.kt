package grand.app.salonssuser.main.bookingdetails.response

import com.google.gson.annotations.SerializedName

data class BookingDetailsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val bookingData: BookingDetailsData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class BookingServiceItem(

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class BookingDetailsData(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("services")
	val bookingServicesList: List<BookingServiceItem?>? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
