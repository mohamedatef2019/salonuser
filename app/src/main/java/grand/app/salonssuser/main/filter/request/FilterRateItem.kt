package grand.app.salonssuser.main.filter.request

data class FilterRateItem(
    var id: Int? = null,
    var name: String? = null
)
