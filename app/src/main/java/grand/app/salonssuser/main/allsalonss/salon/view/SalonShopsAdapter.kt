package grand.app.salonssuser.main.allsalonss.salon.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemSalonShopBinding
import grand.app.salonssuser.main.allsalonss.response.SalonItem
import grand.app.salonssuser.main.allsalonss.salon.viewmodel.ItemSalonShopViewModel
import grand.app.salonssuser.utils.SingleLiveEvent
import java.util.*

class SalonShopsAdapter : RecyclerView.Adapter<SalonShopsAdapter.SalonShopsHolder>()
{
    var itemsList: ArrayList<SalonItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SalonItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonShopsAdapter.SalonShopsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonShopBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_shop, parent, false)
        return SalonShopsHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonShopsHolder, position: Int) {
        val itemViewModel = ItemSalonShopViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonShopsHolder(val binding: ItemSalonShopBinding) : RecyclerView.ViewHolder(binding.root)
}
