package grand.app.salonssuser.main.chat.response

import com.google.gson.annotations.SerializedName
import grand.app.salonssuser.main.messages.response.Receiver

data class ChatsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val chatsList: List<ChatItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class ChatItem(

		@field:SerializedName("salon_id")
	val salonId: Int? = null,

		@field:SerializedName("img")
	val img: String? = null,

		@field:SerializedName("name")
	val name: String? = null,

		@field:SerializedName("created_at")
	val createdAt: String? = null,

		@field:SerializedName("read_at")
	val readAt: String? = null,

		@field:SerializedName("id")
	val id: Int? = null,

		@field:SerializedName("text")
	val text: String? = null,

		@field:SerializedName("counter")
	val counter: Int? = null,

		@field:SerializedName("sender_id")
	val senderId: Int? = null,

		@field:SerializedName("receiver")
	val receiver: Receiver? = null
)
