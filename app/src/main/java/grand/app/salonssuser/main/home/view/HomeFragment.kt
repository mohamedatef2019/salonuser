package grand.app.salonssuser.main.home.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentHomeBinding
import grand.app.salonssuser.location.map.MapsActivity
import grand.app.salonssuser.location.util.AddressItem
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.home.response.HomeResponse
import grand.app.salonssuser.main.home.response.HomeSliderItem
import grand.app.salonssuser.main.home.viewmodel.HomeViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber
import java.util.ArrayList

class HomeFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: HomeViewModel
    lateinit var sliderView : SliderView
    var covers = ArrayList<HomeSliderItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(true)

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getHomePage()
        }

        binding.inputHomeSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        when {
            PrefMethods.getUserLocation() == null -> {
                val intent = Intent(requireActivity(), MapsActivity::class.java)
                startActivityForResult(intent, Codes.GET_LOCATION_FROM_MAP)
            }
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            if (!task.isSuccessful) {
                return@addOnCompleteListener
            }
            if (task.result != null) {
                if (PrefMethods.getUserData() != null)
                {
                    viewModel.updateToken(task.result!!)
                }
            }
        }

        observe(viewModel.bannersAdapter.itemLiveData) { bannerItem ->
            bannerItem?.let { item ->
                val action = HomeFragmentDirections.homeToSalonDetails(item.attributeId!!)
                findNavController().navigate(action)
            }
        }

        observe(viewModel.categoriesAdapter.itemLiveData) { bannerItem ->
            bannerItem?.let { item ->
                Params.isHome = 5
                Params.flag = 1
                Params.categoryid = item.id!!
                val action = HomeFragmentDirections.homeToAllSalons(item.id)
                findNavController().navigate(action)
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is HomeResponse -> {
                            covers = it.data.homeData!!.homeSlidersList as ArrayList<HomeSliderItem>
                            setupSlider(covers)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
         viewModel.getHomePage()
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it)
        {
            Codes.EMPTY_SALON_NAME -> {
                showToast(getString(R.string.msg_empty_salon_name), 1)
            }
            Codes.SEARCH_CLICKED -> {
                findNavController().navigate(HomeFragmentDirections.homeToSearch(viewModel.obsSearchNam.get().toString(), 3))
            }
        }
    }

    private fun setupSlider(covers: ArrayList<HomeSliderItem>) {
        viewModel.bannersAdapter.updateList(covers)
        sliderView = binding.sliderHomeBanner
        sliderView.setSliderAdapter(viewModel.bannersAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            /* When User back from Select Date and Time */
            Codes.GET_LOCATION_FROM_MAP -> {
                when {
                    data != null -> {
                        Timber.e("mou3aaz_back ata != null")
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                Timber.e("mou3aaz_back click action has added")
                                when (data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1)) {
                                    1 -> {
                                        val userAddress =
                                            data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM)
                                        PrefMethods.saveUserLocation(userAddress)
                                        viewModel.getHomePage()
                                        Timber.e("mou3aaz_back 1")
                                    }
                                    0 -> {
                                        Timber.e("mou3aaz_back 2")
                                        viewModel.getHomePage()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}