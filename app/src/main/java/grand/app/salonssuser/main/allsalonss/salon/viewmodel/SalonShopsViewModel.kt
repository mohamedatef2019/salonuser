package grand.app.salonssuser.main.allsalonss.salon.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.allsalonss.home.request.HomeShopsRequest
import grand.app.salonssuser.main.allsalonss.response.AllSalonsResponse
import grand.app.salonssuser.main.allsalonss.response.SalonItem
import grand.app.salonssuser.main.allsalonss.salon.view.SalonShopsAdapter
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.response.FilterSalonsResponse
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class SalonShopsViewModel : BaseViewModel()
{
    var adapter = SalonShopsAdapter()
    var obsSearchNam = ObservableField<String>()
    var request = HomeShopsRequest()

    fun getSalonShops() {
        obsLayout.set(LoadingStatus.SHIMMER)
        request.lat = PrefMethods.getUserLocation()?.lat
        request.lng = PrefMethods.getUserLocation()?.lng
        request.category_id = Params.categoryid
        request.type = 2
        requestCall<AllSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHomeShops(request)} })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.allSalonsData?.salonsList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.allSalonsData!!.salonsList as ArrayList<SalonItem>)
                        }
                    }
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }


    fun getFilteredSalons(filterRequest : FilterRequest) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<FilterSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().filterSalons(filterRequest) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.filterSalonsData!!.filterList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.filterSalonsData.filterList as ArrayList<SalonItem>)
                        }
                    }
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onFilterClicked() {
        setValue(Codes.FILTER_CLICKED)
    }

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun onMapClicked() {
        setValue(Codes.OPEN_MAP)
    }
}