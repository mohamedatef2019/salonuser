package grand.app.salonssuser.main.payment.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.google.gson.Gson
import com.myfatoorah.sdk.entity.executepayment.MFExecutePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentRequest
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentResponse
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCurrencyISO
import com.myfatoorah.sdk.views.MFResult
import com.myfatoorah.sdk.views.MFSDK
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentPaymentBinding
import grand.app.salonssuser.main.favorites.response.AddToFavResponse
import grand.app.salonssuser.main.orderreview.response.CreateOrderResponse
import grand.app.salonssuser.main.orderreview.viewmodel.OrderReviewViewModel
import grand.app.salonssuser.main.payment.viewmodel.PaymentViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.observe
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class PaymentFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentPaymentBinding
    lateinit var viewModel: OrderReviewViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false)

        MFSDK.init("https://api-sa.myfatoorah.com/",
            "cd-SAnf1wAjztqVa3K6OkGLDi7N7a9TbFH_d96VJ3Pa2tkl0yX9XdZYd58y8uDpIkzJKRVza5MPkiEOG0m85miURrWhxg1FfAcOFlrugp0KdoIWozI_B5faHoxoPt94STrovc5l0DshdecUZ4pRZNs5UmAog9kwzbD1tUk4kpjqL-e27gmHSDzfH3ckfhN-28WbdWUGB71zrBahGhB5hkIbTU9OCsxIc7NCX19prw_MQDsWnph_UT-FZf-fyqBzM3YamtqDwPr8XV9hRdZrp9gmwXHor_cIGetPI1nDagqzvmSVbIIu8JMyl-0sv_2cV1hwfmSoqX1c8z0h9sLxqA2JzlI_gtL7ug-VHRjJ4AETFho6onDVSIfluXr0Pvhsj610Hq_if0c82r-nOrTJCcAdC9oCyhRWe6nODjx2EZco4IxJnLg0CZRPUbpwUB3vX4iXOR8WtogQT9IkSn-EYBrD8c8fHSLnd12xCuuWzrC9mn4GOvkn6EDJK8r8znm-HdPpzHln-VyUIhs8U67KPVvwhqHHDSxRMAqhODwrfH3uvM_1d2UKzje3qPA9lbIoiFzKAzXokNemsSnbMW-96bWuMUkCAIHF_wY65FtjXmNeClrgzc-Cn1jr7u9VwJuXXt7RguhmduUAn0URsNnzwWrtvVDV8LN88BzMfSOYk2gA_iAwbyANNVh6tUNq7C5QFykMhBSM4pzyckU4FxG4eQvOW_AM")
        MFSDK.setUpActionBar("MyFatoorah Payment", R.color.colorAccent, R.color.colorPrimary, false)
        initPayment()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
       // viewModelMore = ViewModelProvider(this).get(PaymentViewModel::class.java)
        viewModel = OrderReviewViewModel()
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CreateOrderResponse -> {
                        }
                    }
                }

                else -> {

                }
            }
        }

        viewModel.clickableLiveData.observe(viewLifecycleOwner) {
            executePayment();
        }
    }


    var TAG = "TAGSW"
    private fun initPayment() {

        val request = MFInitiatePaymentRequest(0.100, MFCurrencyISO.SAUDI_ARABIA_SAR)
        MFSDK.initiatePayment(
            request,
            MFAPILanguage.EN
        ) { result: MFResult<MFInitiatePaymentResponse?> ->
            if (result is MFResult.Success) {
                viewModel.createOrder(viewModel.request)

            } else if (result is MFResult.Fail)
            Unit
        }
    }

    private fun executePayment() {
        val request = MFExecutePaymentRequest(2, 0.100)
        MFSDK.executePayment(
            requireActivity(), request, MFAPILanguage.EN
        ) { invoiceId: String, result: MFResult<MFGetPaymentStatusResponse?> ->
            if (result is MFResult.Success) {

            } else if (result is MFResult.Fail)
            Unit
        }
    }

}