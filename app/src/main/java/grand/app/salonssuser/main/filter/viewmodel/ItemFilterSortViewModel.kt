package grand.app.salonssuser.main.filter.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.filter.request.FilterSortItem

class ItemFilterSortViewModel(var item: FilterSortItem) : BaseViewModel()
