package grand.app.salonssuser.main.more.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import es.dmoral.toasty.Toasty
import grand.app.salonssuser.main.more.viewmodel.MoreViewModel
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentMoreBinding
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes

class MoreFragment : BaseHomeFragment(), androidx.lifecycle.Observer<Any?>
{
    lateinit var binding: FragmentMoreBinding
    lateinit var viewModel: MoreViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MoreViewModel::class.java)
        viewModel = MoreViewModel()
        binding.viewModel = viewModel

        showBottomBar(true)

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)
    }

    override fun onChanged(it: Any?) {

        if (it == null) return
        when (it) {
            Codes.ABOUT_US_CLICKED -> {
                findNavController().navigate(R.id.more_to_about)
            }
            Codes.CONTACT_US_CLICKED -> {
                findNavController().navigate(R.id.more_to_contact_us)
            }
            Codes.TERMS_CLICKED -> {
                findNavController().navigate(R.id.more_to_terms)
            }
            Codes.SHARE_APP_CLICKED -> {
                Utils.shareApp(requireActivity(), "مشاركة تطبيق صالونات مع اصدقائك",
                    "https://play.google.com/store/apps/details?id=grand.app.salonssuser")
            }
            Codes.RATE_APP_CLICKED -> {
                val packageName = "grand.app.salonssuser"
                val uri = Uri.parse("market://details?id=$packageName")
                val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
                try {
                    startActivity(myAppLinkToMarket)
                }
                catch (e: ActivityNotFoundException) {
                    Toasty.warning(requireActivity(), "Impossible to find an application for the market").show()
                }
            }
        }
    }
}