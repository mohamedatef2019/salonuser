package grand.app.salonssuser.main.search.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.search.response.SearchSalonItem

class ItemSearchViewModel(var item: SearchSalonItem) : BaseViewModel()