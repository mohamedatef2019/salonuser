package grand.app.salonssuser.main.changepassword.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonssuser.main.changepassword.viewmodel.ChangePassViewModel
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.auth.AuthActivity
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.auth.resetpass.response.ResetPassResponse
import grand.app.salonssuser.databinding.FragmentChangePasswordBinding
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class ChangePassFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding: FragmentChangePasswordBinding
    lateinit var viewModel: ChangePassViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChangePassViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ResetPassResponse -> {
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.LOGIN_CLICKED -> {
                Params.isAskedToLogin = 1
                requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
            }
            Codes.OLD_PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_old_password) , 1)
            }
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password) , 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password) , 1)
            }
            Codes.CONFIRM_PASS_EMPTY -> {
                showToast(getString(R.string.msg_empty_confirm_password) , 1)
            }
            Codes.PASSWORD_NOT_MATCH -> {
                showToast(getString(R.string.msg_not_match) , 1)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(Params.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Params.isAskedToLogin = 0
            }
        }
    }
}