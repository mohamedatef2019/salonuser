package grand.app.salonssuser.main.allsalonss.salon.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentSalonShopsBinding
import grand.app.salonssuser.main.allsalonss.salon.viewmodel.SalonShopsViewModel
import grand.app.salonssuser.main.allsalonss.view.AllSalonsFragmentDirections
import grand.app.salonssuser.main.filter.request.FilterRequest
import grand.app.salonssuser.main.filter.view.FilterActivity
import grand.app.salonssuser.main.home.response.HomeResponse
import grand.app.salonssuser.location.salonsmarker.MapSalonsMarkerActivity
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Params
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class SalonShopsFragment : BaseHomeFragment()
{
    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    lateinit var binding: FragmentSalonShopsBinding
    lateinit var viewModel: SalonShopsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_shops, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(SalonShopsViewModel::class.java)
        viewModel = SalonShopsViewModel()
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData){
            val action = AllSalonsFragmentDirections.allSalonsToSalonDetails(it!!.id!!)
            navController.navigate(action)
        }

        viewModel.getSalonShops()

        binding.inputHomeSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        binding.rvSalons.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            var lastX = 0
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                when (e.action) {
                    MotionEvent.ACTION_DOWN -> lastX = e.x.toInt()
                    MotionEvent.ACTION_MOVE -> {
                        val isScrollingRight = e.x < lastX
                        if (isScrollingRight && (binding.rvSalons.layoutManager as LinearLayoutManager).
                                findLastCompletelyVisibleItemPosition() == binding.rvSalons.adapter!!.itemCount - 1 ||
                                !isScrollingRight && (binding.rvSalons.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0
                        ) {
                            //  binding.p.setUserInputEnabled(true)
                        } else {
                            //  viewPager.setUserInputEnabled(false)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        lastX = 0
                        // viewPager.setUserInputEnabled(true)
                    }
                }
                return false
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
        })

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getSalonShops()
        }

        observe(viewModel.apiResponseLiveData) {

            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is HomeResponse -> {
                            // setupSlider(it.data.homeData!!.homeSlidersList as List<SliderItem>)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.OPEN_MAP -> {
                    val intent = Intent(requireActivity(), MapSalonsMarkerActivity::class.java)
                    startActivityForResult(intent, Codes.SALON_MAP_REQUEST)
                }
                Codes.EMPTY_SALON_NAME -> {
                    showToast(getString(R.string.msg_empty_salon_name), 1)
                }
                Codes.FILTER_CLICKED -> {
                    val intent = Intent(requireActivity(), FilterActivity::class.java)
                    intent.putExtra(Params.SERVICE_TYPE, 3)
                    startActivityForResult(intent, Codes.FILTER_REQUEST)
                }
                Codes.SEARCH_CLICKED -> {
                   navController.navigate(AllSalonsFragmentDirections.allSalonsToSearch(viewModel.obsSearchNam.get().toString(), 2))
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            /* When User back from Select Date and Time */
            Codes.FILTER_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                           var filterRequest = data.getSerializableExtra(Params.FILTER_REQUEST) as FilterRequest
                                            filterRequest.type = 2
                                            viewModel.getFilteredSalons(filterRequest)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            Codes.SALON_MAP_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Timber.tag("mou3az_salah").e( " click action truee")

                                        var salonId = data.getIntExtra(Params.SALON_ID, 1)
                                        val action = AllSalonsFragmentDirections.allSalonsToSalonDetails(salonId)
                                        navController.navigate(action)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

