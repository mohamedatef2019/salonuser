package grand.app.salonssuser.main.rates.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.FragmentRatesBinding
import grand.app.salonssuser.main.profile.viewmodel.ProfileViewModel
import grand.app.salonssuser.main.rates.viewmodel.RatesViewModel

class RatesFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentRatesBinding
    lateinit var viewModel: RatesViewModel
    val fragmentArgs : RatesFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rates, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(RatesViewModel::class.java)
        viewModel = RatesViewModel()

        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.getRates(fragmentArgs.salonId)
    }
}