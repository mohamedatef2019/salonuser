package grand.app.salonssuser.main.profile.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.utils.constants.Codes

class ProfileViewModel : BaseViewModel()
{
    fun onEditClicked() {
        setValue(Codes.EDIT_CLICKED)
    }
    fun onWalletClicked() {
        setValue(Codes.WALLET_CLICKED)
    }
    fun onBookingsClicked() {
        setValue(Codes.BOOKING_CLICKED)
    }
    fun onAddSalonClicked() {
        setValue(Codes.ADD_SALON_CLICKED)
    }
    fun onLogoutClicked() {
        setValue(Codes.LOGOUT_CLICK)
    }

    fun onChangePassClicked() {
        setValue(Codes.CHANGE_PASS_CLICKED)
    }
}