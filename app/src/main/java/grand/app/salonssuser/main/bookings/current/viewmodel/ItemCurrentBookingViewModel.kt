package grand.app.salonssuser.main.bookings.current.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.bookings.response.MyBookingsItem

class ItemCurrentBookingViewModel(var item: MyBookingsItem) : BaseViewModel()