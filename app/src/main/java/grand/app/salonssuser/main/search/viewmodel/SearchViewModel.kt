package grand.app.salonssuser.main.search.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.search.request.SearchRequest
import grand.app.salonssuser.main.search.response.SearchResponse
import grand.app.salonssuser.main.search.response.SearchSalonItem
import grand.app.salonssuser.main.search.view.SearchAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class SearchViewModel : BaseViewModel()
{
    var adapter = SearchAdapter()
    var obsName = ObservableField<String>()
    var obsEmptyText = ObservableField<String>(getString(R.string.all_salons_will_show_here))
    var request = SearchRequest()

    init {
        obsLayout.set(LoadingStatus.EMPTY)
    }

    fun onSearchClicked() {
        when {
            obsName.get() == null -> {
                setValue(Codes.NAME_EMPTY)
            }
            else -> {
                searchSalons()
            }
        }
    }

    fun searchSalons()
    {
        obsIsProgress.set(true)
        when (request.type) {
            3 -> {
                request = SearchRequest(obsName.get())
            }
            else -> {
                val type = request.type
                request = SearchRequest(obsName.get(), type)
            }
        }
        requestCall<SearchResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().searchSalons(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    when {
                        res.searchData!!.searchSalonsList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                            obsEmptyText.set(getString(R.string.no_salons_found))
                            adapter.clearList()
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.searchData.searchSalonsList as ArrayList<SearchSalonItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}