package grand.app.salonssuser.main.messages.response

import com.google.gson.annotations.SerializedName

data class MessagesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val messagesList: List<MessageItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MessageItem(

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("receiver")
	val receiver: Receiver? = null,

	@field:SerializedName("sender")
	val sender: Sender? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("read_at")
	val readAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("media")
	val mediaList: List<MessageMediaItem?>? = null,

	@field:SerializedName("sender_id")
	val senderId: Int? = null
)

data class MessageMediaItem(

		@field:SerializedName("salon_id")
		val salonId: Int? = null,

		@field:SerializedName("message_id")
		val messageId: Int? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("media")
		val media: String? = null,

		@field:SerializedName("type")
		val type: Int? = null
)

data class Receiver(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Sender(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class MediaItem(

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("message_id")
	val messageId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("media")
	val media: String? = null,

	@field:SerializedName("type")
	val type: Int? = null
)
