package grand.app.salonssuser.main.moresalondetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.moresalondetails.view.SalonDaysAdapter
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import grand.app.salonssuser.main.salondetails.response.SalonDetailsData
import grand.app.salonssuser.main.salondetails.view.SalonSlidersAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import java.util.ArrayList

class MoreSalonDetailsViewModel : BaseViewModel()
{
    var salonDetails = SalonDetailsData()
    var obsRate : Float = 0f
    var sliderAdapter = SalonSlidersAdapter()
    var workingDaysAdapter = SalonDaysAdapter()

    fun setSalonData(salonData : SalonDetailsData) {
        setValue(Codes.HOME_CLICKED)
        salonDetails = salonData
        obsRate = salonData.rate!!.toFloat()
        apiResponseLiveData.value = ApiResponse.success(salonData)
        workingDaysAdapter.updateList(salonData.daysList as ArrayList<SalonDayItem>)
        notifyChange()
    }

    fun onHomeClicked() {
        setValue(Codes.HOME_CLICKED)
    }

    fun onSalonClicked() {
        setValue(Codes.SALON_CLICKED)
    }

    fun onFaceClicked() {
        setValue(Codes.FACE_CLICKED)
    }

    fun onTwitterClicked() {
        setValue(Codes.TWITTER_CLICKED)
    }

    fun onInstaClicked() {
        setValue(Codes.INSTAGRAM_CLICKED)
    }

    fun onPhoneClicked() {
        setValue(Codes.PHONE_CLICKED)
    }
    fun onRateClicked() {
        setValue(Codes.RATE_ORDER_CLICKED)
    }
}