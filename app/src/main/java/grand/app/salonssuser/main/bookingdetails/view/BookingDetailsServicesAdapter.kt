package grand.app.salonssuser.main.bookingdetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemBookingDetailsServicesBinding
import grand.app.salonssuser.main.bookingdetails.response.BookingServiceItem
import grand.app.salonssuser.main.bookingdetails.viewmodel.ItemBookingDetailsServiceViewModel
import java.util.*

class BookingDetailsServicesAdapter : RecyclerView.Adapter<BookingDetailsServicesAdapter.BookingDetailsServicesHolder>()
{
    var itemsList: ArrayList<BookingServiceItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingDetailsServicesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBookingDetailsServicesBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_booking_details_services, parent, false)
        return BookingDetailsServicesHolder(binding)
    }

    override fun onBindViewHolder(holder: BookingDetailsServicesHolder, position: Int) {
        val itemViewModel = ItemBookingDetailsServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<BookingServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BookingDetailsServicesHolder(val binding: ItemBookingDetailsServicesBinding) : RecyclerView.ViewHolder(binding.root)
}
