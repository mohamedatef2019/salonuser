package grand.app.salonssuser.main.chat.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.chat.response.ChatItem
import grand.app.salonssuser.main.chat.response.ChatsResponse
import grand.app.salonssuser.main.chat.view.ChatsAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class ChatsViewModel : BaseViewModel() {

    var adapter = ChatsAdapter()
    fun getChats() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<ChatsResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getChats() } })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.chatsList!!.isNotEmpty() -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.chatsList as ArrayList<ChatItem>)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getChats()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}