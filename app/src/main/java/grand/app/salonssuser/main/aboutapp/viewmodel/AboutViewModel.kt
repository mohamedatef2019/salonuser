package grand.app.salonssuser.main.aboutapp.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.aboutapp.response.AboutAppResponse
import grand.app.salonssuser.main.aboutapp.response.AboutItem
import grand.app.salonssuser.main.aboutapp.view.AboutAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class AboutViewModel : BaseViewModel()
{
    var adapter = AboutAdapter()
    var aboutImg = ObservableField<String>()

    init {
        getAboutData()
    }

    fun getAboutData() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<AboutAppResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getAboutApp() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    adapter.updateList(res.aboutData!!.aboutsList as ArrayList<AboutItem>)
                    aboutImg.set(res.aboutData.img)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }
}