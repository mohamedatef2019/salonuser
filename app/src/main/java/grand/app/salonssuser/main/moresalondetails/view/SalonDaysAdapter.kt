package grand.app.salonssuser.main.moresalondetails.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemSalonAppointmentBinding
import grand.app.salonssuser.main.moresalondetails.viewmodel.ItemSalonDayViewModel
import grand.app.salonssuser.main.salondetails.response.SalonDayItem
import java.util.*

class SalonDaysAdapter : RecyclerView.Adapter<SalonDaysAdapter.SalonDaysHolder>()
{
    var itemsList: ArrayList<SalonDayItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonDaysHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonAppointmentBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_appointment, parent, false)
        return SalonDaysHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonDaysHolder, position: Int) {
        val itemViewModel = ItemSalonDayViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        when (itemViewModel.item.jsonMemberSwitch) {
            0 -> {
                holder.binding.tvAppointmentDay.visibility = View.VISIBLE
                holder.binding.tvNotWork.visibility = View.VISIBLE
                holder.binding.tvWorkHours.visibility = View.GONE
            }
            else -> {
                holder.binding.tvAppointmentDay.visibility = View.VISIBLE
                holder.binding.tvNotWork.visibility = View.GONE
                holder.binding.tvWorkHours.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonDayItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonDaysHolder(val binding: ItemSalonAppointmentBinding) : RecyclerView.ViewHolder(binding.root)
}
