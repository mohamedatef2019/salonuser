package grand.app.salonssuser.main.orderreview.request

import grand.app.salonssuser.main.salondetails.response.SalonServiceItem
import java.io.Serializable

data class CreateOrderRequest (
        var salon_id: Int? = null,
        var date: String? = null,
        var time: String? = null,
        var extra_notes: String = "",
        var sub_category_id: ArrayList<Int>? = null,
        var servicesList: ArrayList<SalonServiceItem>? = null,
        var total_price: Double = 0.0,
        var payment_method: Int = 0,

        /* If User select Home Service */
        var city_id: Int ?= null,
        var area: String ?= null,
        var st: String ?= null,
        var building_no: String ?= null,
        var floor_no: String ?= null,
        var phone: String ?= null,
        var extra_phone: String ?= null,
        var mark: String ?= null,

        var serviceType: Int = 0

        ) : Serializable