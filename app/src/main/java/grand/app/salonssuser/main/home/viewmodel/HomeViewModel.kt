package grand.app.salonssuser.main.home.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.home.request.UpdateTokenRequest
import grand.app.salonssuser.main.home.response.CategoriesItem
import grand.app.salonssuser.main.home.response.HomeResponse
import grand.app.salonssuser.main.home.response.HomeSliderItem
import grand.app.salonssuser.main.home.response.UpdateTokenResponse
import grand.app.salonssuser.main.home.view.BannersAdapter
import grand.app.salonssuser.main.home.view.CategoriesAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class HomeViewModel : BaseViewModel()
{
    var bannersAdapter = BannersAdapter()
    var categoriesAdapter = CategoriesAdapter()
    var obsSearchNam = ObservableField<String>()

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun getHomePage() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<HomeResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHome() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    apiResponseLiveData.value = ApiResponse.success(res)
                    categoriesAdapter.updateList(res.homeData!!.homeCategoriesList as ArrayList<CategoriesItem>)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun updateToken(token: String) {
        requestCall<UpdateTokenResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateToken(UpdateTokenRequest(token)) }
        })
        { res ->
            when (res!!.code) {
                200 -> {

                }
            }
        }
    }

    init {
        getHomePage()
    }
}