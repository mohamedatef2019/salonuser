package grand.app.salonssuser.main.search.request

data class SearchRequest (
        var name: String? = null,
        var type: Int? = null
        )