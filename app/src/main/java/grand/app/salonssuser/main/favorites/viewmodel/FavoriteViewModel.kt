package grand.app.salonssuser.main.favorites.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.favorites.response.AddToFavResponse
import grand.app.salonssuser.main.favorites.response.FavoriteItem
import grand.app.salonssuser.main.favorites.view.FavoritesAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.PrefMethods
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class FavoriteViewModel : BaseViewModel() {

    var adapter = FavoritesAdapter()
    var favoritesList = ArrayList<FavoriteItem>()

    fun getFavorites() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getFavorites() } })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.favoriteData?.favoritesList!!.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            apiResponseLiveData.value = ApiResponse.success(res)
                            favoritesList = res.favoriteData.favoritesList as ArrayList<FavoriteItem>
                            adapter.updateList(favoritesList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun deleteFromFav(favoriteITem : FavoriteItem) {
        obsIsProgress.set(true)
        requestCall<AddToFavResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(favoriteITem.id!!) }
        })
        { res ->
            obsIsProgress.set(true)
            when (res!!.code) {
                200 -> {
                    adapter.removeItem(favoriteITem)
                    favoritesList.remove(favoriteITem)
                    apiResponseLiveData.value = ApiResponse.success(res)

                    when (favoritesList.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)

                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getFavorites()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}