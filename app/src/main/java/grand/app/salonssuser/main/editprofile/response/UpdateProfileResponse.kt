package grand.app.salonssuser.main.editprofile.response

import com.google.gson.annotations.SerializedName
import grand.app.salonssuser.auth.login.response.UserData

data class UpdateProfileResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val userData: UserData? = null,

	@field:SerializedName("status")
	val status: String? = null
)
