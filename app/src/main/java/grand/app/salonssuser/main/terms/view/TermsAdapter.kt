package grand.app.salonssuser.main.terms.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemTermsBinding
import grand.app.salonssuser.main.terms.response.TermsItem
import grand.app.salonssuser.main.terms.viewmodel.ItemTermsViewModel
import java.util.*

class TermsAdapter : RecyclerView.Adapter<TermsAdapter.TermsHolder>()
{
    var itemsList: ArrayList<TermsItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TermsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemTermsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_terms, parent, false)
        return TermsHolder(binding)
    }

    override fun onBindViewHolder(holder: TermsHolder, position: Int) {
        val itemViewModel = ItemTermsViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<TermsItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class TermsHolder(val binding: ItemTermsBinding) : RecyclerView.ViewHolder(binding.root)
}
