package grand.app.salonssuser.main.messages.request

import java.io.File

data class SendMessageRequest (
    var receiver_id: Int? = null,
    var salon_id: Int? = null,
    var text: String? = null,
    var img: File? = null
)