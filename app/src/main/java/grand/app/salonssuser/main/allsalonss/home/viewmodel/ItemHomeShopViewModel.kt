package grand.app.salonssuser.main.allsalonss.home.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.allsalonss.response.SalonItem

class ItemHomeShopViewModel(var item: SalonItem) : BaseViewModel(){
    var obsMinPrice = ObservableField<String>()

    init {
        obsMinPrice.set("${getString(R.string.min_price_start_from)}${item.minPrice} ${"ريال"}")
    }
}