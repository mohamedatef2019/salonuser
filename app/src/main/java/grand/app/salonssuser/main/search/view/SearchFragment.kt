package grand.app.salonssuser.main.search.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonssuser.R
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.databinding.FragmentSearchBinding
import grand.app.salonssuser.main.search.response.SearchResponse
import grand.app.salonssuser.main.search.viewmodel.SearchViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class SearchFragment : BaseHomeFragment(), Observer<Any?>
{
    lateinit var binding: FragmentSearchBinding
    lateinit var viewModel: SearchViewModel
    val fragmentArgs : SearchFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        viewModel = SearchViewModel()

        binding.viewModel = viewModel
        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(false)

        viewModel.obsName.set(fragmentArgs.salonName)
        viewModel.request.type = fragmentArgs.type
        Timber.e("Mou3aaaz : " + fragmentArgs.type)
        viewModel.searchSalons()

        observe(viewModel.adapter.itemLiveData){
            val action = SearchFragmentDirections.searchToSalonDetails(it!!.id!!)
            findNavController().navigate(action)
        }

        binding.etHomeSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SearchResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.EMPTY_NAME -> {
                showToast(getString(R.string.msg_empty_salon_name), 1)
            }
        }
    }
}