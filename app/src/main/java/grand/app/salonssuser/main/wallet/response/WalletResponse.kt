package grand.app.salonssuser.main.wallet.response

import com.google.gson.annotations.SerializedName

data class WalletResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val walletData: WalletData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class WalletData(

	@field:SerializedName("wallet")
	val walletCredit: Int? = null,

	@field:SerializedName("orders")
	val walletOrdersList: List<WalletOrderItem?>? = null
)

data class WalletOrderItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
