package grand.app.salonssuser.main.addsalon.request

import java.io.File

data class AddSalonRequest (
        var name: String? = null,
        var email: String? = null,
        var phone: String? = null,
        var type: Int? = null,
        var salon_name: String? = null,
        var salonCertificate: File? = null,
        var salonLicense: File? = null,
        var iban_number: String ?= null,
        var payment_number: String ?= null
)
