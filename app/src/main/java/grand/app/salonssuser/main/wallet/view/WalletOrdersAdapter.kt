package grand.app.salonssuser.main.wallet.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonssuser.R
import grand.app.salonssuser.databinding.ItemWalletOrderBinding
import grand.app.salonssuser.main.wallet.response.WalletOrderItem
import grand.app.salonssuser.main.wallet.viewmodel.ItemWalletOrderViewModel
import java.util.*

class WalletOrdersAdapter : RecyclerView.Adapter<WalletOrdersAdapter.WalletOrdersHolder>()
{
    var itemsList: ArrayList<WalletOrderItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletOrdersHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemWalletOrderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_wallet_order, parent, false)
        return WalletOrdersHolder(binding)
    }

    override fun onBindViewHolder(holder: WalletOrdersHolder, position: Int) {
        val itemViewModel = ItemWalletOrderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<WalletOrderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class WalletOrdersHolder(val binding: ItemWalletOrderBinding) : RecyclerView.ViewHolder(binding.root)
}
