package grand.app.salonssuser.main.terms.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.aboutapp.response.AboutItem
import grand.app.salonssuser.main.terms.response.TermsItem

class ItemTermsViewModel(var item: TermsItem) : BaseViewModel()