package grand.app.salonssuser.main.rates.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.base.LoadingStatus
import grand.app.salonssuser.main.rates.response.RatesItem
import grand.app.salonssuser.main.rates.response.RatesResponse
import grand.app.salonssuser.main.rates.view.RatesAdapter
import grand.app.salonssuser.network.ApiResponse
import grand.app.salonssuser.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class RatesViewModel : BaseViewModel()
{
    var ratesAdapter = RatesAdapter()
    var obsRateCount = ObservableField<String>()
    var obsRateAvrg = ObservableField<Float>()

    fun getRates(salonId : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<RatesResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getRates(salonId) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    if (res.ratesData!!.ratesList!!.isEmpty()) {
                        obsLayout.set(LoadingStatus.EMPTY)
                    }
                    else {
                        obsLayout.set(LoadingStatus.FULL)
                        ratesAdapter.updateList(res.ratesData!!.ratesList as ArrayList<RatesItem>)
                        obsRateCount.set("${BaseApp.getInstance.getString(R.string.label_from)} ${res.ratesData.count} ${BaseApp.getInstance.getString(R.string.label_visitor)}")
                        obsRateAvrg.set(res.ratesData.rate?.toFloat())
                        notifyChange()
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}