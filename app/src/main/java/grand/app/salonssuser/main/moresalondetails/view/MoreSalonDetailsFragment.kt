package grand.app.salonssuser.main.moresalondetails.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.salonssuser.activity.home.BaseHomeFragment
import grand.app.salonssuser.R
import grand.app.salonssuser.base.BaseApp
import grand.app.salonssuser.databinding.FragmentMoreSalonDetailsBinding
import grand.app.salonssuser.main.salondetails.response.SalonDetailsResponse
import grand.app.salonssuser.main.salondetails.response.SalonSliderItem
import grand.app.salonssuser.main.moresalondetails.viewmodel.MoreSalonDetailsViewModel
import grand.app.salonssuser.network.Status
import grand.app.salonssuser.utils.Utils
import grand.app.salonssuser.utils.constants.Codes
import grand.app.salonssuser.utils.constants.Const
import grand.app.salonssuser.utils.observe
import timber.log.Timber

class MoreSalonDetailsFragment : BaseHomeFragment(), OnMapReadyCallback
{
    lateinit var binding: FragmentMoreSalonDetailsBinding
    lateinit var viewModel: MoreSalonDetailsViewModel
    lateinit var sliderView : SliderView
    private val fragmentArgs : MoreSalonDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more_salon_details, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.salon_mab_view) as SupportMapFragment


        mapFragment.getMapAsync(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // viewModel = ViewModelProvider(this).get(MoreSalonDetailsViewModel::class.java)
        viewModel = MoreSalonDetailsViewModel()
        binding.viewModel = viewModel

        showBottomBar(false)

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.setSalonData(fragmentArgs.salonDetails)
        }

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        viewModel.setSalonData(fragmentArgs.salonDetails)
        setupSlider(fragmentArgs.salonDetails.mediaList as List<SalonSliderItem>)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SalonDetailsResponse -> {
                            setupSlider(it.data.salonDetailsData!!.mediaList as List<SalonSliderItem>)
                            when (it.data.salonDetailsData.salonType) {
                                1 -> {
                                    binding.btnHomeServices.visibility = View.VISIBLE
                                    binding.btnSalonServices.visibility = View.GONE
                                }
                                2 -> {
                                    binding.btnHomeServices.visibility = View.GONE
                                    binding.btnSalonServices.visibility = View.VISIBLE
                                }
                                else -> {
                                    binding.btnHomeServices.visibility = View.VISIBLE
                                    binding.btnSalonServices.visibility = View.VISIBLE
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.RATE_ORDER_CLICKED -> {
                    findNavController().navigate(MoreSalonDetailsFragmentDirections.salonMoreDetailsToRates(fragmentArgs.salonDetails.id!!))
                }
                Codes.FACE_CLICKED -> {
                    Utils.openFacebook(requireActivity(), viewModel.salonDetails.facebook)
                }
                Codes.TWITTER_CLICKED -> {
                    Utils.openTwitter(requireActivity(), viewModel.salonDetails.twitter)
                }
                Codes.INSTAGRAM_CLICKED -> {
                    Utils.openInstagram(requireActivity(), viewModel.salonDetails.facebook)
                }
                Codes.PHONE_CLICKED -> {
                    Utils.callPhone(requireActivity(), viewModel.salonDetails.phone.toString())
                }
                Codes.HOME_CLICKED -> {
                    binding.btnHomeServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance,R.color.white))
                    binding.btnSalonServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance,R.color.color_primary))
                }
                Codes.SALON_CLICKED -> {
                    binding.btnSalonServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance,R.color.white))
                    binding.btnHomeServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance,R.color.color_primary))
                }
            }
        }
    }

    private fun setupSlider(covers: List<SalonSliderItem>) {
        viewModel.sliderAdapter.updateList(covers)
        sliderView = requireActivity().findViewById(R.id.slider_more_salon_ads)
        sliderView.setSliderAdapter(viewModel.sliderAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }

    override fun onMapReady(googleMap: GoogleMap)
    {
        googleMap!!.clear()
        val latLng = LatLng(viewModel.salonDetails.lat!!.toDouble(), viewModel.salonDetails.lng!!.toDouble())
        val option = MarkerOptions().position(latLng)
        googleMap.addMarker(option)
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, Const.zoomLevel))
    }
}