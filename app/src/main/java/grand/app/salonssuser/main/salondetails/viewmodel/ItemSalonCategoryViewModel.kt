package grand.app.salonssuser.main.salondetails.viewmodel

import grand.app.salonssuser.base.BaseViewModel
import grand.app.salonssuser.main.salondetails.response.SalonCategoryItem
import grand.app.salonssuser.main.salondetails.response.SalonServiceItem
import grand.app.salonssuser.main.salondetails.view.SalonServicesAdapter
import java.util.ArrayList

class ItemSalonCategoryViewModel(var item: SalonCategoryItem) : BaseViewModel()
{
    var adapter = SalonServicesAdapter()

    init {
        adapter.updateList(item.salonServicesList as ArrayList<SalonServiceItem>)
    }
}