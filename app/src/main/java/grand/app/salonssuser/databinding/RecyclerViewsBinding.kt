package grand.app.salonssuser.databinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.smarteist.autoimageslider.SliderView
import com.smarteist.autoimageslider.SliderViewAdapter

/**
 * Created by MouazSalah on 15/04/2021.
 **/
class RecyclerViewsBinding {

    @BindingAdapter("adapter", "app:disableItemAnimation")
    fun bindAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, disableItemAnimation: Boolean) {
        adapter?.let {
            recyclerView.adapter = it
            if (disableItemAnimation) {
                if (recyclerView.itemAnimator != null) (recyclerView.itemAnimator as SimpleItemAnimator?)?.supportsChangeAnimations =
                        false
            }
        }
    }
}