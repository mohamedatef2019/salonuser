package grand.app.salonssuser.databinding

import androidx.databinding.DataBindingComponent
/**
 * Created by MouazSalah on 15/04/2021.
 **/
class AppDataBindingComponent : DataBindingComponent{

    override fun getOtherViewsBinding(): OtherViewsBinding {
        return OtherViewsBinding()
    }

    override fun getRecyclerViewsBinding(): RecyclerViewsBinding {
        return RecyclerViewsBinding()
    }
}